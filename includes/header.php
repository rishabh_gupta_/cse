<?php require_once("includes/session.php");?>
<?php
	require_once("includes/constants.php");
?>
<?php
		if(!isset($_SESSION['user_name']))
		{
			redirect_to("../home.php");
		}
?>
<header>
	<div id="head_container_loged">
		<div id="head_logo">
			<!--logo.png goes here-->
		</div>
		<div id="head_main">
			<h3>StageSpace</h3>
		</div>
		<div id="head_user">
			<span class="username"><?php echo $_SESSION['user_name'] ?></span>
			<br/>
			<span class="designation">
				<?php
					if($_SESSION['user_name']==SUPER)
					{
						echo "Admin";
					}
					else
					{
						echo "Designation";
					}
				?>
			</span>
		</div>
		<div id="head_menu">
			<!--Menu goes here-->
			<input type="button" name="answer" value=">" onclick="showDiv()" id="menu_btn"  />
			<div id="menu">
				<div>
					<?php include_once("includes/header_menu.php");?>
				</div>
			</div>
		</div>
	</div>
</header>
