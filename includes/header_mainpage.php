<?php
  ini_set('display_errors', true);
  error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../includes/session.php");?>
<?php require_once("../includes/functions.php");?>
<?php require_once("../includes/connection_stagespacedb.php");?>
<?php require_once("../includes/neo4jfunctions.php");?>
<?php
  //get users data srom session
  if(!isset($_SESSION['user_id']))
  {
    redirect_to("../index.php");
  } 
  $id_num_header=$_SESSION['user_id'];
  $user_name_header=$_SESSION['user_name'];
?>
<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
<script type="text/javascript" src="js/jquery1.7.2.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script>  
     $(document).ready(function()
      {
         $("#search").autocomplete({
                            source:'../includes/searchautocomplete.php'
                        });
      });
</script>
<div id="leftheader">
    <h3 class="title">Stage<b>Space</b></h3>
</div>

<div id="mainheader">
    <div class="h-btn">
        <a href="../home/index.php">
            <div class="h-link-btn">
                <i class="fa fa-home fa-1x"></i>
            </div>
        </a>
    </div>
    <div class="h-btn">
        <a href="../home/mail.php">
            <div class="h-link-btn">
               <i class="fa fa-envelope-o fa-1x"></i>
            </div>
        </a>
    </div>
    <div class="s-btn">
        <a href="#">
            <div class="s-link-btn">
               <i class="fa fa-search"></i>
            </div>
        </a>
    </div>
    <div class="search-bar">
       <form action="search.php">
            <input type="text" id="search" name="search" placeholder="Search"/>
        </form>
    </div>

    <div class="h-btn" style="float:right;border-right:0px;border-left: 1px solid #ddd;">
        <a href="../home/inbox.php">
            <div class="h-link-btn">
              <i class="fa fa-comments-o"></i>
            </div>
        </a>
    </div>
    <div class="user-bar">
        <div class="user-img">
            <?php
                $image=get_profile_img($connection,$id_num_header);
                echo '<img class="user-icon" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
            ?>
           <!--<img src="images/user_icon.jpg" class="user-icon" />-->
        </div>
        <p class="user-name"><?php echo $user_name_header;?></p>
    </div>
</div>