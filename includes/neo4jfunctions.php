<!--place to store all neo4j functions-->

<?php
ini_set('display_errors', true);
error_reporting(E_ALL ^ E_NOTICE);
use Everyman\Neo4j\Client,
	    Everyman\Neo4j\Transport,
	    Everyman\Neo4j\Node,
	    Everyman\Neo4j\Relationship;

require('vendor/autoload.php'); 
// Connecting to the default port 7474 on localhost

	/*
	*function:nodeExists to find whether the node exists or not
	*@param $label of the node, $key and $value and optional $where for further filteration
	*@return false if not exists, node if exists
	*/
	function nodeExists($label,$key,$value,$where="")
	{
		$client = new Everyman\Neo4j\Client();
		$queryString = 'MATCH (n:'.$label.' {'.$key.': "'.$value.'"}) '.$where.' return n;';
		//echo $queryString;
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryString);
		$resultSet = $query->getResultSet();
		//echo count($resultSet);
		if (count($resultSet)==0)
		{	
			//echo 'does not exist';
			return false;
		}
		else
		{
			//echo 'exist';
			foreach ($resultSet as $row) 
			{
				
			    return $row[0];

			}
		}
	}

	/*
	*function:relExists to find whether a relation exists between to nodes
	*@param $labelFrom,$keyFrom,$valueFrom of the from node, $labelTo, $keyTo and $valueTo of the To node.
	*$rel relationship lebel between the two nodes.
	*@return false if not exists, true if exists
	*/
	function relExists($labelFrom,$labelTo,$keyFrom,$valueFrom,$rel,$keyTo,$valueTo,$where="")
	{
		$client = new Everyman\Neo4j\Client();
		$queryString = 'MATCH (n:'.$labelFrom.' {'.$keyFrom.': "'.$valueFrom.'"})-[:'.$rel.']->(m:'.$labelTo.' {'.$keyTo.':"'.$valueTo.'"}) '.$where.' return count(*) as C';
		//echo $queryString;
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryString);
		$resultSet = $query->getResultSet();
	
		if ($resultSet[0]['C']>=1)
		{
			return true;
		}
		else
		{	//echo "relationship not exists";
			return false;
		}
	}

	/*
	*function:relExists to find whether a relation exists between to nodes
	*@param $labelFrom,$keyFrom,$valueFrom of the from node, $labelTo, $keyTo and $valueTo of the To node.
	*$rel relationship lebel between the two nodes.
	*@return false if not exists, true if exists
	*/
	function batchRelExists($labelFrom,$labelTo,$keyFrom,$valueFrom,$rel,$keyTo,$valueTo,$where="")
	{
		$client = new Everyman\Neo4j\Client();
		$queryString = 'MATCH (n:'.$labelFrom.' {'.$keyFrom.': "'.$valueFrom.'"})-[:'.$rel.']->(m:'.$labelTo.' {'.$keyTo.':'.$valueTo.'}) '.$where.' return count(*) as C';
		//echo $queryString;
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryString);
		$resultSet = $query->getResultSet();
	
		if ($resultSet[0]['C']>=1)
		{
			return true;
		}
		else
		{	//echo "relationship not exists";
			return false;
		}
	}

	/*
	*function:createStudent_Node to create Student node with properties if not exists
	*@param $id: Reg No, $fullname: Full Name, $deg: Degree, $course: Course, $yoj: year of joiing of Student
	*/
	function createStudent_Node($id,$fullname)
	{
		$client = new Everyman\Neo4j\Client();
		if (!nodeExists('Student','sid',$id))
		{
			//node does not exist, create node	
			$user = new Node($client);
		    $user->setProperty('sid', $id)->save();
		    $user->setProperty('name', $fullname)->save();
		    $userLabel = $client->makeLabel('Student');
			$user->save()->addLabels(array($userLabel));
			
		}
		else
		{
			//node exist, do nothing
		}

	}
	/*
	*function:createFaculty_Node to create faculty node with properties if not exists
	*@param $id: Reg No, $fullname: Full Name, $deg: Degree, $course: Course, $yoj: year of joiing of Student
	*/
	function createFaculty_Node($id,$fullname)
	{
		$client = new Everyman\Neo4j\Client();
		if (!nodeExists('Faculty','fid',$id))
		{
			//node does not exist, create node	
			$user = new Node($client);
		    $user->setProperty('fid', $id)->save();
		    $user->setProperty('name', $fullname)->save();
		    $userLabel = $client->makeLabel('Faculty');
			$user->save()->addLabels(array($userLabel));
			
		}
		else
		{
			//node exist, do nothing
		}

	}

	/*
	*function:createRoom_Node creates Room Node if not exists.
	*@param $hostelName: name of the hostel block, $roomno: room number
	*/
	function createRoom_Node($hostel_id,$roomno)
	{
		$client = new Everyman\Neo4j\Client();
		if (!nodeExists('Room','no',$roomno))
		{
			$room = new Node($client);
			$room->setProperty('no',$roomno)->save();
			$room->setProperty('block',$hostel_id)->save();
			$roomLabel = $client->makeLabel('Room');
			$room->save()->addLabels(array($roomLabel));
		}

	}


	/*
	*function:createHostel_Node creates Hostel Node if not exists.
	*@param $hostel_id: id of the hostel block
	*/
	function createHostel_Node($hostel_id)
	{
		$client = new Everyman\Neo4j\Client();
		if (!nodeExists('Hostel','hostel_id',$hostel_id))
		{
			$hostel = new Node($client);
			$hostel->setProperty('hostel_id',$hostel_id)->save();
			$hostelLabel = $client->makeLabel('Hostel');
			$hostel->save()->addLabels(array($hostelLabel));
		}
		
	}

	/*
	*function:createBatch_Node creates Barch Node if not exists.
	*@param $year: year of the batch
	*/
	function createBatch_Node($year)
	{

		$client = new Everyman\Neo4j\Client();
		$queryString = 'MATCH (n:Batch {year:'.$year.'})  return n';
		//echo $queryString;
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryString);
		$resultSet = $query->getResultSet();
		//echo count($resultSet);
		if (count($resultSet)==0)
		{	
			//echo 'does not exist';
			$batch = new Node($client);
			$batch->setProperty('year',$year)->save();
			$batchLabel = $client->makeLabel('Batch');
			$batch->save()->addLabels(array($batchLabel));
			
		}
		else
		{
			//echo 'exist';
			foreach ($resultSet as $row) 
			{
				
			    return $row[0];

			}
		
		}
		
	}

	/*
	*function:createBatch_Node creates Barch Node if not exists.
	*@param $year: year of the batch
	*/
	function createClass_Node($batch,$course,$section)
	{
		$client = new Everyman\Neo4j\Client();
		$where = 'WHERE n.year = '.$batch.' AND n.section ="'.$section.'"';
		if (!nodeExists('Class','course',$course,$where))
		{
			$class = new Node($client);
			$class->setProperty('year',$batch)->save();
			$class->setProperty('course',$course)->save();
			$class->setProperty('section',$section)->save();
			$classLabel = $client->makeLabel('Class');
			$class->save()->addLabels(array($classLabel));
		}
		
	}

	/*
	*function:createBatch_Node creates Barch Node if not exists.
	*@param $year: year of the batch
	*/
	function createCity_Node($name)
	{
		$client = new Everyman\Neo4j\Client();
		if (!nodeExists('City','name',$name))
		{
			$city = new Node($client);
			$city->setProperty('name',$name)->save();
			$cityLabel = $client->makeLabel('City');
			$city->save()->addLabels(array($cityLabel));
		}
		
	}

	/*
	*function:createBatch_Node creates Barch Node if not exists.
	*@param $year: year of the batch
	*/
	function createInterest_Node($id,$name)
	{
		$client = new Everyman\Neo4j\Client();
		if (!nodeExists('Interest','name',$name))
		{
			$interest = new Node($client);
			$interest->setProperty('name',$name)->save();
			$interest->setProperty('id',$id)->save();
			$iLabel = $client->makeLabel('Interest');
			$interest->save()->addLabels(array($iLabel));
		}
		
	}
	/*
	*function:createStudentRoom_rel to create Relationship between two nodes if node exists
	*@param $id of the Student, $hostelName and $roomno of the hostel
	*/
	function createStudentRoom_rel($id,$hostelName,$roomno)
	{
		//check if hostel room node exists
		$where = 'WHERE n.block = "'.$hostelName.'"';
		if(!nodeExists('Room','no',$roomno,$where))
		{
			createRoom_Node($hostelName,$roomno);
		}
		else
		{
			//room exists, do nothing
		}
		//get the two nodes
		$student=nodeExists('Student','sid',$id);
		$room = nodeExists('Room','no',$roomno,$where);
		//cheak if relationship between the two nodes exists already if it does not then create it
		$where2 = 'WHERE m.block = "'.$hostelName.'"';
		if(!relExists('Student','Room','sid',$id,'STAYS_IN','no',$roomno,$where2))
		{
			//relationship does not exists and needs to be created
			$student->relateTo($room, 'STAYS_IN')->save();
		}
		else
		{
			//reltionship exists, do nothing
			//echo 'not created';
		}
		
	}

	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createStudentClass_rel($id,$batch,$course,$section)
	{
		//check if hostel room node exists
		$where = 'WHERE n.year = '.$batch.' AND n.section ="'.$section.'"';
		if(!nodeExists('Class','course',$course,$where))
		{
			createClass_Node($batch,$course,$section);
		}
		else
		{
			//room exists, do nothing
		}
		//get the two nodes
		$student=nodeExists('Student','sid',$id);
		$class = nodeExists('Class','course',$course,$where);
		//cheak if relationship between the two nodes exists already if it does not then create it
		$where2 = 'WHERE m.year = '.$batch.' AND m.section ="'.$section.'"';
		if(!relExists('Student','Class','sid',$id,'ATTENDS','course',$course,$where2))
		{
			//relationship does not exists and needs to be created
			$student->relateTo($class, 'ATTENDS')->save();
		}
		else
		{
			//reltionship exists, do nothing
			//echo 'not created';
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createFacultyClass_rel($id,$batch,$course,$section)
	{
		//check if hostel room node exists
		$where = 'WHERE n.year = '.$batch.' AND n.section ="'.$section.'"';
		if(!nodeExists('Class','course',$course,$where))
		{
			createClass_Node($batch,$course,$section);
		}
		else
		{
			//room exists, do nothing
		}
		//get the two nodes
		$faculty=nodeExists('Faculty','fid',$id);
		$class = nodeExists('Class','course',$course,$where);
		//cheak if relationship between the two nodes exists already if it does not then create it
		$where2 = 'WHERE m.year = '.$batch.' AND m.section ="'.$section.'"';
		if(!relExists('Faculty','Class','fid',$id,'TAKES','course',$course,$where2))
		{
			//relationship does not exists and needs to be created
			$faculty->relateTo($class, 'TAKES')->save();
		}
		else
		{
			//reltionship exists, do nothing
			//echo 'not created';
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createClassBatch_rel($batch,$course,$section)
	{
		//check if hostel room node exists
		createClass_Node($batch,$course,$section);
		createBatch_Node($batch);
		
		
		//get the two nodes;
		$batch_node=createBatch_Node($batch);
		$where2 = 'WHERE n.year = '.$batch.' AND n.section ="'.$section.'"';
		$class_node = nodeExists('Class','course',$course,$where2);
		//cheak if relationship between the two nodes exists already if it does not then create it
		$where = 'WHERE n.section ="'.$section.'" AND n.year='.$batch.' AND m.year='.$batch.'';
		if(!batchRelExists('Class','Batch','course',$course,'BATCH_OF','year',$batch,$where))
		{
			//relationship does not exists and needs to be created
			
			$class_node->relateTo($batch_node, 'BATCH_OF')->save();
			//echo "created";
		}
		else
		{
			//reltionship exists, do nothing
			echo 'not created';
		}
		
	}
	/*
	*function:createHostelRoom_rel to create Relationship between two nodes if node exists
	*@param $room_no and $block of the room and hostel
	*/
	function createHostelRoom_rel($room_no,$block)
	{
		//check if hostel room node exists
		$where = 'WHERE n.block = "'.$block.'"';
		if(!nodeExists('Room','no',$room_no,$where))
		{
			createRoom_Node($block,$roomno);
		}
		else
		{
			//room exists, do nothing
		}
		//get the two nodes
		$room=nodeExists('Room','no',$room_no,$where);
		$hostel= nodeExists('Hostel','hostel_id',$block);
		//echo var_dump($room_node);
		//cheak if relationship between the two nodes exists already if it does not then create it
		if(!relExists('Room','Hostel','no',$room_no,'IS_IN','hostel_id',$block,$where))
		{
			//relationship does not exists and needs to be created
			$room->relateTo($hostel, 'IS_IN')->save();
			
		}
		else
		{
			//reltionship exists, do nothing
			
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_ROOMATES($roomno,$block)
	{
		$client = new Everyman\Neo4j\Client();
		$queryGetRoomates = 'MATCH (s:Student)-[:STAYS_IN]-(r:Room {no:"'.$roomno.'"}) WHERE r.block="'.$block.'" return s;';
		//echo $queryGetRoomates;
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetRoomates);
		$result = $query->getResultSet();
		return $result;
		/*
		foreach ($result as $row)
        {
			echo $value =$row[0]->getProperty('sid');
        }
        */
		//return $value;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_CLASSMATES($year,$course,$section)
	{
		$client = new Everyman\Neo4j\Client();
		$queryGetRoomates = 'MATCH (c:Class{year:'.$year.', course:"'.$course.'", section:"'.$section.'"})-[:ATTENDS]-(s:Student) return s;';
		//echo $queryGetRoomates;
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetRoomates);
		$result = $query->getResultSet();
		return $result;
		/*
		foreach ($result as $row)
        {
			echo $value =$row[0]->getProperty('sid');
        }
        */
		//return $value;
	}
    /*
	*function:createHostelRoom_rel to create Relationship between two nodes if node exists
	*@param $room_no and $block of the room and hostel
	*/
	function createRoomate_rel($id,$room_no,$block)
	{
		$result=  GET_ROOMATES($room_no,$block);
		foreach ($result as $row)
        {
			$value =$row[0]->getProperty('sid');
			$roomate=nodeExists('Student','sid',$value);
			$student= nodeExists('Student','sid',$id);
			if(!relExists('Student','Student','sid',$id,'ROOMATES','sid',$value))
			{
				//relationship does not exists and needs to be created
				$student->relateTo($roomate, 'ROOMATES')->save();
			}
			if(!relExists('Student','Student','sid',$id,'KNOWS','sid',$value))
			{
				//relationship does not exists and needs to be created
				$student->relateTo($roomate, 'KNOWS')->save();
			}
			
			else
			{
				echo 'not created';
			}
        }
		
		
	}
	/*
	*function:createHostelRoom_rel to create Relationship between two nodes if node exists
	*@param $room_no and $block of the room and hostel
	*/
	function createClassmate_rel($id,$year,$course,$section)
	{
		$result=  GET_CLASSMATES($year,$course,$section);
		foreach ($result as $row)
        {
			$value =$row[0]->getProperty('sid');
			$classmate=nodeExists('Student','sid',$value);
			$student= nodeExists('Student','sid',$id);
			if(!relExists('Student','Student','sid',$id,'CLASSMATES','sid',$value))
			{
				//relationship does not exists and needs to be created
				$student->relateTo($classmate, 'CLASSMATES')->save();
			}
			if(!relExists('Student','Student','sid',$id,'KNOWS','sid',$value))
			{
				//relationship does not exists and needs to be created
				$student->relateTo($classmate, 'KNOWS')->save();
			}
			
			else
			{
				echo 'not created';
			}
        }
		
		
	}

	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createStudentCity_rel($id,$city_name)
	{
		//check if hostel room node exists else create
		createCity_Node($city_name);
		//get the two nodes
		$student=nodeExists('Student','sid',$id);
		$city = nodeExists('City','name',$city_name);
		//cheak if relationship between the two nodes exists already if it does not then create it
		if(!relExists('Student','City','sid',$id,'BELONGS_TO','name',$city_name))
		{
			//relationship does not exists and needs to be created
			$student->relateTo($city, 'BELONGS_TO')->save();
		}
		else
		{
			//reltionship exists, do nothing
			//echo 'not created';
		}
		
	}
	/*
	*function:createFacultyCity_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createFacultyCity_rel($id,$city_name)
	{
		//check if City room node exists else create
		createCity_Node($city_name);
		//get the two nodes
		$faculty=nodeExists('Faculty','fid',$id);
		$city = nodeExists('City','name',$city_name);
		//cheak if relationship between the two nodes exists already if it does not then create it
		if(!relExists('Faculty','City','fid',$id,'BELONGS_TO','name',$city_name))
		{
			//relationship does not exists and needs to be created
			$faculty->relateTo($city, 'BELONGS_TO')->save();
		}
		else
		{
			//reltionship exists, do nothing
			//echo 'not created';
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createStudentInterest_rel($id,$interest_id,$interest_name)
	{
		//check if hostel room node exists else create

		createInterest_Node($interest_id,$interest_name);
		//get the two nodes
		$student=nodeExists('Student','sid',$id);
		$interest = nodeExists('Interest','name',$interest_name);
		//cheak if relationship between the two nodes exists already if it does not then create it
		if(!relExists('Student','Interest','sid',$id,'INTERESTED_IN','name',$interest_name))
		{
			//relationship does not exists and needs to be created
			$student->relateTo($interest, 'INTERESTED_IN')->save();
		}
		else
		{
			//reltionship exists, do nothing
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createFacultyInterest_rel($id,$interest_id,$interest_name)
	{
		//check if hostel room node exists else create

		createInterest_Node($interest_id,$interest_name);
		//get the two nodes
		$faculty=nodeExists('Faculty','fid',$id);
		$interest = nodeExists('Interest','name',$interest_name);
		//cheak if relationship between the two nodes exists already if it does not then create it
		if(!relExists('Faculty','Interest','fid',$id,'INTERESTED_IN','name',$interest_name))
		{
			//relationship does not exists and needs to be created
			$faculty->relateTo($interest, 'INTERESTED_IN')->save();
		}
		else
		{
			//reltionship exists, do nothing
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createProject_Node($id,$name)
	{
		$client = new Everyman\Neo4j\Client();
		if (!nodeExists('Project','id',$id))
		{
			$project = new Node($client);
			$project->setProperty('id',$id)->save();
			$project->setProperty('name',$name)->save();
			$projectLabel = $client->makeLabel('Project');
			$project->save()->addLabels(array($projectLabel));
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createPost_Node($id)
	{
		$client = new Everyman\Neo4j\Client();
		if (!nodeExists('Post','id',$id))
		{
			$post = new Node($client);
			$post->setProperty('id',$id)->save();
			$postLabel = $client->makeLabel('Post');
			$post->save()->addLabels(array($postLabel));
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createInterestPost_rel($post_id,$interest_id)
	{
		//check if hostel room node exists else create
		createPost_Node($post_id);
		//get the two nodes

		$post_id_chck="'".$post_id."'";
		$interest_id_chck="'".$interest_id."'";
		$post=nodeExists('Post','id',$post_id);
		$interest = nodeExists('Interest','id',$interest_id);
		//cheak if relationship between the two nodes exists already if it does not then create it
		if(!relExists('Post','Interest','id',$post_id,'POST_IN','id',$interest_id))
		{
			//relationship does not exists and needs to be created
			$post->relateTo($interest, 'POST_IN')->save();
		}
		else
		{
			echo 'already present';
			//reltionship exists, do nothing
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createProjectPost_rel($post_id,$project_id)
	{
		//check if hostel room node exists else create
		createPost_Node($post_id);
		//get the two nodes

		$post=nodeExists('Post','id',$post_id);
		$project = nodeExists('Project','id',$project_id);
		//cheak if relationship between the two nodes exists already if it does not then create it
		if(!relExists('Post','Project','id',$post_id,'POST_IN','id',$project_id))
		{
			//relationship does not exists and needs to be created
			$post->relateTo($project, 'POST_IN')->save();
		}
		else
		{
			echo 'already present';
			//reltionship exists, do nothing
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createRoomPost_rel($post_id,$room_no,$hostel)
	{
		//check if hostel room node exists else create
		createPost_Node($post_id);
		//get the two nodes
		$where = 'WHERE n.block = "'.$hostel.'"';
		$post=nodeExists('Post','id',$post_id);
		$room=nodeExists('Room','no',$room_no,$where);
		//cheak if relationship between the two nodes exists already if it does not then create it
		$where2 = 'WHERE m.block = "'.$hostel.'"';
		if(!relExists('Post','Room','id',$post_id,'POST_IN','no',$room_no,$where))	
		{
			//relationship does not exists and needs to be created
			$post->relateTo($room, 'POST_IN')->save();
		}
		else
		{
			echo 'already present';
			//reltionship exists, do nothing
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createClassRoomPost_rel($post_id,$batch,$course,$section)
	{
		//check if hostel room node exists else create
		createPost_Node($post_id);
		//get the two nodes
		$where = 'WHERE n.year = '.$batch.' AND n.section ="'.$section.'"';
		$post=nodeExists('Post','id',$post_id);
		$class = nodeExists('Class','course',$course,$where);
		//cheak if relationship between the two nodes exists already if it does not then create it
		if(!relExists('Post','Class','id',$post_id,'POST_IN','course',$course,$where))	
		{
			//relationship does not exists and needs to be created
			$post->relateTo($class, 'POST_IN')->save();
		}
		else
		{
			echo 'already present';
			//reltionship exists, do nothing
		}
		
	}
	
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createStudentProject_rel($id,$project_id,$project_name)
	{
		//check if hostel room node exists else create

		createProject_Node($project_id,$project_name);
		//get the two nodes
		$student=nodeExists('Student','sid',$id);

		$project = nodeExists('Project','id',$project_id);
		//echo var_dump($project);
		//cheak if relationship between the two nodes exists already if it does not then create it
		if(!relExists('Student','Project','sid',$id,'PART_OF','id',$project_id))
		{
			//relationship does not exists and needs to be created
			$student->relateTo($project, 'PART_OF')->save();
		}
		else
		{
			//reltionship exists, do nothing
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createFacultyProject_rel($id,$project_id,$project_name)
	{
		//check if hostel room node exists else create

		createProject_Node($project_id,$project_name);
		//get the two nodes
		$faculty=nodeExists('Faculty','fid',$id);
		$project = nodeExists('Project','id',$project_id);
		//cheak if relationship between the two nodes exists already if it does not then create it
		if(!relExists('Faculty','Project','fid',$id,'PART_OF','id',$project_id))
		{
			//relationship does not exists and needs to be created
			$faculty->relateTo($project, 'PART_OF')->save();
		}
		else
		{
			//reltionship exists, do nothing
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function createStudentRoomate_rel($id,$roomno,$block)
	{
		//check if hostel room node exists else create

		createProject_Node($project_id,$project_name);
		//get the two nodes
		$student=nodeExists('Student','sid',$id);
		$room = nodeExists('Room','id',$project_id);
		//cheak if relationship between the two nodes exists already if it does not then create it
		if(!relExists('Faculty','Project','fid',$id,'PART_OF','id',$project_id))
		{
			//relationship does not exists and needs to be created
			$faculty->relateTo($project, 'PART_OF')->save();
		}
		else
		{
			//reltionship exists, do nothing
		}
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_KNOWS_COUNT_STUDENT($id)
	{
		$client = new Everyman\Neo4j\Client();
		
		if(strlen($id)==10)
		{
			$queryGetCount = "MATCH (s:Student {sid: \"{$id}\"})-[:KNOWS]-(n) return DISTINCT count(*) AS C;";
		}
		else if (strlen($id)==9)
		{
			$queryGetCount = "MATCH (s:Faculty {fid: \"{$id}\"})-[:KNOWS]-(n) return DISTINCT count(*) AS C;";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		$value=$result[0]['C'];
		return $value;
	}	
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_PROJECT_COUNT($id)
	{
		
		$client = new Everyman\Neo4j\Client();
		if(strlen($id)==10)
		{
			$queryGetCount = "MATCH (s:Student {sid: \"{$id}\"})-[:PART_OF]-(n) return count(*) AS C;";
		}
		else if (strlen($id)==9)
		{
			$queryGetCount = "MATCH (s:Faculty {fid: \"{$id}\"})-[:PART_OF]-(n) return count(*) AS C;";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		$value=$result[0]['C'];
		echo $value;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_PROJECTS($id)
	{
		
		$client = new Everyman\Neo4j\Client();
		if(strlen($id)==10)
		{
			$queryGetCount = "MATCH (s:Student {sid: \"{$id}\"})-[:PART_OF]-(n) return n;";
		}
		else if (strlen($id)==9)
		{
			$queryGetCount = "MATCH (s:Faculty {fid: \"{$id}\"})-[:PART_OF]-(n) return n";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		return $result;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_PROJECT_MEMBERS($id)
	{
		
		$client = new Everyman\Neo4j\Client();
		$queryGetCount = "MATCH (p:Project {id: \"{$id}\"})-[:PART_OF]-(s) return s;";
		//echo $queryGetCount;
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		return $result;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_PROJECT($id)
	{
		
		$client = new Everyman\Neo4j\Client();
		$queryGetCount = "MATCH (p:Project {id: \"{$id}\"}) return p;";
		//echo $queryGetCount;
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		return $result;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_INTEREST_COUNT($id)
	{
		$client = new Everyman\Neo4j\Client();
		if(strlen($id)==10)
		{
			$queryGetCount = "MATCH (s:Student {sid: \"{$id}\"})-[:INTERESTED_IN]-(n) return count(*) AS C;";
		}
		else if (strlen($id)==9)
		{
			$queryGetCount = "MATCH (s:Faculty {fid: \"{$id}\"})-[:INTERESTED_IN]-(n) return count(*) AS C;";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		$value=$result[0]['C'];
		return $value;
	}

	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_INTERESTS($id)
	{
		$client = new Everyman\Neo4j\Client();

		if(strlen($id)==10)
		{
			$queryGetCount = "MATCH (s:Student {sid: \"{$id}\"})-[:INTERESTED_IN]-(n) return n;";
		}
		else if (strlen($id)==9)
		{
			$queryGetCount = "MATCH (s:Faculty {fid: \"{$id}\"})-[:INTERESTED_IN]-(n) return n;";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		return $result;
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_KNOWS($id)
	{
		$client = new Everyman\Neo4j\Client();
		
		if(strlen($id)==10)
		{
			$queryGetCount = "MATCH (s:Student {sid: \"{$id}\"})-[:KNOWS]-(n) return DISTINCT n;";
		}
		else if (strlen($id)==9)
		{
			$queryGetCount = "MATCH (f:Faculty {fid: \"{$id}\"})-[:KNOWS]-(n) return DISTINCT n;";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		return $result;
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_STUDENT_COUNT_INTEREST($name)
	{
		$client = new Everyman\Neo4j\Client();
		$queryGetCount = "MATCH (s:Interest {name: \"{$name}\"})-[:INTERESTED_IN]-(n) return count(*) AS C;";
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		$value=$result[0]['C'];
		return $value;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function CHECK_FIRST_DEG($from_id,$to_id)
	{
		$client = new Everyman\Neo4j\Client();
		if(strlen($from_id)==10 && strlen($to_id)==10)
		{
			$queryGetCount = "MATCH (s:Student {sid: \"{$from_id}\"})-[:KNOWS]-(m:Student{sid:\"{$to_id}\"}) return count(*) AS C;";
		}
		else if (strlen($from_id)==9 && strlen($to_id)==10)
		{
			$queryGetCount = "MATCH (s:Faculty {fid: \"{$from_id}\"})-[:KNOWS]-(m:Student{sid:\"{$to_id}\"}) return count(*) AS C;";
		}
		else if (strlen($from_id)==10 && strlen($to_id)==9)
		{
			$queryGetCount = "MATCH (s:Student {sid: \"{$from_id}\"})-[:KNOWS]-(m:Faculty{fid:\"{$to_id}\"}) return count(*) AS C;";
		}
		else if (strlen($from_id)==9 && strlen($to_id)==9)
		{
			$queryGetCount = "MATCH (s:Faculty {fid: \"{$from_id}\"})-[:KNOWS]-(m:Faculty{fid:\"{$to_id}\"}) return count(*) AS C;";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		$value=$result[0]['C'];
		return $value;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_MUTUAL_CON($from_id,$to_id)
	{
		$client = new Everyman\Neo4j\Client();
		if(strlen($from_id)==10 && strlen($to_id)==10)
		{
			$queryGetCount = "MATCH (s1:Student {sid: \"{$from_id}\"})-[:KNOWS]->(s)<-[:KNOWS]-(s2:Student{sid:\"{$to_id}\"}) return s;";
		}
		else if (strlen($from_id)==9 && strlen($to_id)==10)
		{
			$queryGetCount = "MATCH (s:Faculty {fid: \"{$from_id}\"})-[:KNOWS]->(s)<-[:KNOWS]-(s2:Student{sid:\"{$to_id}\"}) return s;";
		}
		else if (strlen($from_id)==10 && strlen($to_id)==9)
		{
			$queryGetCount = "MATCH (s:Student {sid: \"{$from_id}\"})-[:KNOWS]->(s)<-[:KNOWS]-(f1:Faculty{fid:\"{$to_id}\"}) return s;";
		}
		else if (strlen($from_id)==9 && strlen($to_id)==9)
		{
			$queryGetCount = "MATCH (s:Faculty {fid: \"{$from_id}\"})-[:KNOWS]->(s)<-[KNOWS]-(m:Faculty{fid:\"{$to_id}\"}) return s;";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		
		return $result;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_SHORTEST_PATH_COUNT($from_id,$to_id)
	{
		$client = new Everyman\Neo4j\Client();
		
		if(strlen($from_id)==10 && strlen($to_id)==10)
		{
			$queryShortestPath = "MATCH p=shortestPath((a:Student)-[:KNOWS]-(b:Student)) WHERE a.sid = \"{$from_id}\" AND b.sid =\"{$to_id}\" RETURN length(p) AS length;";
		}
		else if (strlen($from_id)==9 && strlen($to_id)==10)
		{
			$queryShortestPath = "MATCH p=shortestPath((a:Faculty)-[:KNOWS]-(b:Student)) WHERE a.fid = \"{$from_id}\" AND b.sid =\"{$to_id}\" RETURN length(p) AS length;";
		}
		else if (strlen($from_id)==10 && strlen($to_id)==9)
		{
			$queryShortestPath = "MATCH p=shortestPath((a:Student)-[:KNOWS]-(b:Faculty)) WHERE a.sid = \"{$from_id}\" AND b.fid =\"{$to_id}\" RETURN length(p) AS length;";
		}
		else if (strlen($from_id)==9 && strlen($to_id)==9)
		{
			$queryShortestPath = "MATCH p=shortestPath((a:Faculty)-[:KNOWS]-(b:Faculty)) WHERE a.fid = \"{$from_id}\" AND b.fid =\"{$to_id}\" RETURN length(p) AS length;";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryShortestPath);
		$result = $query->getResultSet();
		$count= $result[0]['length'];
		return $count;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function FIND_SHORTEST_PATH($from_id,$to_id)
	{
		$client = new Everyman\Neo4j\Client();
		
		if(strlen($from_id)==10 && strlen($to_id)==10)
		{
			$queryShortestPath = "MATCH p=shortestPath((a:Student)-[:KNOWS]-(b:Student)) WHERE a.sid = \"{$from_id}\" AND b.sid =\"{$to_id}\"  RETURN [n in nodes(p) | n.sid] as id;";
		}
		else if (strlen($from_id)==9 && strlen($to_id)==10)
		{
			$queryShortestPath = "MATCH p=shortestPath((a:Faculty)-[:KNOWS]-(b:Student)) WHERE a.fid = \"{$from_id}\" AND b.sid =\"{$to_id}\"  RETURN [n in nodes(p) | n.sid] as id;";

		}
		else if (strlen($from_id)==10 && strlen($to_id)==9)
		{
			$queryShortestPath = "MATCH p=shortestPath((a:Student)-[:KNOWS]-(b:Faculty)) WHERE a.sid = \"{$from_id}\" AND b.fid =\"{$to_id}\"  RETURN [n in nodes(p) | n.sid] as id";
			//echo $queryShortestPath;
			
		}
		else if (strlen($from_id)==9 && strlen($to_id)==9)
		{
			$queryShortestPath = "MATCH p=shortestPath((a:Faculty)-[:KNOWS]-(b:Faculty)) WHERE a.fid = \"{$from_id}\" AND b.fid =\"{$to_id}\"  RETURN [n in nodes(p) | n.sid] as id;";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryShortestPath);
		$result = $query->getResultSet();
		return $result;
		/*
		$value = GET_SHORTEST_PATH_COUNT($from_id,$to_id);
		foreach ($result as $row) 
		{
		    for($count= 0; $count<=$value;$count++)
		    {
		        echo $row[0][$count]."<br>";
		    }
		}
		*/
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_INTEREST_POSTS($interest_id)
	{
		$client = new Everyman\Neo4j\Client();
		$queryGetCount = "MATCH (i:Interest {id: \"{$interest_id}\"})-[:POST_IN]-(p:Post) return p ORDER BY p.id DESC;";
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		return $result;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_ROOM_POSTS($roomno,$block)
	{
		$client = new Everyman\Neo4j\Client();
		$queryGetRoomates = 'MATCH (r:Room {no:"'.$roomno.'"})-[:POST_IN]-(p:Post) WHERE r.block="'.$block.'" return p;';
		//echo $queryGetRoomates;
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetRoomates);
		$result = $query->getResultSet();
		return $result;
		/*
		foreach ($result as $row)
        {
			echo $value =$row[0]->getProperty('sid');
        }
        */
		//return $value;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_PROJECT_POSTS($pid)
	{
		$client = new Everyman\Neo4j\Client();
		$queryGetRoomates = 'MATCH (p:Project {id:"'.$pid.'"})-[:POST_IN]-(po:Post) return po;';
		
		//echo $queryGetRoomates;
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetRoomates);
		$result = $query->getResultSet();
		return $result;
		/*
		foreach ($result as $row)
        {
			echo $value =$row[0]->getProperty('sid');
        }
        */
		//return $value;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_CLASS_POSTS($batch,$course,$section)
	{
		$client = new Everyman\Neo4j\Client();
		$queryGetRoomates = 'MATCH (c:Class{year:'.$batch.', course:"'.$course.'", section:"'.$section.'"})-[:POST_IN]-(p:Post) return p;';
		//echo $queryGetRoomates;
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetRoomates);
		$result = $query->getResultSet();
		return $result;
		/*
		foreach ($result as $row)
        {
			echo $value =$row[0]->getProperty('sid');
        }
        */
		//return $value;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function IF_KNOWS($fromID,$toID)
	{
		$client = new Everyman\Neo4j\Client();
		$queryGetCount = "MATCH (from:Student {sid: \"{$fromID}\"})-[:KNOWS]-(to:Student{sid: \"{$toID}\"}) return count(*) as C";
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$resultSet = $query->getResultSet();
		
		if ($resultSet[0]['C']>=1)
		{
			return 1;
		}
		else
		{	//echo "relationship not exists";
			return 0;
		}
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function SET_KNOWS_WEIGHT($fromID,$toID,$weight)
	{
		$client = new Everyman\Neo4j\Client();
		$queryGetCount = "MATCH (from:Student {sid: \"{$fromID}\"})-[r:KNOWS]-(to:Student{sid: \"{$toID}\"}) SET r.weight={$weight}";
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$resultSet = $query->getResultSet();
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_KNOWS_WEIGHT($fromID,$toID,$weight)
	{
		$client = new Everyman\Neo4j\Client();
		$queryGetCount = "MATCH (from:Student {sid: \"{$fromID}\"})-[r:KNOWS]-(to:Student{sid: \"{$toID}\"}) return r.weight as W";
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$resultSet = $query->getResultSet();
		return $resultSet[0]['W'];
		
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_ROOMATES_COUNT($id)
	{
		$client = new Everyman\Neo4j\Client();
		if(strlen($id)==10)
		{
			$queryGetCount = "MATCH (s:Student {sid: \"{$id}\"})-[:ROOMATES]-(n) return count(*) AS C;";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		$value=$result[0]['C'];
		return $value;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_CLASSMATES_COUNT($batch,$course,$section)
	{
		$client = new Everyman\Neo4j\Client();
		$queryGetCount = 'MATCH (c:Class{year:'.$batch.', course:"'.$course.'", section:"'.$section.'"})-[:ATTENDS]-(s:Student) return count(*) as C;';
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		$value=$result[0]['C'];
		return $value;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_ROOMATES_FROMID($id)
	{
		$client = new Everyman\Neo4j\Client();
		if(strlen($id)==10)
		{
			$queryGetCount = "MATCH (s:Student {sid: \"{$id}\"})-[:ROOMATES]-(n) return n;";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		return $result;
	}
	/*
	*function:createStudentClass_rel to create Relationship between two nodes if node exists
	*@param $id, $batcn, $course and $section of the student
	*/
	function GET_CLASSMATES_FROMID($id)
	{
		$client = new Everyman\Neo4j\Client();
		if(strlen($id)==10)
		{
			$queryGetCount = "MATCH (s:Student {sid: \"{$id}\"})-[:CLASSMATES]-(n) return n ;";
		}
		$query = new Everyman\Neo4j\Cypher\Query($client, $queryGetCount);
		$result = $query->getResultSet();
		$value=$result[0]['C'];
		return $value;
	}
	


?>