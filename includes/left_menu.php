<div id ="left">
    <a href="profile.php">
        <div class="menu-list-item">
            <i class="fa fa-user fa-1x menu-icon"></i>
            <p style="float:left;margin:0px">Profile</p>
            <div class="clear"></div>
        </div>
    </a>
    <div class="clear"></div>
    <a href="index.php">
        <div class="menu-list-item">
            <i class="fa fa-clock-o fa-1x menu-icon"></i>
            <p style="float:left;margin:0px">Timeline</p>
            <div class="clear"></div>
        </div>
    </a>
    <a href="mail.php">
        <div class="menu-list-item">
            <i class="fa fa-comment  fa-1x menu-icon"></i>
            <p style="float:left;margin:0px">Messeges</p>
            <div class="clear"></div>
        </div>
    </a>
   
    <div id="lefth">
        <div id="interest">
            <input class="toggle-box" id="interests" type="checkbox" >
            <label  for="interests">Interests</label>
            <div id="subskill">
                <li><a href="../home/add_interest.php" class="sub">Add Interests</a></li>
                <li><a href="../home/interests.php" class="sub">Show Intersts</a></li>
            </div>
        </div>
    </div>
    <div id="lefth">
        <div id="projects">
            <input class="toggle-box" id="project" type="checkbox" >
            <label  for="project">Projects</label>
            <div id="subskill">
                <li><a href="#" class="sub">Add Projects</a></li>
                <li><a href="../home/projects.php" class="sub">Show Projects</a></li>
            </div>
        </div>
    </div>
    <a href="connect.php">
        <div class="menu-list-item">
            <i class="fa fa-users fa-1x menu-icon"></i>
            <p style="float:left;margin:0px">Connections</p>
            <div class="clear"></div>
        </div>
    </a>
    <a href="../home/class.php">
        <div class="menu-list-item">
            <i class="fa fa-university fa-1x menu-icon"></i>
            <p style="float:left;margin:0px">Classrooms</p>
            <div class="clear"></div>
        </div>
    </a>

    <a href="../index.php">
        <div class="menu-list-item">
            <i class="fa fa-sign-out fa-1x menu-icon"></i>
            <p style="float:left;margin:0px">Logout</p>
            <div class="clear"></div>
        </div>
    </a>
</div>