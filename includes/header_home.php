
<?php
	ini_set('display_errors', true);
	error_reporting(E_ALL ^ E_NOTICE);
	require_once("session.php");
	include_once("functions.php");
	//require_once("connection.php");
	require_once("connection_stagespacedb.php");
	function __autoload($class_name)
	{
		include 'class.'.$class_name.'.inc';
	} 
?>
<?php
	//start form processing
	if(isset($_POST['submit']))
	{
		//Form has been submitted
		if(true)
		{
			//perform validation on the form data
			$username=trim(mysqli_prep($connection,$_POST['username']));
			$password=trim(mysqli_prep($connection,$_POST['password']));
			$hashed_password=sha1($password);
			if($username!="" && $hashed_password!=NULL)
			{
				$query="SELECT * ";
				$query.="FROM users ";
				$query.="WHERE id_num ='{$username}' ";
				$query.="AND hashed_password ='{$hashed_password}' ";
				$query.="LIMIT 1";
				$result=mysqli_query($connection,$query);
				if(mysqli_affected_rows($connection)==1)
				{
					//successful
					$user = mysqli_fetch_array($result,MYSQLI_ASSOC);
					$stage = $user['signup'];
					$id_user=$_SESSION['user_id'];
					$id_len = strlen ( $id_user );

					
					$query2="SELECT * ";
					$query2.="FROM  ";

					if($id_len==10)
					{
						$query2.="student_profiles ";
						$query2.="WHERE SID ='{$username}' ";
					}
					else
					{
						$query2.="faculty_profiles";
						$query2.="WHERE FID ='{$username}' ";
					}
					
					$query2.="LIMIT 1";
					$result2=mysqli_query($connection,$query2);
					$user_details = mysqli_fetch_array($result2,MYSQLI_ASSOC);
					$message=1;
					
					//echo $user['id'];
					$_SESSION['user_id']=$user['id_num'];
					$_SESSION['user_name']=$user_details['FNAME']." ".$user_details['LNAME'];
					
				
					$log = new Log();
					if($user['username']==SUPER)
					{
						redirect_to("../admin.php");
					}
					else if($id_len==10)
					{	switch ($stage) 
						{
							case '1':
								redirect_to("../signup/student/basic.php");
								break;
							case '2':
								redirect_to("../signup/student/studentinfo.php");
								break;
							case '3':
								redirect_to("../signup/student/profilepic.php");
								break;
							case '4':
								redirect_to("../signup/student/interest.php");
								break;
							case '5':
								redirect_to("../signup/student/projects.php");
								break;
							case '6':
								redirect_to("../signup/student/verify.php");
								break;
							case '7':
								redirect_to("../signup/student/welcome.php");
								break;
							default:
								redirect_to("../home/index.php");
								break;
						}

						//redirect_to("../content.php");
					}
					else if($id_len==9)
					{	switch ($stage) 
						{
							case '1':
								redirect_to("../signup/faculty/basic.php");
								break;
							case '2':
								redirect_to("../signup/faculty/facultyinfo.php");
								break;
							case '3':
								redirect_to("../signup/faculty/profilepic.php");
								break;
							case '4':
								redirect_to("../signup/faculty/interest.php");
								break;
							case '5':
								redirect_to("../signup/faculty/projects.php");
								break;
							case '6':
								redirect_to("../signup/faculty/verify.php");
								break;
							case '7':
								redirect_to("../signup/faculty/welcome.php");
								break;
							default:
								redirect_to("../home/index.php");
								break;
						}

						//redirect_to("../content.php");
					}		
					
					
				}
				else
				{
					//failed
					$message=0;
					echo  "<p>".mysqli_error($connection)."</p>";
					redirect_to("../index.php");
					
				}

			}
			else
			{
				//failed
				$message=0;
				echo  "<p>".mysqli_error($connection)."</p>";
				redirect_to("../index.php");
		
			}
			
		}
		else
		{
			//failed
			$message=0;
	
		}

	}
	else
	{
		$username="";
		$password="";
	}
?>
<script type="text/javascript">
    $(window).scroll(function() {
        $('header').css('top', $(this).scrollTop() + "px");
    });
</script>
<header>
	<div id="head_container">
		<div id="head_logo">
			<!--logo.png goes here-->
		</div>
		<div id="head_main" style="width:1140px;">
			<h1 style="float:left; margin-top:15px;">Stage<b>Space</b></h1>
			<form action="includes/header_home.php" method="post">
				<div id="signin">
					<div class="signin_form">
						<h5 style="margin-bottom:1px;">User ID/Email</h5>
						<input type="text" name="username"  id="user_id"  maxlength="40" class="text" ="" style="margin-bottom:4px;" /><br>
						<h5 style="padding:0px 0px; color:#aaa;"><input type="checkbox" name="remeber" value="Remember Me"> Remeber Me</h5>
					</div>
					<div class="signin_form">
						<h5 style="margin-bottom:1px;">Password</h5>
						<input type="password" name="password"  id="password" maxlength="30"  class="text" value="" style="margin-bottom:4px;"/>
						<a href="#"><h5 style="margin-bottom:1px;">Forgot Password?</h5></a>
					</div>
					<div class="signin_btn">
						<input type="submit" name="submit" value="Login" class="login" style=" margin-top:-2px;" />
					</div>
				</div>
			</form>
		</div>
	</div>
</header>
