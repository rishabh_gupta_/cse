<!--place to store all basic functions-->
<?php

	function mysql_prep($value)
	{
		$magic_quotes_active =get_magic_quotes_gpc();
		$new_enough_php=function_exists("mysql_real_escape_string");
		if($new_enough_php)
		{
			if($magic_quotes_active)
			{
				$value=stripslashes($value);
			}
			$value=mysqli_real_escape_string($value);
		}
		else
		{
			if(!magic_quotes_active)
			{
				$value=addslashes($value);
			}
		}
		return $value;
	}
	function mysqli_prep($conn,$value)
	{
		$magic_quotes_active =get_magic_quotes_gpc();
		$new_enough_php=function_exists("mysqli_real_escape_string");
		if($new_enough_php)
		{
			if($magic_quotes_active)
			{
				$value=stripslashes($value);
			}
			$value=mysqli_real_escape_string($conn,$value);
		}
		else
		{
			if(!magic_quotes_active)
			{
				$value=addslashes($value);
			}
		}
		return $value;
	}
	function getHostels($conn,$id=1031110060)
	{
		$query_gender = "SELECT * FROM student_profiles WHERE SID = {$id} LIMIT 1";
		$result_set =mysqli_query($conn,$query_gender);
		if($result_set)
		{
			$row=mysqli_fetch_array($result_set,MYSQLI_ASSOC);
			$gender_id= $row['GENDER'];
		}
		$query_hostels = "SELECT * FROM hostels WHERE hostel_type = '{$gender_id}' OR hostel_type = 'B'";
		$result_hostels =mysqli_query($conn,$query_hostels);
		while($hostel= mysqli_fetch_array($result_hostels, MYSQLI_ASSOC))
		{
			echo '<option value= '.$hostel['hostel_id'].'>'.$hostel['hostel_name'].'</option>';
		}
	}
	function getCourse($conn,$id)
	{
		$query_course = "SELECT * FROM student_profiles WHERE SID = {$id} LIMIT 1";
		$result_set =mysqli_query($conn,$query_course);
		if($result_set)
		{
			$row=mysqli_fetch_array($result_set,MYSQLI_ASSOC);
			$course= $row['COURSE'];
		}

		return $course;
	}
	function redirect_to($location=NULL)
	{
		if($location!=NULL)
		{
			header("Location: {$location}");
			exit;
		}
		
	}
	function confirm_query($result_set)
	{
		if(!$result_set)
		{
			die("Database query failed".mysqli_error($connection));
		}
	}
	function get_all_subjects($public=true)
	{
		$query= "SELECT * FROM
				 subjects";
		if($public==true)
		{
			$query .=" WHERE visible=1";
		}
		$query .="  ORDER BY position ASC";
		$subject_set =mysql_query($query);
		//result is a collection of rows
		confirm_query($subject_set);

		return $subject_set;
	}
	function get_all_pages($subject_id,$public=true)
	{
		$query="SELECT * FROM 
				pages
				WHERE subject_id={$subject_id}";
		if($public)
		{
			$query .= " AND visible=1";
		}
		$query .=" ORDER BY id DESC";	
				

		$page_set =mysql_query($query);
		//result is a collection of rows
		confirm_query($page_set);

		return $page_set;
	}
	
	
	function get_pages_by_id($page_id)
	{
		$query = "SELECT * FROM
				  pages 
				  WHERE id={$page_id}  
				  LIMIT 1";
		$result_set=mysql_query($query);
		confirm_query($result_set);
		if($page = mysql_fetch_array($result_set))
		{
			return $page;
		}
		else
		{
			return NULL;
		}
	}
	function get_subject_by_id($subject_id)
	{
		$query = "SELECT * FROM
				  subjects
				  WHERE id={$subject_id} 
				  LIMIT 1";
		$result_set=mysql_query($query);
		confirm_query($result_set);
		if($subject = mysql_fetch_array($result_set))
		{
			return $subject;
		}
		else
		{
			return NULL;
		}
	}


	function find_selected_page()
	{
		global $sel_subject;
		global $sel_page;
		if(isset($_GET['subj']))
		{
			$sel_subject=get_subject_by_id($_GET['subj']);
			$sel_page=NULL;
		}
		else if(isset($_GET['page']))
		{
			$sel_subject=NULL;;
			$sel_page=get_pages_by_id($_GET['page']);

		}
		else
		{
			$sel_page=NULL;
			$sel_subject=NULL;
		}
	}
	function navigation($sel_subject,$sel_page,$public=false)
	{
		 $output= "<ul>";
				
			//STEP 3: Perform database query
			$subject_set=get_all_subjects();
			
			//Step 4: Use returned data
			while($subject= mysql_fetch_array($subject_set))
			{
				
				$output .= "<li";
				if($subject["id"]==$sel_subject['id'])
				{
					$output .= " class=\"selected\" ";
				}
				$output .= "><a href=\"content.php?subj=".urlencode($subject["id"])."\">{$subject["menu_name"]}</a></li>";
			
				$page_set=get_all_pages($subject["id"],$public);
				
				//Step 4: Use returned data
				$output .= "<ul class=\"pages\">";
				while($page= mysql_fetch_array($page_set))
				{
					$output .="<li";
					if($page["id"]==$sel_page['id'])
					{
						$output .= " class =\"selected\"";
					}
					$output .= "><a href=\"content.php?page=".urlencode($page["id"])."\">{$page["menu_name"]}</a></li>";
				}
				$output .= "</ul>";
			}

		$output .= "<ul/>";	
		return $output;
				
	}
	function navigation_admin($sel_subject,$sel_page,$public=false)
	{
		 $output= "<ul>";
				
			//STEP 3: Perform database query
			$subject_set=get_all_subjects();
			
			//Step 4: Use returned data
			while($subject= mysql_fetch_array($subject_set))
			{
				
				$output .= "<li";
				if($subject["id"]==$sel_subject['id'])
				{
					$output .= " class=\"selected\" ";
				}
				$output .= "><a href=\"admin.php?subj=".urlencode($subject["id"])."\">{$subject["menu_name"]}</a></li>";
			
				$page_set=get_all_pages($subject["id"],$public);
				
				//Step 4: Use returned data
				$output .= "<ul class=\"pages\">";
				while($page= mysql_fetch_array($page_set))
				{
					$output .="<li";
					if($page["id"]==$sel_page['id'])
					{
						$output .= " class =\"selected\"";
					}
					$output .= "><a href=\"admin.php?page=".urlencode($page["id"])."\">{$page["menu_name"]}</a></li>";
				}
				$output .= "</ul>";
			}

		$output .= "<ul/>";	
		return $output;
				
	}
	function navigation_public($sel_subject,$sel_page,$public=true)
	{
		 $output= "<ul>";
				
			//STEP 3: Perform database query
			$subject_set=get_all_subjects($public);
			
			//Step 4: Use returned data
			while($subject= mysql_fetch_array($subject_set))
			{
				
				$output .= "<li";
				if($subject["id"]==$sel_subject['id'])
				{
					$output .= " class=\"selected\" ";
				}
				$output .= "><a href=\"index.php?subj=".urlencode($subject["id"])."\">{$subject["menu_name"]}</a></li>";
				if($subject['id']==$sel_subject['id'] )
				{
					$page_set=get_all_pages($subject["id"],$public);
					
					//Step 4: Use returned data
					$output .= "<ul class=\"pages\">";
					while($page= mysql_fetch_array($page_set))
					{
						$output .="<li";
						if($page["id"]==$sel_page['id'])
						{
							$output .= " class =\"selected\"";
						}
						$output .= "><a href=\"index.php?page=".urlencode($page["id"])."\">{$page["menu_name"]}</a></li>";
					}
					$output .= "</ul>";
				}
			}

		$output .= "<ul/>";	
		return $output;
				
	}
	
	function upload($user_id)
	{
		/*** check if a file was uploaded ***/
		if(is_uploaded_file($_FILES['userfile']['tmp_name']) && getimagesize($_FILES['userfile']['tmp_name']) != false)
	    {
	    /***  get the image info. ***/
	    $size = getimagesize($_FILES['userfile']['tmp_name']);
	    /*** assign our variables ***/
	    $type = $size['mime'];
	    $imgfp = fopen($_FILES['userfile']['tmp_name'], 'rb');
	    $size = $size[3];
	    $name = $_FILES['userfile']['name'];
	    $maxsize = 99999999;


	    /***  check the file is less than the maximum file size ***/
	    if($_FILES['userfile']['size'] < $maxsize )
        {

	        /*** our sql query ***/
	       $query = "INSERT INTO 
	       			profile_img 
	       			(profile_id ,image) 
	       			VALUES 
	       			($user_id, $imgfp, PDO::PARAM_LOB)";

	   
	       if(mysql_query($query))
			{
				//Success
				header("Location: content.php");
				exit;
			}
			else
			{
				//Display Error Message
				echo "<p>Subject creation failed</p>";
				echo  "<p>".mysql_error()."</p>";
			}
	    }
	    else
	        {
	        /*** throw an exception is image is not of type ***/
	        throw new Exception("File Size Error");
	        }
	    }
	else
	    {
	    // if the file is not less than the maximum allowed, print an error
	    throw new Exception("Unsupported Image Format!");
	    }
	}
	function set_stage($connection,$id,$stage)
	{
		
		$query="UPDATE users ";
		$query.="SET signup = '{$stage}' WHERE id_num = '{$id}'";
		$result=mysqli_query($connection,$query);
		if(mysqli_affected_rows($connection)==1)
		{
		}
	}
	function get_project_id($connection,$title,$doc)
	{
		$query = "SELECT * FROM
				  project 
				  WHERE NAME = '{$title}' AND DOC = '{$doc}' 
				  LIMIT 1";
		$result_set=mysqli_query($connection,$query);
		confirm_query($result_set);
		if($page = mysqli_fetch_array($result_set,MYSQLI_ASSOC))
		{
			return $page['id'];
		}
		else
		{
			return NULL;
		}
	}
	function get_project_details($connection,$pid)
	{
		$query = "SELECT * FROM
				  project 
				  WHERE PID = '{$pid}'
				  LIMIT 1";
		$result_set=mysqli_query($connection,$query);
		confirm_query($result_set);
		$project = mysqli_fetch_array($result_set,MYSQLI_ASSOC);
		return $project;
		
	}
	function get_project_name($connection,$pid)
	{
		$project=get_project_details($connection,$pid);
		return $project['NAME'];
		
	}
	function get_emailid($connection,$id)
	{
		$query = "SELECT * FROM
				  student_profiles 
				  WHERE SID = '{$id}'
				  LIMIT 1";
		$result_set=mysqli_query($connection,$query);
		confirm_query($result_set);
		if($page = mysqli_fetch_array($result_set,MYSQLI_ASSOC))
		{
			return $page['CEMAIL'];
		}
		else
		{
			return NULL;
		}
	}
	function get_vcode($connection,$id)
	{
		$query = "SELECT * FROM
				  users 
				  WHERE id_num = '{$id}'
				  LIMIT 1";
		$result_set=mysqli_query($connection,$query);
		confirm_query($result_set);
		if($page = mysqli_fetch_array($result_set,MYSQLI_ASSOC))
		{
			return $page['vcode'];
		}
		else
		{
			return NULL;
		}
	}
	function get_profile_details($connection,$id)
	{
		$id_len = strlen ( $id );
		$query="SELECT * ";
		$query.="FROM  ";

		if($id_len==10)
		{
			$query.="student_profiles ";
			$query.="WHERE SID ='{$id}' ";
		}
		else if($id_len==9)
		{
			$query.="faculty_profiles ";
			$query.="WHERE FID ='{$id}' ";
		}
		
		$query.="LIMIT 1";
		$result=mysqli_query($connection,$query);
		$user_details = mysqli_fetch_array($result,MYSQLI_ASSOC);
		return $user_details;
	}
	function get_dob($connection,$id)
	{
		$user=get_profile_details($connection,$id);
		return $user['DOB'];
	}
	function get_gender($connection,$id)
	{
		$user=get_profile_details($connection,$id);
		return $user['GENDER'];
	}
	function get_course($connection,$id)
	{
		$user=get_profile_details($connection,$id);
		return $user['COURSE'];
	}
	function get_batch($connection,$id)
	{
		$user=get_profile_details($connection,$id);
		return $user['BATCH'];
	}
	function get_section($connection,$id)
	{
		$user=get_profile_details($connection,$id);
		return $user['SECTION'];
	}
	function get_hostel($connection,$hostel_code)
	{

		$query="SELECT * ";
		$query.="FROM  ";
		$query.="hostels ";
		$query.="WHERE hostel_id ='{$hostel_code}' ";
		$query.="LIMIT 1";
		$result=mysqli_query($connection,$query);
		$hostel_details = mysqli_fetch_array($result,MYSQLI_ASSOC);
		return  $hostel_details['hostel_name'];
	}
	function get_hostel_id($connection,$id)
	{
		$user=get_profile_details($connection,$id);
		$hostel_id=$user['HNAME'];
		return $hostel_id;
	}
	function get_room_no($connection,$id)
	{
		$user=get_profile_details($connection,$id);
		$room_no=$user['ROOMNO'];
		return $room_no;
	}
	function get_college_add($connection,$id)
	{
		$user=get_profile_details($connection,$id);
		$hostel_name=get_hostel($connection,$user['HNAME']);
		$hostel_roomno=$user['ROOMNO'];
		return $hostel_name.", ".$hostel_roomno;
	}
	function get_hometown($connection,$id)
	{
		$user=get_profile_details($connection,$id);
		return ucwords ($user['CITY']);
	}
	function get_cgpa($connection,$id)
	{
		$user=get_profile_details($connection,$id);
		return $user['CGPA'];
	}
	
	function get_user_name($connection,$id)
	{
		$user=get_profile_details($connection,$id);
		return $user['FNAME']." ".$user['LNAME'];
	}
	function get_profile_img($connection,$id)
	{

        $query= "SELECT * 
            FROM  profile_img
            WHERE user_id ={$id}
            LIMIT 1";

        $result_set=mysqli_query($connection,$query);
        confirm_query($result_set);
        if($page = mysqli_fetch_array($result_set,MYSQLI_ASSOC))
        {
          $imageData=$page["image"];
        }

        return $imageData;

                  
	}
	function get_interest_img($connection,$name)
	{
		$query= "SELECT * 
            FROM  interest_list
            WHERE NAME ='{$name}'
            LIMIT 1";

        $result_set=mysqli_query($connection,$query);
        confirm_query($result_set);
        if($page = mysqli_fetch_array($result_set,MYSQLI_ASSOC))
        {
          $imageData=$page["URL"];
        }

        return $imageData;
	}
	function getInterestList($connection)
	{
		
		$query= "SELECT * FROM  interest_list";
		$result_set =mysqli_query($connection,$query);
		while($interest= mysqli_fetch_array($result_set, MYSQLI_ASSOC))
		{
			echo '<option value= '.$interest['IID'].'>'.$interest['NAME'].'</option>';
		}
	}
	function getUsersList($connection)
	{
		$output ="";
		$query= "SELECT * FROM  users";
		$result_set =mysqli_query($connection,$query);
		while($user= mysqli_fetch_array($result_set, MYSQLI_ASSOC))
		{
			$output .= '<option value= '.$user['id_num'].'>'.$user['id_num'].'</option>';
		}
		return $output;
	}
	function getInterestList_new($connection)
	{
		$output ="";
		$query= "SELECT * FROM  interest_list";
		$result_set =mysqli_query($connection,$query);
		while($interest= mysqli_fetch_array($result_set, MYSQLI_ASSOC))
		{
			$output .= '<option value= '.$interest['IID'].'>'.$interest['NAME'].'</option>';
		}
		return $output;
	}
	function insertPostDB($connection,$from_id,$post,$tag)
	{
		$query="INSERT INTO posts
				(FROMID,POST,TAG)
				VALUES
				('{$from_id}','{$post}','{$tag}')";
		$result=mysqli_query($connection,$query);
		if(mysqli_affected_rows($connection)==1)
		{
			
			$query= "SELECT * 
            FROM  posts
            WHERE FROMID ='{$from_id}'
            AND POST = '{$post}'
            ANd TAG = '{$tag}'
            LIMIT 1";

	        $result_set=mysqli_query($connection,$query);
	        confirm_query($result_set);
	        if($page = mysqli_fetch_array($result_set,MYSQLI_ASSOC))
	        {
	           return $page['POSTID'];
	        }
		}
	}
	function getPost($connection,$post_id)
	{
		$query= "SELECT * 
            FROM  posts
            WHERE POSTID ='{$post_id}'
            LIMIT 1";

        $result_set=mysqli_query($connection,$query);
        confirm_query($result_set);
        return $result_set;
	}
	function insertMsgDB($connection,$from_id,$to_id,$msg)
	{
		$query="INSERT INTO messages
				(FromID,ToID,msg)
				VALUES
				('{$from_id}','{$to_id}','{$msg}')";
		$result=mysqli_query($connection,$query);
		if(mysqli_affected_rows($connection)==1)
		{
			
			//do nothung
		}
	}
	function get_interest_name($connection,$interest_id)
	{
		$query= "SELECT * 
            FROM  interest_list
            WHERE IID ='{$interest_id}'
            LIMIT 1";
        $result_set=mysqli_query($connection,$query);
        confirm_query($result_set);
        if($page = mysqli_fetch_array($result_set,MYSQLI_ASSOC))
        {
           return $page['NAME'];
        }
	}
	function get_messages($connection,$from_id,$to_id)
	{
		$query= "SELECT * FROM messages
		 WHERE (FromID='{$from_id}' AND ToID='{$to_id}') OR (ToID='{$from_id}' AND FromID='{$to_id}')
                      
          ";
          //echo $query;
        $result_set=mysqli_query($connection,$query);
        confirm_query($result_set);
        return $result_set;
	}
?>