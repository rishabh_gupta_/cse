<?php

/**
 * This class logs the users login detials into the database upon instantiation. 
 */
require_once("session.php");
//include 'class.Database.inc';
class Log 
{

  
  // Stores users ip address
  public $user_ip_address;
  // Stores users user id
  public $user_id;
  // Stores users browser
  public $user_browser;
  // Stores users os
  public $user_os;
  // login time
  public $user_time;
  
  
  /**
   * Constructor.
   */
  function __construct() 
  {
     $this->user_id= $_SESSION['user_name'];
     $this->user_ip_address= $this->getIpAddress();
     $this->user_os = $this->getOS();
     $this->user_browser=$this->getBrowser();
     $this->user_time = date("Y/m/d");
     $this->insert();
  }

  /*
  *Function to get the ip address of the user
  */
  function getIpAddress()
  {
      $http_client_ip=$_SERVER['HTTP_CLIENT_IP'];
      $http_x_forwarded_for=$_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote_addr=$_SERVER['REMOTE_ADDR'];
      if(!empty($this->http_client_ip))
      {
          return $http_client_ip;
      }
      else if(!empty($this->http_x_forwarded_for))
      {
          return $http_x_forwarded_for;
      }
      else
      {
          return $remote_addr;
      }
  }

  /*
  *Function to get the OS of the user
  */
  function getOS() 
  { 

      $user_agent=$_SERVER['HTTP_USER_AGENT'];

      $os_platform    =   "Unknown OS Platform";

      $os_array       =   array(
                              '/windows nt 6.3/i'     =>  'Windows 8.1',
                              '/windows nt 6.2/i'     =>  'Windows 8',
                              '/windows nt 6.1/i'     =>  'Windows 7',
                              '/windows nt 6.0/i'     =>  'Windows Vista',
                              '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                              '/windows nt 5.1/i'     =>  'Windows XP',
                              '/windows xp/i'         =>  'Windows XP',
                              '/windows nt 5.0/i'     =>  'Windows 2000',
                              '/windows me/i'         =>  'Windows ME',
                              '/win98/i'              =>  'Windows 98',
                              '/win95/i'              =>  'Windows 95',
                              '/win16/i'              =>  'Windows 3.11',
                              '/macintosh|mac os x/i' =>  'Mac OS X',
                              '/mac_powerpc/i'        =>  'Mac OS 9',
                              '/linux/i'              =>  'Linux',
                              '/ubuntu/i'             =>  'Ubuntu',
                              '/iphone/i'             =>  'iPhone',
                              '/ipod/i'               =>  'iPod',
                              '/ipad/i'               =>  'iPad',
                              '/android/i'            =>  'Android',
                              '/blackberry/i'         =>  'BlackBerry',
                              '/webos/i'              =>  'Mobile'
                          );

  foreach ($os_array as $regex => $value) 
  { 
    if (preg_match($regex, $user_agent)) 
    {
        $os_platform    =   $value;
    }
  }   

  return $os_platform;

  }

  /*
  *Function to get the browser of the user
  */
  function getBrowser() 
  {
    $user_agent=$_SERVER['HTTP_USER_AGENT'];;

    $browser        =   "Unknown Browser";

    $browser_array  =   array(
                            '/msie/i'       =>  'Internet Explorer',
                            '/firefox/i'    =>  'Firefox',
                            '/safari/i'     =>  'Safari',
                            '/chrome/i'     =>  'Chrome',
                            '/opera/i'      =>  'Opera',
                            '/netscape/i'   =>  'Netscape',
                            '/maxthon/i'    =>  'Maxthon',
                            '/konqueror/i'  =>  'Konqueror',
                            '/mobile/i'     =>  'Handheld Browser'
                        );

    foreach ($browser_array as $regex => $value) 
    { 
      if (preg_match($regex, $user_agent)) 
      {
        $browser    =   $value;
      }
    }

    return $browser;
  }

  /*
  *Function to insert the users log into the database
  */
  public function insert() 
  {
    $db = Database::getInstance();
    $mysqli = $db->getConnection();
    $sql_query  = 'SELECT* FROM pages ';
    $query="INSERT INTO log
        (ID,TIME,IP,BROWSER,OS)
        VALUES
        ('{$this->user_id}','{$this->user_time}','{$this->user_ip_address}','{$this->user_browser}','{$this->user_os}')";
    $result = $mysqli->query($query);
  }
}