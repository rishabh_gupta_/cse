<?php require_once("../../includes/session.php");?>
<?php require_once("../../includes/connection.php");?>
<?php require_once("../../includes/functions.php");?>

<?php
	find_selected_page();

?>
<?php
	//start form processing
	if(isset($_POST['submit']))
	{
		//Form has been submitted

		//perform validation on the form data
		$username=trim(mysql_prep($_POST['username']));
		$password=trim(mysql_prep($_POST['password']));
		$hashed_password=sha1($password);

		$query="INSERT INTO faculty_users
				(username,hashed_password)
				VALUES
				('{$username}','{$hashed_password}')";
		$result=mysql_query($query,$connection);
		if(mysql_affected_rows()==1)
		{
			//successful
			$message=1;
			//redirect_to("content.php?page={$id}");
		}
		else
		{
			//failed
			$message=0;
			echo  "<p>".mysql_error()."</p>";
	
		}

	}
	else
	{
		$username="";
		$password="";
		if(isset($_GET['logout']) && $_GET['logout']==1)
		{
			echo "You are now logged out";
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../../style/main.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="javascripts/basic.js"></script>
<title>StageSpace</title>

</head>
<body>
	<!--include header-->
	<?php include("../../includes/header_home.php");?>
	<!--header ends-->
	<div id="bubble_container">
	
	</div>
	
	<!--include footer-->
	<?php require("../../includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
