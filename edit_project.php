<?php require_once("includes/connection.php");?>
<?php require_once("includes/functions.php");?>

<?php
	if(intval($_GET['page'])==0)
	{
		redirect_to("content.php");
	}
	if(isset($_POST['submit']))
	{
		
		$errors=array();
		$required_fields=array('menu_name','position','content');
		foreach ($required_fields as  $fieldname) {
			if(!isset($_POST[$fieldname]) || empty($_POST[$fieldname]) )
			{
				$errors[]=$fieldname;
			}
		}
		if(empty($errors))
		{
			//perform query
			$id=mysql_prep($_GET['page']);
			$menu_name=mysql_prep($_POST['menu_name']);
			$position=mysql_prep($_POST['position']);
			$visible= mysql_prep($_POST['visible']);
			$content= mysql_prep($_POST['content']);
			$query="UPDATE pages SET
					menu_name ='{$menu_name}',
					position = {$position},
					visible = {$visible},
					content ='{$content}'
					WHERE id={$id}";
			$result= mysql_query($query);
			if(mysql_affected_rows()==1)
			{
				//successful
				$message=1;
				//redirect_to("content.php?page={$id}");
			}
			else
			{
				//failed
				$message=0;
			}
		}
		else
		{
			$message=0;
		}
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style/main.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="javascripts/basic.js"></script>
<title>Edit Project</title>

</head>
<body>
	<!--include header-->
	<?php find_selected_page();?>
	<?php include("includes/header.php");?>
	<!--header ends-->
	<div id="body_container">
		<div id="body_left">
			<!--User Pofile goes here-->
			<div id="profile_pic">
				<?php include("includes/profile_pic.php");?>
			</div>
			<div id="profile_nav">
				<!--Profile Navigation goes here-->
				<?php echo navigation($sel_subject,$sel_page);?>
				<br/>
				<div class="create_menu">
	
				</div>
			</div>
		</div>
		<div id="body_main">
			<!--Feed goes here-->
			<div class="body_header">
				<h2>Edit Project: <?php echo $sel_page['menu_name'] ?></h2>
				<?php
					if(isset($message) && $message==1)
					{
					?>
						<div class="saved">
							Saved
						</div>
					<?php
					}
					else if(isset($message) && $message==0)
					{
					?>
						<div class="error">
							Error
						</div>
					<?php
					}
				?>
			</div>
			<div class="body_container">
				<form action="edit_project.php?page=<?php echo urlencode($sel_page['id']); ?>" method="post">
					<p>Project Name:
						<input type="text" name="menu_name" value="<?php echo $sel_page['menu_name']?>" id="menu_name"/>
					</p>
					<p>Discription:</p>
					<p>
						<textarea rows="5" cols="94" name="content" value="" id="content"><?php echo $sel_page['content']?></textarea>
					</p>
					<p>Position:
						<select name="position">
							<?php
								$subject_set=get_all_pages(2);
								$subject_count=mysql_num_rows($subject_set);
								for($count=1; $count <= $subject_count+1; $count++ )
								{
									echo "<option value=\"{$count}\"";
									if($sel_page['position']==$count)
									{
										echo " selected";
									}
									echo ">{$count}</option>";
								}
								
							?>	
							
						</select>
					</p>
					<p>Visible:
						&nbsp;
						<input type="radio" name="visible" value="0" <?php 
							if($sel_page['visible']==0)
							{
								echo " checked";
							}
						?>/>&nbsp;No
						&nbsp;
						<input type="radio" name="visible" value="1" <?php
							if($sel_page['visible']==1)
							{
								echo " checked";
							}
						?>/>&nbsp;Yes
					</p>
					<input type="submit" name="submit" value="Save" id="submit_btn" />
					<div class="clear"></div>
				</form>
				</br>
				
				<div class="btn"> 
					<a href=<?php echo "delete_project.php?page=".urlencode($sel_page["id"]);?> onclick="return confirm('Are You Sure?');">Delete</a>
				</div>
				<div class="btn">
					<a href=<?php echo "content.php?page=".urlencode($sel_page["id"]);?>>Cancel</a>
				</div>
				<div class="clear"></div>
			</div>
				
			
		</div>
		<div id="body_right">
			<!--opposite details goes here-->
			<?php
				include('neo.php');
			?>
		</div>
	</div>
	<!--include footer-->
	<?php require("includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 