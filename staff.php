<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style/main.css" />
<title>CSE Portal-Staff</title>

</head>
<body>
	<!--include header-->
	<?php include("includes/header.php");?>
	<!--header ends-->
	<div id="body_container">
		<div id="body_left">
			<!--User Pofile goes here-->
			<div id="profile_pic"><!--prfile picture goes here--></div>
			<div id="profile_nav">
				<!--Profile Navigation goes here-->
				<ul>
					<li><a href="content.php">Profile</a></li>
					<li><a href="projects.php">Projects</a></li>
					<li><a href="new_user.php">Add Staff User</a></li>
					<li><a href="logout.php">Logout</a></li>
				</ul>
			</div>
		</div>
		<div id="body_main">'
			<!--Feed goes here-->
		</div>
		<div id="body_right">
			<!--opposite details goes here-->
		</div>
	</div>
	<!--include footer-->
	<?php include("includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
