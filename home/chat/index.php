<!DOCTYPE HTML>
<html>
	<head>
		<title>Chat</title>
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	</head>
	<body>
		<div class="chat">
			<input type="text" class="chat-name" placeholder="Enter the name"/>
			<div class="chat-messages">
				
			</div>
			<textarea class="chat-textarea" placeholder="Type your message."></textarea>
			<div class="chat-status">Status: <span>Idle</span></div>
		</div>
		<script src="http://127.0.0.1:8080/socket.io/socket.io.js"></script>
		<script type="text/javascript">
			(function() 
			{
				var getNode = function(s)
				{
					return document.querySelector(s);
				},
				
				//get required nodes
				status = getNode('.chat-status span');
				textarea = getNode('.chat textarea');
				chatName = getNode('.chat-name');
				messages= getNode('.chat-messages');

				statusDefault = status.textContent;

				setStatus = function(s)
				{
					status.textContent=s;
					var delay = setTimeout(function(){
						setStatus(statusDefault);
						clearInterval(delay);
					},3000);
				
				};
				//setStatus('Testing');
				console.log(statusDefault);
				try
				{
					var socket = io.connect('http://127.0.0.1:8080');
				}
				catch(e)
				{
					//set status to warn user
				}
				if(socket !== undefined)
				{
					//listen for output
					socket.on('output',function(data){
						//console.log(data);
						if(data.length){
							//loop through results
							for(var x=0; x<data.length;x=x+1)
							{
								var message = document.createElement('div');
								message.setAttribute('class','chat-message');
								message.textContent=data[x].name + ":" + data[x].message;
								//append
								messages.appendChild(message);
								//ssages.insertBefore(message,messages.firstChild);
								$(".chat-messages").animate({ scrollTop: $(".chat-messages")[0].scrollHeight}, 2000);
							}
						}
					});
					//listen for a status
					socket.on('status', function(data)
					{
						setStatus((typeof data === 'object') ? data.message : data);
						if(data.clear===true)
						{
							textarea.value="";
						}
					});
					//listen for keydown
					textarea.addEventListener('keydown', function(event){
						var self = this,
							name = chatName.value;
				
						if(event.which ===13 && event.shiftKey===false)
						{
							//console.log('Sent!');
							socket.emit('input',{
								name: name,
								message: self.value
							});
						}
					});
				}
			})();
		</script>
	</body>
</html>