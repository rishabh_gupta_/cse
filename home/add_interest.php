
<?php
  ini_set('display_errors', true);
  error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../includes/session.php");?>
<?php require_once("../includes/functions.php");?>
<?php require_once("../includes/connection_stagespacedb.php");?>
<?php require_once("../includes/neo4jfunctions.php");?>
<?php
  //get users data srom session
  if(!isset($_SESSION['user_id']))
  {
    redirect_to("../index.php");
  } 
  $id_num=$_SESSION['user_id'];
  $user_name=$_SESSION['user_name'];
  $batch= get_batch($connection,$id_num);
  $course= get_course($connection,$id_num);
  $section=get_section($connection,$id_num); 
  $interest_list=getInterestList_new($connection);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" href="main.css" type="text/css"/>
        <link href="main.js" type="text/javascript"/>
        <script type="text/javascript" ="js/jquery-1.4.2.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-type" content="text/html" charset="utf-8"/>
        <meta http-equiv="content-type" content="cache"/>
        <meta name ="robots" content="index,follow"/>
        <meta name="keywords" content="enter contents"/>
        <meta name="description"  content="describe here"/>
        
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="../style/signup_form.css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <script src="js/modernizr.custom.js"></script>
		<title>StageSpace</title>
	</head>
	<body>
        <!--Header included here-->
		<?php include('../includes/header_mainpage.php');?>
        <!--Header ends here-->
        
        <!--left main menu is included here-->
        <?php include('../includes/left_menu.php');?>
        <!--Left Menu Ends Here-->

       
        <div id= "body-gen-main">
           <div class="interest-cards-container" style="margin-top:15px;height:100px;" >
                   <p class="contact" style="font-size:14px;"><i class="fa fa-plus">&nbsp;&nbsp;Add Interest</i></p>
                     <form name="interest-form" method="post" style="margin:15px 15px;" >
                        <select id="tag-selector" name="selector" >
                              <?php
                                  echo $interest_list;
                              ?>
                        </select>
                        <div style="clear:both"></div>
                         <div class="post-btn" style="float:left;width:20px;padding:8px 5px;" >
                         <i class="fa fa-bookmark" style="float:left;width:80px;margin-top: 4px;">&nbsp;&nbsp;
                         
                           <input type='submit' name="post" id="post-btn" value="Add" style="float:right;width:50px;  margin-top: 1px;
  margin-right: 9px; background-color:white;">
                          
                         </i>
                     </div>
                   </form>
                   <!--
                   <div class="interest-card" style="margin-right:0px;">
                       <p class="interest-card-header">Interest Name</p>
                       <i class="fa fa-heart">&nbsp;&nbsp;219</i>
                       <div class="interest-card-img"></div>
                       <p class="interest-card-desp">Lates News of the interest description goes here.</p>
                   </div>
                  -->
                   <div style="clear:both"></div>
               </div>
         </div>
           
        <!--
        <div id="footer">
            <p> Copyright</p>
        </div>
        -->
        <script src="js/classie.js"></script>
        <script>
            var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
                menuRight = document.getElementById( 'cbp-spmenu-s2' ),
                menuTop = document.getElementById( 'cbp-spmenu-s3' ),
                menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
                showLeft = document.getElementById( 'showLeft' ),
                showRight = document.getElementById( 'showRight' ),
                showTop = document.getElementById( 'showTop' ),
                showBottom = document.getElementById( 'showBottom' ),
                showLeftPush = document.getElementById( 'showLeftPush' ),
                showRightPush = document.getElementById( 'showRightPush' ),
                body = document.body;

           
            showRight.onclick = function() {
                classie.toggle( this, 'active' );
                classie.toggle( menuRight, 'cbp-spmenu-open' );
                
            };
            
           

            
        </script>
        



	</body>
</html>
