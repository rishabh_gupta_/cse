
<?php
  ini_set('display_errors', true);
  error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../includes/session.php");?>
<?php require_once("../includes/functions.php");?>
<?php require_once("../includes/connection_stagespacedb.php");?>
<?php require_once("../includes/neo4jfunctions.php");?>
<?php
  //get users data srom session
  if(!isset($_SESSION['user_id']))
  {
    redirect_to("../index.php");
  } 
  $id_num=$_SESSION['user_id'];
  $user_name=$_SESSION['user_name'];
  $batch= get_batch($connection,$id_num);
  $course= get_course($connection,$id_num);
  $section=get_section($connection,$id_num); 
  $user_list=getUsersList($connection);
  if(isset($_POST['submit']))
  {
    //Form has been submitted

    //perform validation on the form data
    $title=trim(mysqli_prep($connection,$_POST['title']));
    $field=trim(mysqli_prep($connection,$_POST['field']));
    $doc=trim(mysqli_prep($connection,$_POST['doc']));
    $partner=trim(mysqli_prep($connection,$_POST['student_id']));
    $desc =trim(mysqli_prep($connection,$_POST['section']));
    $status =trim(mysqli_prep($connection,$_POST['status']));
    //echo $title." ".$field." ".$doc." ".$partner." ".$desc;
    $query="INSERT INTO project
        (NAME,AREA,DES,STATUS,DOC)
        VALUES
        ('{$title}','{$field}','{$desc}','{$status}','{$doc}')";
    $result=mysqli_query($connection,$query);
    if(mysqli_affected_rows($connection)==1)
    {
      
      $query_pid = "SELECT * 
      FROM  `project` 
      WHERE NAME =  '{$title}'
      LIMIT 1";
      $result_set=mysqli_query($connection,$query_pid);
      confirm_query($result_set);
      if($page = mysqli_fetch_array($result_set,MYSQLI_ASSOC))
      {
        $pid= $page['PID'];
        
        createStudentProject_rel($id_num,$pid,$title);
        
        if ($partner!=NULL) 
        {
          createStudentProject_rel($partner,$pid,$title);
    
        }
       }
        
      echo $pid;
      }
  }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" href="main.css" type="text/css"/>
        <link href="main.js" type="text/javascript"/>
        <script type="text/javascript" ="js/jquery-1.4.2.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-type" content="text/html" charset="utf-8"/>
        <meta http-equiv="content-type" content="cache"/>
        <meta name ="robots" content="index,follow"/>
        <meta name="keywords" content="enter contents"/>
        <meta name="description"  content="describe here"/>
        <script type="text/javascript" src="../javascripts/autocomplete.js"></script>
        <script type="text/javascript" src="../javascripts/project.js"></script>
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="../style/signup_form.css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <style type="text/css">
        label
        {
            font-size: 14px;
            color: #5B5B5B;
        }
        </style>
		<title>StageSpace</title>
	</head>
	<body>
        <!--Header included here-->
		<?php include('../includes/header_mainpage.php');?>
        <!--Header ends here-->
        
        <!--left main menu is included here-->
        <?php include('../includes/left_menu.php');?>
        <!--Left Menu Ends Here-->

       
        <div id= "body-gen-main">
           <div class="interest-cards-container" style="margin-top:15px;min-height:400px;" >
                   <p class="contact" style="font-size:14px;"><i class="fa fa-plus">&nbsp;&nbsp;Add Project</i></p>
                     <form name="signup_basic_form" method="post">
          <div class="clear"></div>
          <div class="signup_left_col" style="width:30%;">
            <p>
              <label>Title</label><br>
              <input type="text" name="title" maxlength="50" id="title" />
            </p>
            <p>
              <label>Field</label><br>
              <select name="field" id="field">
                <option value="Research">Research</option>
                <option value="Application">Application</option>
                 <option value="Paper">Paper</option>
              </select>
            </p>
            <p>
              <label>Date of Commencement</label><br>
              <input type="date" name="doc" maxlength="50" id="doc" />
            </p>
            <p>
              <label>Status</label><br>
              <select name="status" id="status">
                <option value="Starting">Starting</option>
                <option value="On Going">On Going</option>
                <option value="Finished">Finished</option>
              </select>
            </p>
           
          </div>
          <div class="signup_right_col" style="width:32%;float:left;">
            <p>
              <!--
              <label style="float:left;width:50%;margin-bottom:0px;">Add partner</label>
              <label style="float:right;margin-bottom:0px;" class="add">+</label>
              -->
              <label>Add Partner</label>
              <br>
              <select name="student_id">
                    <option value="none">Select</option>
                    <?php
                        echo $user_list;
                    ?>
              </select>
            </p>
          </div>
          <div class="signup_right_col" style="width:30%;float:right;margin-right:15px;">
            <p>
              <label>Discription</label><br>
              <textarea row="5" col="50" name="section" maxlength="200" id="section" class="dicp"></textarea> 
            </p>
          </div>
          <div style="clear:both"></div>
          <input type="submit" name="submit" value="Add" id="signup_btn" style="width:50px;float:right;margin-right:15px;">
        </form>
                   <!--
                   <div class="interest-card" style="margin-right:0px;">
                       <p class="interest-card-header">Interest Name</p>
                       <i class="fa fa-heart">&nbsp;&nbsp;219</i>
                       <div class="interest-card-img"></div>
                       <p class="interest-card-desp">Lates News of the interest description goes here.</p>
                   </div>
                  -->
                   <div style="clear:both"></div>
               </div>
         </div>
           
        <!--
        <div id="footer">
            <p> Copyright</p>
        </div>
        -->
        <script src="js/classie.js"></script>
        <script>
            var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
                menuRight = document.getElementById( 'cbp-spmenu-s2' ),
                menuTop = document.getElementById( 'cbp-spmenu-s3' ),
                menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
                showLeft = document.getElementById( 'showLeft' ),
                showRight = document.getElementById( 'showRight' ),
                showTop = document.getElementById( 'showTop' ),
                showBottom = document.getElementById( 'showBottom' ),
                showLeftPush = document.getElementById( 'showLeftPush' ),
                showRightPush = document.getElementById( 'showRightPush' ),
                body = document.body;

           
            showRight.onclick = function() {
                classie.toggle( this, 'active' );
                classie.toggle( menuRight, 'cbp-spmenu-open' );
                
            };
            
           

            
        </script>
        



	</body>
</html>
