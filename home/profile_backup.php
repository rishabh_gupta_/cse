

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" href="main.css" type="text/css"/>
        <link href="main.js" type="text/javascript"/>
        <script type="text/javascript" ="js/jquery-1.4.2.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-type" content="text/html" charset="utf-8"/>
        <meta http-equiv="content-type" content="cache"/>
        <meta name ="robots" content="index,follow"/>
        <meta name="keywords" content="enter contents"/>
        <meta name="description"  content="describe here"/>
        
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <script src="js/modernizr.custom.js"></script>
		<title>StageSpace</title>
	</head>
	<body>
        <!--Header included here-->
		<?php include('../includes/header_mainpage.php');?>
        <!--Header ends here-->
        
        <!--left main menu is included here-->
        <?php include('../includes/left_menu.php');?>
        <!--Left Menu Ends Here-->

       
        <div id= "body-gen-main">
           <div id="body-gen-left">
               <div class="left-container">
                   <div class="profile-picture"></div>
                   <p class="user-name-profile">User Name</p>
                   <div class="follow-btn">
                     <i class="fa fa-user-plus">&nbsp;&nbsp;Know him?</i>
                   </div>
                   <div class="profile-picture-footer">
                       <i class="fa fa-users" style="float:left;margin-right:20px;margin-left:30px;">&nbsp;&nbsp;5</i>
                       <i class="fa fa-file-o" style="float:left;margin-right:20px;">&nbsp;&nbsp;2</i>
                       <i class="fa fa-heart-o" style="float:left;margin-right:20px;">&nbsp;&nbsp;12</i>
                       <i class="fa fa-certificate" style="float:left;margin-right:20px;">&nbsp;&nbsp;2</i>
                   </div>
               </div>
               <div class="left-container" style="margin-top:15px">
                   <p style="color:#666;text-align:center;">
                     Hi, i am user name, final year CSE student from Dehradun.
                     Hi, i am user name, final year CSE student from Dehradun.
                     Hi, i am user name, final year CSE student from Dehradun.
                   </p>
               </div>
               <div class="left-container" style="margin-top:15px">
                   <p class="contact">Contact</p>
                   <p class="contact-item"><i class="fa fa-envelope-o">&nbsp;&nbsp;username@srmuniv.edu.in</i></p>
                   <p class="contact-item"><i class="fa fa-facebook">&nbsp;&nbsp;facebook.com/username</i></p>
                   <p class="contact-item" style="border-bottom:0;"><i class="fa fa-mobile">&nbsp;&nbsp;+91 9962263227</i></p>
               </div>
           </div>
           <div id="body-gen-right">
                <div class="search-container">
                    <p><i class="fa fa-paper-plane">&nbsp;&nbsp;Send</i></p>
                    <input type="text" placeholder="Send Message" class="send-box"/>
                </div>
                <div class="connection-container">
                    <div class="user-node"></div>
                    <p class="arrow">-------- 1 -------></i></p>
                    <div class="user-node"></div>
                </div>
                <div class="right-container" style="margin-top:15px">
                   <p class="contact" style="font-size:14px;"><i class="fa fa-info-circle">&nbsp;&nbsp;About</i></p>
                   <p class="contact-item" style="border-bottom:0;font-size:14x;colot:#979797;"><i class="fa fa-birthday-cake">&nbsp;&nbsp;Date of Birth:&nbsp;&nbsp;&nbsp;&nbsp;11-12-1991</i></p>
                   <p class="contact-item" style="border-bottom:0;font-size:14px;colot:#979797;"><i class="fa fa-circle-thin">&nbsp;&nbsp;&nbsp;Gender:&nbsp;&nbsp;&nbsp;&nbsp;Male</i></p>
                   <p class="contact-item" style="border-bottom:0;font-size:14px;colot:#979797;"><i class="fa fa-university">&nbsp;&nbsp;Branch:&nbsp;&nbsp;&nbsp;&nbsp;Computer Science</i></p>
                   <p class="contact-item" style="border-bottom:0;font-size:14px;colot:#979797;"><i class="fa fa-credit-card">&nbsp;&nbsp;Reg Number:&nbsp;&nbsp;&nbsp;&nbsp;1031110060</i></p>
                   <p class="contact-item" style="border-bottom:0;font-size:14px;colot:#979797;"><i class="fa fa-home">&nbsp;&nbsp;Hostel:&nbsp;&nbsp;&nbsp;&nbsp;Paari, 629</i></p>
                   <p class="contact-item" style="border-bottom:0;font-size:14px;colot:#979797;"><i class="fa fa-map-marker">&nbsp;&nbsp;&nbsp;&nbsp;Hometown:&nbsp;&nbsp;&nbsp;&nbsp;Chennai</i></p>
                   <p class="contact-item" style="border-bottom:0;font-size:14px;colot:#979797;"><i class="fa fa-file">&nbsp;&nbsp;&nbsp;CGPA:&nbsp;&nbsp;&nbsp;&nbsp;7.96</i></p>
               </div>
                <div class="right-container" style="margin-top:15px; float:right;width:53%;">
                   <p class="contact" style="font-size:14px;"><i class="fa fa-users">&nbsp;&nbsp;Connections</i></p>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div style="clear:both;"></div>
               </div>
               <div style="clear:both;"></div>
               <div class="interest-cards-container" style="margin-top:15px">
                   <p class="contact" style="font-size:14px;"><i class="fa fa-star">&nbsp;&nbsp;Interest Cards</i></p>
                   <div class="interest-card" style="margin-right:0px;">
                       <p class="interest-card-header">Interest Name</p>
                       <i class="fa fa-heart">&nbsp;&nbsp;219</i>
                       <div class="interest-card-img"></div>
                       <p class="interest-card-desp">Lates News of the interest description goes here.</p>
                   </div>
                  
                   <div style="clear:both"></div>
               </div>
           </div>

        </div>
        <!--
        <div id="footer">
            <p> Copyright</p>
        </div>
        -->
        <script src="js/classie.js"></script>
        <script>
            var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
                menuRight = document.getElementById( 'cbp-spmenu-s2' ),
                menuTop = document.getElementById( 'cbp-spmenu-s3' ),
                menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
                showLeft = document.getElementById( 'showLeft' ),
                showRight = document.getElementById( 'showRight' ),
                showTop = document.getElementById( 'showTop' ),
                showBottom = document.getElementById( 'showBottom' ),
                showLeftPush = document.getElementById( 'showLeftPush' ),
                showRightPush = document.getElementById( 'showRightPush' ),
                body = document.body;

           
            showRight.onclick = function() {
                classie.toggle( this, 'active' );
                classie.toggle( menuRight, 'cbp-spmenu-open' );
                
            };
            
           

            
        </script>
        



	</body>
</html>
