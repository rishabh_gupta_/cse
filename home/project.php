<?php
  ini_set('display_errors', true);
  error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../includes/session.php");?>
<?php require_once("../includes/functions.php");?>
<?php require_once("../includes/connection_stagespacedb.php");?>
<?php require_once("../includes/neo4jfunctions.php");?>
<?php
  //get users data srom session
  if(!isset($_SESSION['user_id']))
  {
    redirect_to("../index.php");
  } 
  $id_num=$_SESSION['user_id'];
  $user_name=$_SESSION['user_name'];


  
  if(isset($_GET['pid']))
  {
    $pid=$_GET['pid'];
    $project_name=get_project_name($connection,$pid);
  }

  if(isset($_POST['post']))
  {
    $post = $_POST['post-matter'];
    $tag=$project_name;
    $post_id= insertPostDB($connection,$id_num,$post,$tag);
    createProjectPost_rel($post_id,$pid);

    //start form processing
  }
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" href="main.css" type="text/css"/>
        <link href="main.js" type="text/javascript"/>
        <script type="text/javascript" ="js/jquery-1.4.2.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-type" content="text/html" charset="utf-8"/>
        <meta http-equiv="content-type" content="cache"/>
        <meta name ="robots" content="index,follow"/>
        <meta name="keywords" content="enter contents"/>
        <meta name="description"  content="describe here"/>
        
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <script src="js/modernizr.custom.js"></script>
		<title>StageSpace</title>
	</head>
	<body>
        <!--Header included here-->
		<?php include('../includes/header_mainpage.php');?>
        <!--Header ends here-->
        
        <!--left main menu is included here-->
        <?php include('../includes/left_menu.php');?>
        <!--Left Menu Ends Here-->

        <div id="cover">
            <div id="coverpic">
                <p class="user-name-logo"><?php echo $project_name;?></p>
            </div>
            <div id="coverbox">
                 <?php 
                   $people=GET_PROJECT_MEMBERS($pid);
                   foreach ($people as $row)
                    {
                        $value =$row[0]->getProperty('sid');
                        if($value==NULL)
                        {
                          $value =$row[0]->getProperty('fid');
                        }
                        $image=get_profile_img($connection,$value);
                        $user_pic='<img class="user_pic_index" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
                        $output ='
                          <a href="profile.php?user_id='.$value.'">
                            <div class="user_pic">
                                '.$user_pic.'
                            </div>
                          </a>
                        ';
                        echo $output;
                    }
                   ?>
                   <!--
                <div class="user_pic"></div>
                <div class="user_pic"></div>
                <div class="user_pic"></div>
                <div class="user_pic"></div>
                <div class="user_pic"></div>
                <div class="user_pic"></div>
                <div class="user_pic"></div>
                <div class="user_pic"></div>
                <div class="user_pic"></div>
                -->
            </div>
        </div>
        <div style="clear:both"></div>
        <div id= "mid1">
            <div class="profile_pic">
              <?php
                        
                  $image=get_profile_img($connection,$id_num);
            
                  echo '<img class="pic-index" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
               ?>
            </div>
            <div class="h-mid-btn" style="margin-left:10px;">
                <a href="#">
                    <div class="h-mid-link-btn font-color-dark-grey">
                        <i class="fa fa-clock-o">&nbsp;&nbsp;Timeline</i>
                    </div>
                </a>
            </div>
        </div>
        <div id= "body-main">
           <div id="body-left">
            <?php
              $interest_list=getInterestList_new($connection);
              $postBox='
                <div id="post-container">
                  <form name="post-form" method="post" style="float:right;" >
                   <h3 class="container-header">Post</h3>
                   <textarea row="5" col="50" name="post-matter" maxlength="500" id="post" class="post"></textarea> 
                   <div class="post-footer">
                      <p class="comment" style="margin-top:6px;">
                         <i class="fa fa-tag">&nbsp;&nbsp;Tag: '.$tag.'</i>
                      </p>
                      
                       
                     <div class="post-btn">
                         <i class="fa fa-paper-plane-o">&nbsp;&nbsp;
                         
                           <input type=\'submit\' name="post" id="post-btn" value="Post">
                          
                         </i>
                     </div>
                    </form>
                   </div>
               </div>
              ';
              if(!isset($_GET['user_id']))
                echo $postBox;


            ?>
               
               <?php
                  //get room of that user
                  
                  $posts = GET_PROJECT_POSTS($pid);
                  foreach ($posts as $post) 
                  {
                     $post_id=$post[0]->getProperty('id');
                     $result_set=getPost($connection,$post_id);
                     if($post = mysqli_fetch_array($result_set,MYSQLI_ASSOC))
                      {
                         $from_id=$post['FROMID'];
                         $from_image=get_profile_img($connection,$from_id);
                         $from_name=get_user_name($connection,$from_id);
                         $from_time=$post['TIME'];
                         $from_post=$post['POST'];
                         $from_tag=$post['TAG'];
                         $output= '
                           <div class="reg-post-container">
                               <div class="reg-post-header">
                                   <div class="reg-post-user-pic">
                                      <img class="pic-post" src="data:image/jpg;base64,' .  base64_encode($from_image)  . '" />
                                   </div>
                                   <p class="reg-post-user-name">'.$from_name.'</p>
                                   <p class="reg-post-date">on '.$from_time.'</p>
                                   <div style="clear:both;"></div>
                                   <div class="reg-post-box">'.$from_post.'</div>
                                   <div class="reg-post-footer">
                                       <p class="comment"><i class="fa fa-tag">&nbsp;&nbsp;'.$from_tag.'</i></p>
                                       <p class="like"><i class="fa fa-thumbs-o-up">&nbsp;&nbsp;Like</i></p>
                                   </div>
                               </div>
                           </div>
                         '; 
                         echo $output;
                      }
                  }
                      
                  
               ?>
                <!--
                <div class="reg-post-container">
                   <div class="reg-post-header">
                       <div class="reg-post-user-pic"></div>
                       <p class="reg-post-user-name">User Name</p>
                       <p class="reg-post-date">on 15th January, 2014</p>
                       <div style="clear:both;"></div>
                       <div class="reg-post-box">This is a text post on StageSpace.</div>
                       <div class="reg-post-footer">
                           <p class="comment"><i class="fa fa-comment-o">&nbsp;&nbsp;Comment</i></p>
                           <p class="like"><i class="fa fa-thumbs-o-up">&nbsp;&nbsp;Like</i></p>
                       </div>
                   </div>
               </div>
               -->
           </div>
           <div id="body-right">
               <div id="events">
                   <div class="events-header">
                        <p>Events</p>
                   </div>
                   <div class="events-body">
                       <div class="event-container">
                           <div class="reg-post-user-pic"></div>
                           <p class="reg-post-user-name">Event Name</p>
                           <p class="reg-post-date">on 15th January, 2014</p>
                            <div style="clear:both"></div>
                            <div class="event-footer">
                                 <p class="tags"><i class="fa fa-tag">&nbsp;&nbsp;Tags</i></p>
                            </div>
                       </div>
                        <div class="event-container">
                           <div class="reg-post-user-pic"></div>
                           <p class="reg-post-user-name">Event Name</p>
                           <p class="reg-post-date">on 15th January, 2014</p>
                            <div style="clear:both"></div>
                            <div class="event-footer">
                                 <p class="tags"><i class="fa fa-tag">&nbsp;&nbsp;Tags</i></p>
                            </div>
                       </div>
                   </div>
               </div>
           </div>
        </div>
        <!--
        <div id="footer">
            <p> Copyright</p>
        </div>
        -->
        <script src="js/classie.js"></script>
        <script>
            var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
                menuRight = document.getElementById( 'cbp-spmenu-s2' ),
                menuTop = document.getElementById( 'cbp-spmenu-s3' ),
                menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
                showLeft = document.getElementById( 'showLeft' ),
                showRight = document.getElementById( 'showRight' ),
                showTop = document.getElementById( 'showTop' ),
                showBottom = document.getElementById( 'showBottom' ),
                showLeftPush = document.getElementById( 'showLeftPush' ),
                showRightPush = document.getElementById( 'showRightPush' ),
                body = document.body;

           
            showRight.onclick = function() {
                classie.toggle( this, 'active' );
                classie.toggle( menuRight, 'cbp-spmenu-open' );
                
            };
            
           

            
        </script>
        



	</body>
</html>
