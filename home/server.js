var mongo = require('mongodb').MongoClient,
	client = require('socket.io').listen(8080).sockets,
	col_name="msg";

mongo.connect('mongodb://127.0.0.1/stagespace_chat',function(err, db){

	if(err) throw err;
	
	client.on('connection',function(socket){
		//connecting to mongo collection
		
		socket.on('user',function(data){
			var id_one=data.FromID,
				id_two=data.ToID;
			if(id_one<id_two)
			{
				col_name="msg_"+id_one+"_"+id_two;
				//console.log(col_name);
			}
			else
			{
				col_name="msg_"+id_two+"_"+id_one;
				//onsole.log(id_two+"@"+id_one);
				//console.log(col_name);
			}
			
			
		   
		});
		//console.log(col_name);
		//console.log(col_name);
		console.log(col_name);
		var col= db.collection(col_name),
			sendStatus=function(s){
				socket.emit('status',s);
			};
		//emit all messages
		col.find().limit(100).sort({_id:1}).toArray(function(err,res){
			if(err) throw err;
			socket.emit('output',res);
		});
		//wait for input
		socket.on('input',function(data){
			var fromID= data.FromID,
				toID= data.ToID,
				msg=data.mesg,
				whitespacePattern=/^\s*$/;
			if(whitespacePattern.test(fromID) || whitespacePattern.test(toID) || whitespacePattern.test(msg))
			{
				sendStatus('Message required.');
				//console.log('Invalid Input.');
			}
			else
			{
				col.insert({FromID:fromID,ToID:toID,mesg:msg},function(){
					//emit latest message to all clients
					client.emit('output',[data]);
					sendStatus({
						mesg:"Sent",
						clear:true
					});
					//console.log('Inserted');
				});
			}
			

		});
	});

});

