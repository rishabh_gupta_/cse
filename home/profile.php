<?php
  ini_set('display_errors', true);
  error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../includes/session.php");?>
<?php require_once("../includes/functions.php");?>
<?php require_once("../includes/connection_stagespacedb.php");?>
<?php require_once("../includes/neo4jfunctions.php");?>
<?php
  //get users data srom session
  if(!isset($_SESSION['user_id']))
  {
    redirect_to("../index.php");
  } 
  $id_num=$_SESSION['user_id'];
  $user_name=$_SESSION['user_name'];
  if(isset($_GET['user_id']))
  {
    $id_num=$_GET['user_id'];
    $user_name=get_user_name($connection,$id_num);
    //find if a=[:KNOWS]-b
    $knows= IF_KNOWS($_SESSION['user_id'],$_GET['user_id']);
    $cWeight=GET_KNOWS_WEIGHT($_SESSION['user_id'],$_GET['user_id'],$weight);
  }
  if(isset($_POST['rate']))
  {
    //modify relationship value
    $weight=$_POST['selector'];
    $fromID=$_SESSION['user_id'];
    $toID=$_GET['user_id'];
    SET_KNOWS_WEIGHT($fromID,$toID,$weight);

  }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" href="main.css" type="text/css"/>
        <link href="main.js" type="text/javascript"/>
        <script type="text/javascript" ="js/jquery.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-type" content="text/html" charset="utf-8"/>
        <meta http-equiv="content-type" content="cache"/>
        <meta name ="robots" content="index,follow"/>
        <meta name="keywords" content="enter contents"/>
        <meta name="description"  content="describe here"/>
        
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <script src="js/modernizr.custom.js"></script>
		<title>StageSpace</title>
	</head>
	<body>
        <!--Header included here-->
		<?php include('../includes/header_mainpage.php');?>
        <!--Header ends here-->
        
        <!--left main menu is included here-->
        <?php include('../includes/left_menu.php');?>
        <!--Left Menu Ends Here-->

       
        <div id= "body-gen-main">
           <div id="body-gen-left">
               <div class="left-container">
                   <div class="profile-picture">
                    <?php
                        
                        $image=get_profile_img($connection,$id_num);
                  
                        echo '<img class="pic" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
                     ?>
                    </a>
                   </div>
                   <p class="user-name-profile"><?php echo $user_name;?></p>
                   <div class="follow-btn">
                    
                        <?php
                          if(!isset($_GET['user_id']))
                          {
                            echo " <i class=\"fa fa-cog\">&nbsp;&nbsp;";
                            echo 'View Settings';
                          }
                          else
                          {
                            echo '<a style="color:white;" href="index.php?user_id='.$_GET['user_id'].'" >';
                            echo " <i class=\"fa fa-clock-o\">&nbsp;&nbsp;";
                            echo 'View Timeline</i>';
                            echo '</a>';
                          }
                        ?>
                     </i>
                   </div>
                   <div class="profile-picture-footer">
                       <i class="fa fa-users" style="float:left;margin-right:20px;margin-left:30px;">&nbsp;&nbsp;
                          <?php
                            echo GET_KNOWS_COUNT_STUDENT($id_num);
                          ?>
                       </i>
                       <i class="fa fa-file-o" style="float:left;margin-right:20px;">&nbsp;&nbsp;
                          <?php
                            echo  GET_PROJECT_COUNT($id_num);
                          ?>
                       </i>
                       <i class="fa fa-heart-o" style="float:left;margin-right:20px;">&nbsp;&nbsp;
                          <?php
                            echo GET_INTEREST_COUNT($id_num);
                          ?>
                       </i>
                       <i class="fa fa-certificate" style="float:left;margin-right:20px;">&nbsp;&nbsp;0</i>
                   </div>
               </div>
               <?php

                if(isset($_GET['user_id']) && $knows=="1")
                {
                  
                  for($i=0;$i<$cWeight;$i++)
                  {
                    $string .='<i class="fa fa-star star_active"></i>';
                  }
                  $left=5-$cWeight;
                 
                  for($j=0;$j<$left;$j++)
                  {
                     $string .='<i class="fa fa-star"></i>';
                  }
                  $output='
                        <div class="left-container" style="margin-top:15px;">
                         <p class="contact">Rating</p>
                         <div class="stars"> 
                            '.$string.'
                         </div>
                          
                        <div style="clear:both"></div>
                         <div class="profile-picture-footer" style="padding:2px 2px; height:20px;">
                            <p class="comment" style="margin-top:0px;">
                               <i class="fa fa-star"></i>
                            </p>
                            <form name="rate-form" method="post" >
                            <div style="float:left;margin-top:opx;">
                              <select id="tag-selector" name="selector" style="margin-top:1px;">
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                              </select>
                            </div>
                            <div class="post-btn" style="margin-top:2px; margin-right5px;padding:0;">
                                 <i class="fa fa-flag">&nbsp;&nbsp;
                                 
                                   <input type=\'submit\' name="rate" id="post-btn" value="Rate">
                                  
                                 </i>
                            </div>
                            </form>
                            <div style="clear:both"></div>
                         </div>
                        </div>
                        ';
                  echo $output;
                }
               ?>
               <div class="left-container" style="margin-top:15px">
                   <p style="color:#666;text-align:center;">
                     Hi, i am <?php echo $user_name?>. Final year Computer Science and Engineering Student.
                     I am currently living in chennai and i love to code.
                   </p>
               </div>
               <div class="left-container" style="margin-top:15px">
                   <p class="contact">Contact</p>
                   <p class="contact-item">
                      <i class="fa fa-envelope-o">&nbsp;&nbsp;
                          <?php echo strtolower($user_name);?>@srmuniv.edu.in
                      </i>
                   </p>
                   <p class="contact-item">
                      <i class="fa fa-facebook">&nbsp;&nbsp;facebook.com/
                          <?php echo strtolower($user_name);?>
                      </i>
                   </p>
                   <p class="contact-item" style="border-bottom:0;"><i class="fa fa-mobile">&nbsp;&nbsp;+91 9962263227</i></p>
               </div>
                <div class="left-container" style="margin-top:15px;padding-bottom:3px;">
                   <p class="contact">Mutual Friends</p>
                    <?php 
                      if(isset($_GET['user_id']))
                      {
                         $people=GET_MUTUAL_CON($_GET['user_id'],$_SESSION['user_id']);
                         foreach ($people as $row)
                          {
                              $value =$row[0]->getProperty('sid');
                              if($value==NULL)
                              {
                                $value =$row[0]->getProperty('fid');
                              }
                              if(isset($_GET['user_id']))
                              {
                                if($value!=$_GET['user_id'])
                                {
                                  $image=get_profile_img($connection,$value);
                                    $user_pic='<img class="user_pic_box" style="width:50px;height:50px;" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
                                    $output ='
                                      <a href="profile.php?user_id='.$value.'">
                                        <div class="user_pic_pro" style="width:50px;height:50px;">
                                            '.$user_pic.'
                                        </div>
                                      </a>
                                    ';
                                    echo $output;
                                }
                              }
                             
                          }

                      }
                      else
                      {
                        echo '<p>its just you</p>';
                      }

                       
                   ?>
                   <div style="clear:both;"></div>
                 
               </div>
           </div>
           <div id="body-gen-right">
                <div class="search-container">
                    <p><i class="fa fa-paper-plane">&nbsp;&nbsp;Send</i></p>
                    <input type="text" placeholder="Send Message" class="send-box"/>
                </div>
                <div class="connection-container">
                    <div class="user-node">
                      <?php
                    
                          $image=get_profile_img($connection,$id_num_header);
                          echo '<img class="connect-pic" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
                      ?>
                    </div>
                    <?php
                      if(!isset($_GET['user_id']) || $_GET['user_id']==$id_num_header)
                      {
                        $output= '
                          <p class="arrow">Its just you.</p>
                        ';
                        echo $output;
                      }
                      else
                      {
                        $connect_id = $_GET['user_id'];
                        $deg=CHECK_FIRST_DEG($id_num_header,$id_num);
                        if($deg==1)
                        {
                            $image=get_profile_img($connection,$connect_id);
                            $output = '
                              <p class="arrow">------ 1 -----></i></p>
                              <div class="user-node">
                                <img class="connect-pic" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />;
                              </div>
                          ';
                          echo $output;
                        }
                        else
                        { 
                          $path=FIND_SHORTEST_PATH($id_num_header,$id_num);
                          
                          $value = GET_SHORTEST_PATH_COUNT($id_num_header,$id_num);
                          if(strlen($id_num)==9)
                          {
                              $value=$value-1;
                          }
                          foreach ($path as $row) 
                          {
                              for($count= 1; $count<=$value;$count++)
                              {
                                  $connect_id= $row[0][$count];
                                  
                                  $image=get_profile_img($connection,$connect_id);
                                  $degree=$count;
                                  $output = '
                                      <p class="arrow">------ '.$degree.' -----></i></p>
                                      <div class="user-node">
                                        <img class="connect-pic" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />;
                                      </div>
                                  ';
                                  echo $output;
                                
                              }
                          }
                          if(strlen($id_num)==9)
                          {
                             $degree=$value+1;
                             $image_last=get_profile_img($connection,$id_num);
                             $output_last = '
                                  <p class="arrow">------ '.$degree.' -----></i></p>
                                  <div class="user-node">
                                    <img class="connect-pic" src="data:image/jpg;base64,' .  base64_encode($image_last)  . '" />;
                                  </div>
                              ';
                            echo $output_last;
                          }
                        
                          
                        }
                       
                        
                      }
                      /*
                      $image=get_profile_img($connection,$id_num);
                      echo '<img class="connect-pic" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
                       $output= '
                          <p class="arrow">-------- 1 -------></i></p>
                          <div class="user-node"></div>
                       ';
                       */
                    ?>
                </div>
                <div class="right-container" style="margin-top:15px">
                   <p class="contact" style="font-size:14px;"><i class="fa fa-info-circle">&nbsp;&nbsp;About</i></p>
                   <p class="contact-item" style="border-bottom:0;font-size:14x;colot:#979797;">
                     <i class="fa fa-birthday-cake">&nbsp;&nbsp;Date of Birth:&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo get_dob($connection,$id_num);?>
                     </i>
                   </p>
                   <p class="contact-item" style="border-bottom:0;font-size:14px;colot:#979797;">
                     <i class="fa fa-circle-thin">&nbsp;&nbsp;&nbsp;Gender:&nbsp;&nbsp;&nbsp;&nbsp;
                         <?php echo get_gender($connection,$id_num);?>
                     </i>
                   </p>
                   <p class="contact-item" style="border-bottom:0;font-size:14px;colot:#979797;">
                      <i class="fa fa-university">&nbsp;&nbsp;Branch:&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo get_course($connection,$id_num);?>
                      </i>
                    </p>
                   <p class="contact-item" style="border-bottom:0;font-size:14px;colot:#979797;">
                      <i class="fa fa-credit-card">&nbsp;&nbsp;Reg Number:&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo $id_num;?>
                      </i>
                   </p>
                   <p class="contact-item" style="border-bottom:0;font-size:14px;colot:#979797;">
                     <i class="fa fa-home">&nbsp;&nbsp;Hostel:&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo get_college_add($connection,$id_num);?>
                     </i>
                   </p>
                   <p class="contact-item" style="border-bottom:0;font-size:14px;colot:#979797;">
                     <i class="fa fa-map-marker">&nbsp;&nbsp;&nbsp;&nbsp;Hometown:&nbsp;&nbsp;&nbsp;&nbsp;
                         <?php echo get_hometown($connection,$id_num);?>
                     </i>
                   </p>
                   <p class="contact-item" style="border-bottom:0;font-size:14px;colot:#979797;">
                     <i class="fa fa-file">&nbsp;&nbsp;&nbsp;CGPA:&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo get_cgpa($connection,$id_num);?>
                     </i>
                   </p>
               </div>
                <div class="right-container" style="margin-top:15px; float:right;width:53%;">
                   <p class="contact" style="font-size:14px;"><i class="fa fa-users">&nbsp;&nbsp;Connections</i></p>
                   <?php 
                       $people=GET_KNOWS($id_num);
                       foreach ($people as $row)
                        {
                            $value =$row[0]->getProperty('sid');
                            if($value==NULL)
                            {
                              $value =$row[0]->getProperty('fid');
                            }
                            if(!isset($_GET['user_id']))
                            {
                              if($value!=$_SESSION['user_id'])
                              {
                                  $image=get_profile_img($connection,$value);
                                  $user_pic='<img class="user_pic_box" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
                                  $output ='
                                    <a href="profile.php?user_id='.$value.'">
                                      <div class="user_pic_pro">
                                          '.$user_pic.'
                                      </div>
                                    </a>
                                  ';
                                  echo $output;
                              }
                            }
                            if(isset($_GET['user_id']))
                            {
                              if($value!=$_GET['user_id'])
                              {
                                $image=get_profile_img($connection,$value);
                                  $user_pic='<img class="user_pic_box" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
                                  $output ='
                                    <a href="profile.php?user_id='.$value.'">
                                      <div class="user_pic_pro">
                                          '.$user_pic.'
                                      </div>
                                    </a>
                                  ';
                                  echo $output;
                              }
                            }
                           
                        }
                   ?>
                   <!--
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   <div class="user_pic_pro"></div>
                   -->
                   <div style="clear:both;"></div>
               </div>
               <div style="clear:both;"></div>
               <div class="interest-cards-container" style="margin-top:15px">
                   <p class="contact" style="font-size:14px;"><i class="fa fa-star">&nbsp;&nbsp;Interest Cards</i></p>
                   <?php
                      $interests=GET_INTERESTS($id_num);
                      foreach ($interests as $row)
                      {

                          $value =$row[0]->getProperty('name');
                          $number_std = GET_STUDENT_COUNT_INTEREST($value);
                          $src = "images/interests/cooking.jpg";
                          $src = get_interest_img($connection,$value);
                          $output ='
                             <div class="interest-card" style="margin-right:0px;">
                                 <p class="interest-card-header">'.$value.'</p>
                                 <i class="fa fa-heart">&nbsp;&nbsp;'.$number_std.'</i>
                                 <div class="interest-card-img">
                                    <img src="'.$src.'" class="interest-img"/>
                                 </div>
                                 <p class="interest-card-desp">This interest deal with people who like to do this and that.</p>
                             </div>
                          ';
                          echo $output;
                      }
                   ?>
                   <!--
                   <div class="interest-card" style="margin-right:0px;">
                       <p class="interest-card-header">Interest Name</p>
                       <i class="fa fa-heart">&nbsp;&nbsp;219</i>
                       <div class="interest-card-img"></div>
                       <p class="interest-card-desp">Lates News of the interest description goes here.</p>
                   </div>
                  -->
                   <div style="clear:both"></div>
               </div>
           </div>

        </div>
        <!--
        <div id="footer">
            <p> Copyright</p>
        </div>
        -->
        <script src="js/classie.js"></script>
        <script>
            var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
                menuRight = document.getElementById( 'cbp-spmenu-s2' ),
                menuTop = document.getElementById( 'cbp-spmenu-s3' ),
                menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
                showLeft = document.getElementById( 'showLeft' ),
                showRight = document.getElementById( 'showRight' ),
                showTop = document.getElementById( 'showTop' ),
                showBottom = document.getElementById( 'showBottom' ),
                showLeftPush = document.getElementById( 'showLeftPush' ),
                showRightPush = document.getElementById( 'showRightPush' ),
                body = document.body;

           
            showRight.onclick = function() {
                classie.toggle( this, 'active' );
                classie.toggle( menuRight, 'cbp-spmenu-open' );
                
            };
            
           

            
        </script>
        



	</body>
</html>
