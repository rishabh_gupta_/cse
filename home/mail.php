<?php
  ini_set('display_errors', true);
  error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../includes/session.php");?>
<?php require_once("../includes/functions.php");?>
<?php require_once("../includes/connection_stagespacedb.php");?>
<?php require_once("../includes/neo4jfunctions.php");?>
<?php
  //get users data srom session
  if(!isset($_SESSION['user_id']))
  {
    redirect_to("../index.php");
  } 
  $id_num=$_SESSION['user_id'];
  $fromID=$_SESSION['user_id'];
  $nameFROM=get_user_name($connection,$id_num);
  $user_name=$_SESSION['user_name'];
  if(isset($_GET['user_id']))
  {
    $fromID=$_SESSION['user_id'];
    $toID=$_GET['user_id'];
    $msgs=get_messages($connection,$fromID,$toID);
    
    //$id_num=$_GET['user_id'];
    //$user_name=get_user_name($connection,$id_num);
    //find if a=[:KNOWS]-b
    //$knows= IF_KNOWS($_SESSION['user_id'],$_GET['user_id']);
    //$cWeight=GET_KNOWS_WEIGHT($_SESSION['user_id'],$_GET['user_id'],$weight);
  }
  if(isset($_POST['send']))
  {
    //modify relationship value
    $fromID=$_SESSION['user_id'];
    $toID=$_GET['user_id'];
    $msg=$_POST['msg'];
    //echo $fromID." ".$toID." ".$msg;
    insertMsgDB($connection,$fromID,$toID,$msg);
    redirect_to('mail.php?user_id='.$toID);

  }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" href="main.css" type="text/css"/>
    <link rel="stylesheet" href="chat.css" type="text/css"/>
        <link href="main.js" type="text/javascript"/>
        <script type="text/javascript" ="js/jquery-1.4.2.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-type" content="text/html" charset="utf-8"/>
        <meta http-equiv="content-type" content="cache"/>
        <meta name ="robots" content="index,follow"/>
        <meta name="keywords" content="enter contents"/>
        <meta name="description"  content="describe here"/>
        <script type="text/javascript" src="../javascripts/basic.js"></script>
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <script src="js/modernizr.custom.js"></script>
		<title>StageSpace</title>
	</head>
	<body>
        <!--Header included here-->
		<?php include('../includes/header_mainpage.php');?>
        <!--Header ends here-->
        
        <!--left main menu is included here-->
        <?php include('../includes/left_menu.php');?>
        <!--Left Menu Ends Here-->

       
        <div id= "body-gen-main" onload="loadImage()">
           <div id="body-gen-left">
               <div class="left-container" style="height:525px;overflow-y:auto;background-color:whitesmoke;">
                <?php 
                   $people=GET_KNOWS($id_num);
                   foreach ($people as $row)
                    {

                        $value =$row[0]->getProperty('sid');
                        if($value==NULL)
                        {
                          $value =$row[0]->getProperty('fid');
                        }
            
                        if($value!=$_SESSION['user_id'])
                        {
                            if(!isset($_GET['user_id']) )
                            {
                              $toID=$value;
                              $nameTO=get_user_name($connection,$value);
                            }
                            $sel_class="";
                            $sel_p="";
                            if(isset($_GET['user_id']) && $_GET['user_id']==$value)
                            {
                              $sel_class='style="background-color:#26A69A;"';
                              $sel_p='style="color:white;"';
                            }
                            $name=get_user_name($connection,$value);
                            $image=get_profile_img($connection,$value);
                            $user_pic='<img class="user_pic_box" style="width:50px;height:50px;" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
                            $output ='
                                <a href="mail.php?user_id='.$value.'" >
                                 <div id="chat-null" class="user-chat" '.$sel_class.'>
                                   <div class="user-chat-img">'.$user_pic.'</div>
                                   <div class="user-chat-info">
                                     <p class="user-chat-name" '.$sel_p.'>'.$name.'</p>
                                     <p class="user-chat-last" '.$sel_p.'>Last Message</p>
                                   </div>
                                 </div>
                                </a>
                            
                            ';
                            echo $output;
                        
                        }
                      }

                    ?>
                  </div>
              
           </div>
           <div id="body-gen-right">
                <div class="interest-cards-container" style="margin-top:0px;background-color:whitesmoke;margin-bottom:15px;height:450px;overflow-y:auto;border:0;">
                  <?php
                    if(isset($msgs))
                    {
                      while($msg= mysqli_fetch_array($msgs, MYSQLI_ASSOC))
                      {
                        $message=$msg['msg'];
                        $from_id_num=$msg['FromID'];
                        $from_name=get_user_name($connection,$from_id_num);
                        $to_id_num=$msg['ToID'];
                        $output='
                          <div class="chat-message">
                            <p class="chat-names">'.$from_name.':</p>
                            <p>'.$message.'</p>
                          </div>
                        ';
                        echo $output;
                      
                      }
                    }

                  ?>
                </div>
                <div class="status-container"> 
                  <p class="status">Idle</p>
                  <input type="text" class="fromID" style="display:none;" value=<?php echo '"'.$fromID.'"'?> />
                   <input type="text" class="toID" style="display:none;" value=<?php echo '"'.$toID.'"'?> />
                </div>
                <div class="search-container">
                      <form method="post">
                       <input type="text" placeholder="Send Message" name="msg" class="send-box"/>
                       <input type="submit" value="Send" name="send" class="send-btn">
                    </form>
                </div>
               
           </div>

        </div>


        
	</body>
</html>
