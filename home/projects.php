
<?php
  ini_set('display_errors', true);
  error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../includes/session.php");?>
<?php require_once("../includes/functions.php");?>
<?php require_once("../includes/connection_stagespacedb.php");?>
<?php require_once("../includes/neo4jfunctions.php");?>
<?php
  //get users data srom session
  if(!isset($_SESSION['user_id']))
  {
    redirect_to("../index.php");
  } 
  $id_num=$_SESSION['user_id'];
  $user_name=$_SESSION['user_name'];
  $batch= get_batch($connection,$id_num);
  $course= get_course($connection,$id_num);
  $section=get_section($connection,$id_num); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" href="main.css" type="text/css"/>
        <link href="main.js" type="text/javascript"/>
        <script type="text/javascript" ="js/jquery-1.4.2.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-type" content="text/html" charset="utf-8"/>
        <meta http-equiv="content-type" content="cache"/>
        <meta name ="robots" content="index,follow"/>
        <meta name="keywords" content="enter contents"/>
        <meta name="description"  content="describe here"/>
        
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <script src="js/modernizr.custom.js"></script>
		<title>StageSpace</title>
	</head>
	<body>
        <!--Header included here-->
		<?php include('../includes/header_mainpage.php');?>
        <!--Header ends here-->
        
        <!--left main menu is included here-->
        <?php include('../includes/left_menu.php');?>
        <!--Left Menu Ends Here-->

       
        <div id= "body-gen-main">
           <p class="connect-header"><i class="fa fa-pie-chart">&nbsp;&nbsp;Projects</i></p>
           <?php
              $projects=GET_PROJECTS($id_num);
              foreach ($projects as $project)
              {
                $project_id=$project[0]->getProperty('id');
                $project_name=$project[0]->getProperty('name');
               
                $output='
                  <div class="connect-container" style="margin-right:0px; float:left;">
                     <div class="connect-container-header">
                         <div class="connect-container-user"><i class="fa fa-flask "></i></div>
                         <p>'.$project_name.'</p>
                         <i class="fa fa-users">&nbsp;&nbsp;290</i>
                         <i class="fa fa-rss">&nbsp;&nbsp;120</i>
                     </div>
                     <div class="connect-users-container">
                             <p>Members</p>
                ';
                $members =GET_PROJECT_MEMBERS($project_id);
                foreach ($members as $member)
                {
                  $value =$member[0]->getProperty('sid');
                  if($value=="")
                  {
                    $value =$member[0]->getProperty('fid');
                  }
                  //echo $value;
                  $image=get_profile_img($connection,$value);
                  $user_pic='<img class="user_pic_box_connect" src="data:image/jpg;base64,' .  base64_encode($image)  . '" />';
                  $output .='
                     
                  
                            <div class="connect-container-user">
                              <a href="profile.php?user_id='.$value.'">
                                <div class="user_pic_pro">
                                    '.$user_pic.'
                                </div>
                              </a>
                            </div>
                            ';
                }
                $output .= '
                    </div>
                ';
                echo $output;
                 $button ='
                  <a href="project.php?pid='.$project_id.'">
                     <div class="view-btn">
                         <i class="fa fa-external-link">&nbsp;&nbsp;View</i>
                    </div>
                  </a>
                  </div>
                ';
                echo $button;
              }
              
           ?>
        
        </div>
        <!--
        <div id="footer">
            <p> Copyright</p>
        </div>
        -->
        <script src="js/classie.js"></script>
        <script>
            var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
                menuRight = document.getElementById( 'cbp-spmenu-s2' ),
                menuTop = document.getElementById( 'cbp-spmenu-s3' ),
                menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
                showLeft = document.getElementById( 'showLeft' ),
                showRight = document.getElementById( 'showRight' ),
                showTop = document.getElementById( 'showTop' ),
                showBottom = document.getElementById( 'showBottom' ),
                showLeftPush = document.getElementById( 'showLeftPush' ),
                showRightPush = document.getElementById( 'showRightPush' ),
                body = document.body;

           
            showRight.onclick = function() {
                classie.toggle( this, 'active' );
                classie.toggle( menuRight, 'cbp-spmenu-open' );
                
            };
            
           

            
        </script>
        



	</body>
</html>
