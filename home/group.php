<?php
  ini_set('display_errors', true);
  error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../includes/session.php");?>
<?php require_once("../includes/functions.php");?>
<?php require_once("../includes/connection_stagespacedb.php");?>
<?php require_once("../includes/neo4jfunctions.php");?>
<?php
 
  if(isset($_GET['hostel']) && isset($_GET['room']))
  {
    redirect_to("room.php?hostel={$_GET['hostel']}&room={$_GET['room']}");
  }

?>


