// autocomplet : this function will be executed every time we change the text
function autocomplet() {
	
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#student_id').val();
	
	if (keyword.length >= min_length) {
		$.ajax({
			url: 'ajax_refresh.php',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
			
				$('#student_list_id').show();
				$('#student_list_id').html(data);
			}
			
		    //$('.ui-autocomplete').keypress(function(event) {});
		});
	} else {
		$('#student_list_id').hide();
	}
	
}

// set_item : this function will be executed when we select an item
function set_item(item) {
	// change input value
	$('#student_id').val(item);
	// hide proposition list
	$('#student_list_id').hide();
}

$(document).mouseup(function (e)
{
    
    /*
    var container = $("#menu");
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
        document.getElementById('menu').style.display = "none";
        document.getElementById("body").style.overflowY="scroll";
    }
    */

    var cont = $("#student_list_id");
    if (!cont.is(e.target) // if the target of the click isn't the container...
        && cont.has(e.target).length === 0) // ... nor a descendant of the container
    {
        cont.hide();
       

    }
});