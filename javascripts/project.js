
/*
*Ajax funtion to verify if the user id already exists in the database
*logs error if it already exists
*/


function showDiv() {
   document.getElementById('menu').style.display = "block";
  
}
var corr = 4;
function correct_inc()
{
	console.log(corr);
}
function correct_dec()
{
	corr--;
	console.log(corr);
}
function correct()
{	
	$("#signup_btn").attr("disabled", true);
	var found=false;
	if ($(".signup_right").find(".error_msg").length > 0){ 
  		found=true;
  		console.log(found);
  	}
	if(corr<=0 && found===false)
	{
		$("#signup_btn").removeAttr("disabled");
		
	}


}
/*
*function 
*@param value is id of the field
*re
*/
function isPresent(id)
{
	if(document.getElementById(id))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
*fucntion to scroll the right side div to the bottom
*/
function scrollUp()
{
	$(".signup_right").animate({ scrollTop: $(".signup_right")[0].scrollHeight}, 2000);
}
/*
*function removes ok border and adds error border
*@param value is id of the textfield
*/
function set_border_error(value)
{
    if($(value).hasClass("input_ok"))
    {
    	$(value).removeClass("input_ok");
    }
    $(value).addClass("input_error");
}

/*
*function removes error border and adds ok border
*@param value is id of the textfield
*/
function set_border_ok(value)
{
    if($(value).hasClass("input_error"))
    {
    	$(value).removeClass("input_error");
    }
    $(value).addClass("input_ok");
}

/*
*function calls error border and adds div with classand id_error_msg
*removes id_ok_msg if exists before
*@param value is id of the textfield and its value
*/
function set_error_msg_border(value,id)
{	
	var id_msg = id+"_err_msg";
	var id_ok_msg = id+"_ok_msg";
	if(document.getElementById(id_msg)==null)
	{
		$(".signup_right").append("<div class=\"signup_msg error_msg\" id="+id_msg+">Please enter a valid "+value+"</div>");
	}
	if(document.getElementById(id_ok_msg))
	{
		//document.getElementById(id_msg).style.display = "none";
		var element = document.getElementById(id_ok_msg);
		element.outerHTML = "";
		delete element;
		//console.log('remove'+id_ok_msg);
	}
	set_border_error(id);
	$(".signup_right").animate({ scrollTop: $(".signup_right")[0].scrollHeight}, 2000);
}

/*
*function calls error border and adds div with classand id_ok_msg
*removes id_err_msg if exists before
*@param value is id of the textfield and its value
*/
function set_ok_msg_border(value,id)
{
	var id_err_msg = id+"_err_msg";
	var id_ok_msg = id+"_ok_msg";
	if(document.getElementById(id_err_msg))
	{
		//document.getElementById(id_msg).style.display = "none";
		var element = document.getElementById(id_err_msg);
		element.outerHTML = "";
		delete element;
		//console.log('remove'+id_err_msg);
	}
	if(document.getElementById(id_ok_msg))
	{
		//document.getElementById(id_msg).style.display = "none";
		var element = document.getElementById(id_ok_msg);
		element.outerHTML = "";
		delete element;
		//console.log('remove'+id_ok_msg);
	}
	$(".signup_right").append("<div class=\"signup_msg ok_msg\" id="+id_ok_msg+">It is "+value+"</div>");
	set_border_ok(id);
	$(".signup_right").animate({ scrollTop: $(".signup_right")[0].scrollHeight}, 2000);
}
$(document).ready(function()
{
	
	
	$("#title").focus(function()
	{
		var id_name="#title_msg";
		if(!isPresent(id_name))
		{
			$(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please enter the title of your project.</div>");
		}
	    scrollUp();
	    
	});
	$("#title").blur(function()
	{	
		
		var l = document.forms["signup_basic_form"]["title"].value;	
	    if (l == null || l== "") 
	    {
	    	set_error_msg_border('title','#title');
    		correct();
	    }
    	else 
    	{
    		set_ok_msg_border(l,'#title');
    		correct_dec();
    		correct();
    	
    	}
    	
    });
    $("#field").focus(function(){
    	var id_name="#field_msg";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Select the field of your project.</div>");
	 	}
	    scrollUp();
    });
	$("#field").blur(function()
	{	
		var id = document.forms["signup_basic_form"]["field"].value;
	    if (id == null || id== "") 
	    {
	    	set_error_msg_border('field','#field');
    		correct();
    	}
    	else 
    	{	
    		set_ok_msg_border(id,'#field');
    		correct_dec();
    		correct();
    	}
    	
    });
    $("#doc").focus(function(){
    	var id_name="#doc_msg";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">The date of commencement of the project.</div>");
	 	}
	    scrollUp();
    });
	$("#doc").blur(function()
	{	
		var val = document.forms["signup_basic_form"]["doc"].value;

	    if (!val == null || val== "") 
	    {
	    	set_error_msg_border('Date','#doc');
    		correct();
    	}
    	else 
    	{
    		
    		set_ok_msg_border(val,'#doc');
    		correct_dec();
    		correct();
    	}
    	
    });
    $("#student_id").focus(function(){
    	var id_name="#partner_msg";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Select a partner if any.</div>");
	 	}
	    scrollUp();
    });

    $("#section").focus(function(){
    	var id_name="#section_msg";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Describe your project in a few words.</div>");
	 	}
	    scrollUp();
    });
	$("#section").blur(function()
	{	
		var val = document.forms["signup_basic_form"]["section"].value;
	    if (!val == null || val== "") 
	    {
	    	set_error_msg_border('description. Description is Required','#section');
    		correct();
    	}
    	else 
    	{
    		set_ok_msg_border('','#section');
    		correct_dec();
    		correct();
    	}
    	
    });
    $("#r_password").focus(function(){
    	var id_name="#r_password_msg";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Retype to Confirm password.</div>");
	 	}
	    scrollUp();
    });
	$("#r_password").blur(function()
	{	
		var pass = document.forms["signup_basic_form"]["password"].value;
		var r_pass= document.forms["signup_basic_form"]["r_password"].value;
	    if (pass!==r_pass) 
	    {
	    	set_error_msg_border('Password. Passwords does not match.','#r_password');
    		correct();
    	}
    	else 
    	{
    		set_ok_msg_border('a match','#r_password');
    		correct_dec();
    		correct();
    	}
    	
    });

});