$(document).mouseup(function (e)
	{
	    var container = $("#menu");
	    if (!container.is(e.target) // if the target of the click isn't the container...
	        && container.has(e.target).length === 0) // ... nor a descendant of the container
	    {
	        container.hide();
	        document.getElementById('menu').style.display = "none";
	    }
});
/*
*Ajax funtion to verify if the user id already exists in the database
*logs error if it already exists
*/
function verifyID() 
{
	var min_length = 10; // min caracters to display the autocomplete
	var keyword = $('#id_num').val();
	
	if (keyword.length >= min_length) {
		$.ajax({
			url: '../../includes/ajax_verifyID.php',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				console.log(data);
				if(data=='false')
				{
					console.log('user exists');
					set_error_msg_border('Id. User with this id already exists','#id_num');
					correct();
				}
				
			}
			

		});
	} else
	{
		//do nothing 
	}
	
}

function verifyEmailID() 
{
	var min_length = 10; // min caracters to display the autocomplete
	var keyword = $('#email_id').val();
	
	if (keyword.length >= min_length) {
		$.ajax({
			url: '../../includes/ajax_verifyEMAIL.php',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				console.log(data);
				if(data=='false')
				{
					console.log('email exists');
					set_error_msg_border('Id. User with this email id already exists','#email_id');
					correct();
				}
				
			}
			

		});
	} else
	{
		//do nothing 
	}
	
}

function showDiv() {
   document.getElementById('menu').style.display = "block";
  
}
var corr = 7;
function correct_inc()
{
	console.log(corr);
}
function correct_dec()
{
	corr--;
	console.log(corr);
}
function correct()
{	
	$("#signup_btn").attr("disabled", true);
	var found=false;
	if ($(".signup_right").find(".error_msg").length > 0){ 
  		found=true;
  		console.log(found);
  	}
	if(corr<=0 && found===false)
	{
		$("#signup_btn").removeAttr("disabled");
		$(".signup_right").append("<div class=\"signup_msg ok_msg\" id=\"final\">In case any problems from now on is signup precedure login with the email and password provided here you will be redirected to where you left.</div>");
	}


}
/*
*function 
*@param value is id of the field
*re
*/
function isPresent(id)
{
	if(document.getElementById(id))
	{
		return true;
	}
	else
	{
		return false;
	}
}
/*
*function to validate password
^                         Start anchor
(?=.*[A-Z])       		  Ensure string has 1 uppercase letter.
(?=.*[!@#$&*])            Ensure string has one special case letter.
(?=.*[0-9])        		  Ensure string has 1 digits.
(?=.*[a-z])			      Ensure string has three lowercase letters.
.{8}                      Ensure string is of length 8.
$                         End anchor
*returns true or false
*/
function validatePassword(value)
{
	var passReg =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;  
	return passReg.test(value);
}

/*
*function to validate email address
*returns true or false
*/
function validateEmail(email) {
  var emailReg = /^([\w-\.]+@(ktr.srmuniv.ac.in)+)$/;
  return emailReg.test(email);
}

/*
*fucntion to scroll the right side div to the bottom
*/
function scrollUp()
{
	$(".signup_right").animate({ scrollTop: $(".signup_right")[0].scrollHeight}, 2000);
}
/*
*function removes ok border and adds error border
*@param value is id of the textfield
*/
function set_border_error(value)
{
    if($(value).hasClass("input_ok"))
    {
    	$(value).removeClass("input_ok");
    }
    $(value).addClass("input_error");
}

/*
*function removes error border and adds ok border
*@param value is id of the textfield
*/
function set_border_ok(value)
{
    if($(value).hasClass("input_error"))
    {
    	$(value).removeClass("input_error");
    }
    $(value).addClass("input_ok");
}

/*
*function calls error border and adds div with classand id_error_msg
*removes id_ok_msg if exists before
*@param value is id of the textfield and its value
*/
function set_error_msg_border(value,id)
{	
	var id_msg = id+"_err_msg";
	var id_ok_msg = id+"_ok_msg";
	if(document.getElementById(id_msg)==null)
	{
		$(".signup_right").append("<div class=\"signup_msg error_msg\" id="+id_msg+">Please enter a valid "+value+"</div>");
	}
	if(document.getElementById(id_ok_msg))
	{
		//document.getElementById(id_msg).style.display = "none";
		var element = document.getElementById(id_ok_msg);
		element.outerHTML = "";
		delete element;
		//console.log('remove'+id_ok_msg);
	}
	set_border_error(id);
	$(".signup_right").animate({ scrollTop: $(".signup_right")[0].scrollHeight}, 2000);
}

/*
*function calls error border and adds div with classand id_ok_msg
*removes id_err_msg if exists before
*@param value is id of the textfield and its value
*/
function set_ok_msg_border(value,id)
{
	var id_err_msg = id+"_err_msg";
	var id_ok_msg = id+"_ok_msg";
	if(document.getElementById(id_err_msg))
	{
		//document.getElementById(id_msg).style.display = "none";
		var element = document.getElementById(id_err_msg);
		element.outerHTML = "";
		delete element;
		//console.log('remove'+id_err_msg);
	}
	if(document.getElementById(id_ok_msg))
	{
		//document.getElementById(id_msg).style.display = "none";
		var element = document.getElementById(id_ok_msg);
		element.outerHTML = "";
		delete element;
		//console.log('remove'+id_ok_msg);
	}
	$(".signup_right").append("<div class=\"signup_msg ok_msg\" id="+id_ok_msg+">It is "+value+"</div>");
	set_border_ok(id);
	$(".signup_right").animate({ scrollTop: $(".signup_right")[0].scrollHeight}, 2000);
}
$(document).ready(function()
{
	
	$("#first_name").blur(function()
	{
	    var x = document.forms["signup_basic_form"]["first_name"].value;
	    if (x == null || x == "") 
	    {
	    
	        set_error_msg_border('first name','#first_name');
    		correct();

    	}
    	else
    	{
    		set_ok_msg_border(x,'#first_name');
    		correct_dec();
    		correct();

    	}
    	
	});
	$("#middle_name").focus(function()
	{
		var id_name="#middle_name_msg";
		if(!isPresent(id_name))
		{
			$(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please enter your middle name if any.</div>");
		}
	    scrollUp();
	    
	});
	$("#middle_name").blur(function()
	{	
		
    });
    $("#last_name").focus(function()
	{
		var id_name="#last_name_msg";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please enter your last name.</div>");
	 	}
	    scrollUp();
	});
	$("#last_name").blur(function()
	{	
		
		var l = document.forms["signup_basic_form"]["last_name"].value;	
	    if (l == null || l== "") 
	    {
	    	set_error_msg_border('last name','#last_name');
    		correct();
	    }
    	else 
    	{
    		set_ok_msg_border(l,'#last_name');
    		correct_dec();
    		correct();
    	
    	}
    	
    });
    $("#id_num").focus(function(){
    	var id_name="#id_num";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please enter your id number.</div>");
	 	}
	    scrollUp();
    });
	$("#id_num").blur(function()
	{	
		var id = document.forms["signup_basic_form"]["id_num"].value;
	    if (id == null || id== "") 
	    {
	    	set_error_msg_border('Id Number','#id_num');
    		correct();
    	}
    	else 
    	{	var exists= verifyID();
    		console.log(exists);
    		set_ok_msg_border(id,'#id_num');
    		correct_dec();
    		correct();
    	}
    	
    });
    $("#email_id").focus(function(){
    	var id_name="#email_id_msg";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please enter your srmuniv.edu email id.</div>");
	 	}
	    scrollUp();
    });
	$("#email_id").blur(function()
	{	
		var email = document.forms["signup_basic_form"]["email_id"].value;
	    if (!validateEmail(email)) 
	    {
	    	set_error_msg_border('Email srmuniv.edu.in address','#email_id');
    		correct();
    	}
    	else 
    	{
    		var exists= verifyEmailID();
    		console.log(exists);
    		set_ok_msg_border(email,'#email_id');
    		correct_dec();
    		correct();
    	}
    	
    });
    $("#r_email_id").focus(function(){
    	var id_name="#r_email_id_msg";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Confirm email id.</div>");
	 	}
	    scrollUp();
    });
	$("#r_email_id").blur(function()
	{	
		var email = document.forms["signup_basic_form"]["email_id"].value;
		var remail = document.forms["signup_basic_form"]["r_email_id"].value;
	    if (email!=remail) 
	    {
	    	set_error_msg_border('Email. Email mismatch.','#r_email_id');
    		correct();
    	}
    	else 
    	{
    		set_ok_msg_border(email,'#r_email_id');
    		correct_dec();
    		correct();
    	}
    	
    });
    $("#password").focus(function(){
    	var id_name="#password_msg";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Password between 8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character.</div>");
	 	}
	    scrollUp();
    });
	$("#password").blur(function()
	{	
		var pass = document.forms["signup_basic_form"]["password"].value;
	    if (!validatePassword(pass)) 
	    {
	    	set_error_msg_border('Password. Password does not match our criteria.','#password');
    		correct();
    	}
    	else 
    	{
    		set_ok_msg_border('valid password','#password');
    		correct_dec();
    		correct();
    	}
    	
    });
    $("#r_password").focus(function(){
    	var id_name="#r_password_msg";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Retype to Confirm password.</div>");
	 	}
	    scrollUp();
    });
	$("#r_password").blur(function()
	{	
		var pass = document.forms["signup_basic_form"]["password"].value;
		var r_pass= document.forms["signup_basic_form"]["r_password"].value;
	    if (pass!==r_pass) 
	    {
	    	set_error_msg_border('Password. Passwords does not match.','#r_password');
    		correct();
    	}
    	else 
    	{
    		set_ok_msg_border('a match','#r_password');
    		correct_dec();
    		correct();
    	}
    	
    });

});