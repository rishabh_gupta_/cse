$(document).mouseup(function (e)
	{
	    var container = $("#menu");
	    if (!container.is(e.target) // if the target of the click isn't the container...
	        && container.has(e.target).length === 0) // ... nor a descendant of the container
	    {
	        container.hide();
	        document.getElementById('menu').style.display = "none";
	    }
});
function showDiv() {
   document.getElementById('menu').style.display = "block";
  
}
var corr = 6;
function correct_inc()
{
	console.log(corr);
}
function correct_dec()
{
	corr--;
	console.log(corr);
}
/*
*function 
*@param value is id of the field
*re
*/
function isPresent(id)
{
	if(document.getElementById(id))
	{
		return true;
	}
	else
	{
		return false;
	}
}
function correct()
{	
	$("#signup_btn").attr("disabled", true);
	var found=false;
	if ($(".signup_right").find(".error_msg").length > 0){ 
  		found=true;
  		console.log(found);
  	}
	if(corr<=0 && found===false)
	{
		$("#signup_btn").removeAttr("disabled");

	}


}
/*
*function 
*@param value is id of the field
*re
*/
function isPresent(id)
{
	if(document.getElementById(id))
	{
		return true;
	}
	else
	{
		return false;
	}
}
/*
*function to validate password
^                         Start anchor
(?=.*[A-Z])       		  Ensure string has 1 uppercase letter.
(?=.*[!@#$&*])            Ensure string has one special case letter.
(?=.*[0-9])        		  Ensure string has 1 digits.
(?=.*[a-z])			      Ensure string has three lowercase letters.
.{8}                      Ensure string is of length 8.
$                         End anchor
*returns true or false
*/
function validatePassword(value)
{
	var passReg =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;  
	return passReg.test(value);
}

/*
*function to validate email address
*returns true or false
*/
function validateEmail(email) {
  var emailReg = /^([\w-\.]+@(srmuniv.edu.in)+)$/;
  return emailReg.test(email);
}

/*
*fucntion to scroll the right side div to the bottom
*/
function scrollUp()
{
	$(".signup_right").animate({ scrollTop: $(".signup_right")[0].scrollHeight}, 2000);
}
/*
*function removes ok border and adds error border
*@param value is id of the textfield
*/
function set_border_error(value)
{
    if($(value).hasClass("input_ok"))
    {
    	$(value).removeClass("input_ok");
    }
    $(value).addClass("input_error");
}

/*
*function removes error border and adds ok border
*@param value is id of the textfield
*/
function set_border_ok(value)
{
    if($(value).hasClass("input_error"))
    {
    	$(value).removeClass("input_error");
    }
    $(value).addClass("input_ok");
}

/*
*function calls error border and adds div with classand id_error_msg
*removes id_ok_msg if exists before
*@param value is id of the textfield and its value
*/
function set_error_msg_border(value,id)
{	
	var id_msg = id+"_err_msg";
	var id_ok_msg = id+"_ok_msg";
	if(document.getElementById(id_msg)==null)
	{
		$(".signup_right").append("<div class=\"signup_msg error_msg\" id="+id_msg+">Please enter a valid "+value+"</div>");
	}
	if(document.getElementById(id_ok_msg))
	{
		//document.getElementById(id_msg).style.display = "none";
		var element = document.getElementById(id_ok_msg);
		element.outerHTML = "";
		delete element;
		//console.log('remove'+id_ok_msg);
	}
	set_border_error(id);
	$(".signup_right").animate({ scrollTop: $(".signup_right")[0].scrollHeight}, 2000);
}

/*
*function calls error border and adds div with classand id_ok_msg
*removes id_err_msg if exists before
*@param value is id of the textfield and its value
*/
function set_ok_msg_border(value,id)
{
	var id_err_msg = id+"_err_msg";
	var id_ok_msg = id+"_ok_msg";
	if(document.getElementById(id_err_msg))
	{
		//document.getElementById(id_msg).style.display = "none";
		var element = document.getElementById(id_err_msg);
		element.outerHTML = "";
		delete element;
		//console.log('remove'+id_err_msg);
	}
	if(document.getElementById(id_ok_msg))
	{
		//document.getElementById(id_msg).style.display = "none";
		var element = document.getElementById(id_ok_msg);
		element.outerHTML = "";
		delete element;
		//console.log('remove'+id_ok_msg);
	}
	$(".signup_right").append("<div class=\"signup_msg ok_msg\" id="+id_ok_msg+">So it is "+value+"</div>");
	set_border_ok(id);
	$(".signup_right").animate({ scrollTop: $(".signup_right")[0].scrollHeight}, 2000);
}
/*
*Funtion to check wether the region has been selected or not
*/
function chckRegion()
{
	console.log("Region")
	var l = document.forms["signup_basic_form"]["region_sel"].selected;
	if(l)
	{
		set_ok_msg_border(l,'#region_sel');
		
	}
	else
	{
		set_error_msg_border('Region','#region_sel');
	}
}
/*
*Funtion to check wether the country has been selected or not
*/
function chckCountry()
{
	var l = document.forms["signup_basic_form"]["county_sel"].value;
	if(l)
	{
		set_ok_msg_border(l,'#region_sel');
	}
	else
	{
		var id_name="#region_msg";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please select your Region.</div>");
	 	}
	    scrollUp();
		
	}
}
$(document).ready(function()
{

	$("#dob").focus(function()
	{
		var id_name="#dob";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please enter your date of birth.</div>");
	 	}
	    scrollUp();
	});
	$("#dob").blur(function()
	{	
		
		var l = document.forms["signup_basic_form"]["dob"].value;	
	    if (l == null || l== "") 
	    {
	    	set_error_msg_border('date of birth','#dob');
	    }
    	else 
    	{
    		set_ok_msg_border(l,'#dob');
    	
    	}
    	
    });
    $("#doj").focus(function()
	{
		var id_name="#doj";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please enter your date of joiing SRM University.</div>");
	 	}
	    scrollUp();
	});
	$("#doj").blur(function()
	{	
		
		var l = document.forms["signup_basic_form"]["doj"].value;	
	    if (l == null || l== "") 
	    {
	    	set_error_msg_border('date of birth','#doj');
    		
	    }
    	else 
    	{
   
    		set_ok_msg_border(l,'#doj');
    		
    	}
    	
    });
    $("#cgpa").focus(function()
	{
		var id_name="#doj";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please enter your current CGPA.</div>");
	 	}
	    scrollUp();
	});
	$("#cgpa").blur(function()
	{	
		
		var l = document.forms["signup_basic_form"]["cgpa"].value;	
	    if (l == null || l== "" || l<0 || l>10) 
	    {
	    	set_error_msg_border('CGPA','#cgpa');
    		correct();
	    }
    	else 
    	{
   
    		set_ok_msg_border(l,'#cgpa');
    		correct_dec();
    		correct();
    	
    	}
    	
    });
    $("#address").focus(function()
	{
		var id_name="#paddress";
		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please enter your Permanent Address</div>");
	 	}
	    scrollUp();
	});
	$("#address").blur(function()
	{	
		
		var l = document.forms["signup_basic_form"]["address"].value;
	    if (l == null || l== "") 
	    {
	    	set_error_msg_border('permanent address','#address');
    		correct();
	    }
    	else 
    	{
   
    		set_ok_msg_border(l,'#address');
    		correct_dec();
    		correct();
    	
    	}
    	
    });
    $("#city").focus(function()
	{
		var id_name="#city";

		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please city you belong to</div>");
	 	}
	    scrollUp();
	  

	});
	$("#city").blur(function()
	{	
		
		var l = document.forms["signup_basic_form"]["city"].value;
	    if (l == null || l== "" ) 
	    {
	    	set_error_msg_border('city','#city');
    		correct();
	    }
    	else 
    	{
   
    		set_ok_msg_border(l,'#city');
    		correct_dec();
    		correct();
    	
    	}
    	
    });
	$("#roomno").focus(function()
	{
		var id_name="#roomno";

		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please enter your room number.</div>");
	 	}
	    scrollUp();
	  

	});
	$("#roomno").blur(function()
	{	
		
		var l = document.forms["signup_basic_form"]["roomno"].value;
	    if (l == null || l== "" ) 
	    {
	    	set_error_msg_border('room numaber','#roomno');
    		correct();
	    }
    	else 
    	{
   
    		set_ok_msg_border(l,'#roomno');
    		correct_dec();
    		correct();
    	
    	}
    	
    });
    $("#block").focus(function()
	{
		var id_name="#block";

		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please enter your room number.</div>");
	 	}
	    scrollUp();
	  

	});
	$("#block").change(function()
	{	
		
		var l = document.forms["signup_basic_form"]["block"].value;
	    if (l == null || l== "") 
	    {
	    	set_error_msg_border('room numaber','#block');
    		correct();
	    }
    	else 
    	{
   
    		set_ok_msg_border(l,'#block');
    		correct_dec();
    		correct();
    	
    	}
    	
    });
    $("#region_sel").focus(function()
	{
		var id_name="#region_sel";

		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please select the region you belong to.</div>");
	 	}
	    scrollUp();
	  

	});
	$("#region_sel").click(function()
	{	
		
		var l = document.forms["signup_basic_form"]["region_sel"].value;
	    if (l == null || l==  0) 
	    {
	    	set_error_msg_border('region','#region_sel');
    		correct();
	    }
    	else 
    	{
   
    		set_ok_msg_border(l,'#region_sel');
    		correct_dec();
    		correct();
    	
    	}
    	
    });
	$("#country_sel").focus(function()
	{
		var id_name="#country_sel";

		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please select the country you live in.</div>");
	 	}
	    scrollUp();
	  

	});
	$("#country_sel").click(function()
	{	
		
		var l = document.forms["signup_basic_form"]["country_sel"].value;
	    if (l == null || l==  0) 
	    {
	    	set_error_msg_border('country','#country_sel');
    		correct();
	    }
    	else 
    	{
   
    		set_ok_msg_border(l,'#country_sel');
    		correct_dec();
    		correct();
    	
    	}
    	
    });
    $("#state_sel").focus(function()
	{
		var id_name="#state_sel";

		if(!isPresent(id_name))
		{
	    	 $(".signup_right").append("<div class=\"signup_msg\" id=\""+id_name+"\">Please select the state you live in.</div>");
	 	}
	    scrollUp();
	  

	});
	$("#state_sel").click(function()
	{	
		
		var l = document.forms["signup_basic_form"]["state_sel"].value;
	    if (l == null || l==0 ) 
	    {
	    	set_error_msg_border('state','#state_sel');
    		correct();
	    }
    	else 
    	{
   
    		set_ok_msg_border(l,'#state_sel');
    		correct_dec();
    		correct();
    	
    	}
    	
    });
	
});