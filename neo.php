<?php
use Everyman\Neo4j\Client,
    Everyman\Neo4j\Transport,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Relationship;

require('vendor/autoload.php'); // or your custom autoloader

// Connecting to the default port 7474 on localhost
$client = new Everyman\Neo4j\Client();
/*
$client2 = new Client(new Transport('localhost', 7474));
$annie = new Node($client2);
$userLabel = $client2->makeLabel('Student');
$annie->setProperty('name', 'Anahita Kaul')->save();
$annie->addLabels(array($userLabel))->save();
// Test connection to server
//print_r($client->getServerInfo());
*/
$queryStringClass = "MATCH (class:Class {sid: \"4CSEA\"})<-[:ATTENDS]-(s) return s;";
$queryStringRoom = "MATCH (room:Room {no: \"629\"})<-[:STAYS_IN]-(s) return s;";
$queryStringInt = "MATCH (interest:Interest {name : \"Hacking\"})<-[:INTERESTED_IN]-(a) return a;";
$queryFirstDeg = "MATCH (sud:Student {name : \"Sudhanshu Malik\"})-[:KNOWS]-(fof) WHERE sud <> fof RETURN DISTINCT fof;";
$queryScndDeg = "MATCH (sud:Student {name : \"Sudhanshu Malik\"})-[:KNOWS*2]-(fof) WHERE sud <> fof RETURN DISTINCT fof;";
$queryThirdDeg = "MATCH (sud:Student {name : \"Sudhanshu Malik\"})-[:KNOWS*3]-(fof) WHERE sud <> fof RETURN DISTINCT fof;";
$queryLength = "MATCH p=shortestPath((sud:Student)-[:KNOWS]-(annie:Student)) WHERE sud.name = \"Sudhanshu Malik\" AND annie.name = \"Anahita Kaul\" RETURN length(p) AS length;";
$queryShortestPath = "MATCH p=shortestPath((sud:Student)-[:KNOWS]-(annie:Student)) WHERE sud.name = \"Sudhanshu Malik\" AND annie.name =\"Anahita Kaul\"  RETURN [n in nodes(p) | n.name] as names;";
//"MATCH (tom:Person {name:\"Tom Hanks\"})-[ACTED_IN]->(m), (director)-[:DIRECTED]->(m) return  director as D, m as M;";
//$queryString = "MATCH (tom:Person {name:\"Tom Hanks\"})-[ACTED_IN]->(movie:Movie) where movie.released <1992 return distinct movie as M;";
$query = new Everyman\Neo4j\Cypher\Query($client, $queryStringClass);
$result = $query->getResultSet();
echo "Classmates <br>";
foreach ($result as $row) {
	//echo "Tom Hanks ACTED IN ";
    //echo $row['M']->getProperty('title')." DIRECTED BY ";
    //echo $row['D']->getProperty('name');
    //echo " IN THE YEAR ";
    //echo $row['M']->getProperty('released')."</br>";
    echo $row[0]->getProperty('name')."</br>";

}
$query = new Everyman\Neo4j\Cypher\Query($client, $queryStringRoom);
$result = $query->getResultSet();
echo "<br>Roomates <br>";
foreach ($result as $row) {
	//echo "Tom Hanks ACTED IN ";
    //echo $row['M']->getProperty('title')." DIRECTED BY ";
    //echo $row['D']->getProperty('name');
    //echo " IN THE YEAR ";
    //echo $row['M']->getProperty('released')."</br>";
    echo $row[0]->getProperty('name')."</br>";

}
$query = new Everyman\Neo4j\Cypher\Query($client, $queryStringInt);
$result = $query->getResultSet();
echo "<br>Hackers <br>";
foreach ($result as $row) {
	//echo "Tom Hanks ACTED IN ";
    //echo $row['M']->getProperty('title')." DIRECTED BY ";
    //echo $row['D']->getProperty('name');
    //echo " IN THE YEAR ";
    //echo $row['M']->getProperty('released')."</br>";
    echo $row[0]->getProperty('name')."</br>";

}
echo "<br>First Degree Friends <br>";
$query = new Everyman\Neo4j\Cypher\Query($client, $queryFirstDeg);
$result = $query->getResultSet();
foreach ($result as $row) {
    echo $row[0]->getProperty('name')."</br>";

}
echo "<br>First Second Degree Friends <br>";
$query = new Everyman\Neo4j\Cypher\Query($client, $queryScndDeg);
$result = $query->getResultSet();
foreach ($result as $row) {
    echo $row[0]->getProperty('name')."</br>";

}
echo "<br>First Third Degree Friends <br>";
$query = new Everyman\Neo4j\Cypher\Query($client, $queryThirdDeg);
$result = $query->getResultSet();
foreach ($result as $row) {
    echo $row[0]->getProperty('name')."</br>";

}
echo "<br>shortestPath Between You and Anahita Kaul is:<br>";
$query = new Everyman\Neo4j\Cypher\Query($client, $queryLength);
$result2 = $query->getResultSet();
$value=$result2[0]['length'];
echo $result2[0]['length'];

echo "<br>Path <br>";
$query = new Everyman\Neo4j\Cypher\Query($client, $queryShortestPath);
$result = $query->getResultSet();
foreach ($result as $row) {
    
    for($count= 0; $count<=$value;$count++)
    {
        echo $row[0][$count]."<br>";
    }
}

/*IMPORTANT THIS IS HOW WE CREATE NODES IS NEO$J IN PHP
  	$client2 = new Client('localhost', 7474);
    $user = new Node($client2);
    $user->setProperty('name', 'Anahita Kaul')->save();
    $user->setProperty('sid','1031110145')->save();
    $user->setProperty('yoj','2011')->save();
    $userLabel = $client->makeLabel('Student');
	$user->save()->addLabels(array($userLabel));
 
*/ 
