<?php require_once("includes/connection.php");?>
<?php require_once("includes/functions.php");?>
<?php require_once("includes/session.php");?>
<?php
	find_selected_page();
?>
<?php
		if(isset($_SESSION['user_name']))
		{
			if($_SESSION['user_name']!=SUPER)
			{
				 redirect_to("home.php");
			}
		}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style/main.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="javascripts/basic.js"></script>
<title>CSE Portal-Content</title>

</head>
<body>
	<!--include header-->
	<?php include("includes/header.php");?>
	<!--header ends-->
	<div id="body_container">
		<div id="body_left">
			<!--User Pofile goes here-->
			<div id="profile_pic">
				<?php include("includes/profile_pic.php");?>
			</div>
			<div id="profile_nav">
				<!--Profile Navigation goes here
				<ul>
					<li><a href="content.php">Profile</a></li>
					<li><a href="projects.php">Projects</a></li>
					<li><a href="new_user.php">Add Staff User</a></li>
					<li><a href="logout.php">Logout</a></li>
				</ul>
				-->
				<?php echo navigation_admin($sel_subject,$sel_page);?>
				<br/>
				<div class="create_menu">
					<a href="new_project.php">+ Add a new Project</a>
					</br>
					<a href="new_user.php">+ Add a new User</a>
				</div>
			</div>
		</div>
		<div id="body_main">
			<!--Feed goes here-->
			<div class="body_header">
				<?php
					if(!is_null($sel_subject))
					{
						echo "<h2> {$sel_subject['menu_name']}";
					}
					else if(!is_null($sel_page))
					{
						echo "<h2> {$sel_page['menu_name']}";

					}
					else
					{
						echo "<h2>Updates</h2>";
					}
				?>
				
				<?php
					if(!is_null($sel_page))
					{
					?>
					  	<div class="edit">
							<a href=<?php echo "edit_project.php?page=".urlencode($sel_page["id"]);?>>+ Edit</a>
						</div>

					<?php
					}
				?>
			</div>
			
				<?php
					if(!is_null($sel_page))
					{
					?>
					  	<div class="body_container">
						 	<?php echo strip_tags(nl2br($sel_page['content']),"<b><i><u><a><br>");?>
						</div>

					<?php
					}
				?>
				
			
		</div>
		<div id="body_right">
			<!--opposite details goes here-->
		</div>
	</div>
	<!--include footer-->
	<?php require("includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
