<?php require_once("includes/session.php");?>
<?php require_once("includes/functions.php");?>

<?php
	//start form processing
	
	if(isset($_POST['student']))
	{
		redirect_to('signup/student/basic.php');
	}
	if(isset($_POST['faculty']))
	{

		redirect_to('signup/faculty/basic.php');
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style/main.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="javascripts/basic.js"></script>
<title>StageSpace</title>

</head>
<body>
	<!--include header-->
	<?php include("includes/header_home.php");?>
	<!--header ends-->
	<img src="images/image.jpg" id="background">
	<div id="bubble_container">
		<div id="body_container_home" >
		<div class="signup" style="margin-right:70px;" >
			<div class="bubble">
				<h2>Mentee</h2>
				<p>Students</p>
				<form action="index.php" method="post">
					<input type="submit" name="student" value="Sign Up" id="signup_btn" />
				</form>
			</div>
		</div>
		<div class="signup" style="margin-right:70px;">
			<div class="bubble" >
				<h2>Mentor</h2>
				<p>Faculty, Professors and others</p>
				<form action="index.php" method="post">
					<input type="submit" name="faculty" value="Sign Up" id="signup_btn" />
				</form>
			</div>
		</div>
		<div class="signup">
			<div class="bubble">
				<h2>Alumnus</h2>
				<p>Graduates and Post Graduates</p>
				<form action="index.php" method="post">
					<input type="submit" name="alumnus" value="Sign Up" id="signup_btn" />
				</form>
			</div>
		</div>
	</div>
	</div>
	
	<!--include footer-->
	<?php require("includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
