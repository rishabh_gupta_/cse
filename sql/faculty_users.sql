-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 08, 2015 at 02:57 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cse_dept`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty_users`
--

CREATE TABLE IF NOT EXISTS `faculty_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `hashed_password` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `faculty_users`
--

INSERT INTO `faculty_users` (`id`, `username`, `hashed_password`) VALUES
(1, '1031110026', '1eb0b8dc987b82e5e310aa9e7d73f3798fdeebea'),
(2, '1031110011', '1eb0b8dc987b82e5e310aa9e7d73f3798fdeebea'),
(3, '1031110061', '1eb0b8dc987b82e5e310aa9e7d73f3798fdeebea'),
(6, '1031110010', '1eb0b8dc987b82e5e310aa9e7d73f3798fdeebea'),
(8, '1031111010', '1eb0b8dc987b82e5e310aa9e7d73f3798fdeebea'),
(9, '1021110060', 'aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d'),
(10, '101111050', '30603fa9e0f620c305cd627ab0ff138a960c48bd'),
(12, '1021110101', '1eb0b8dc987b82e5e310aa9e7d73f3798fdeebea'),
(13, '1041110060', '1eb0b8dc987b82e5e310aa9e7d73f3798fdeebea'),
(25, '1031110060', '1eb0b8dc987b82e5e310aa9e7d73f3798fdeebea'),
(29, '1031110029', 'aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d'),
(30, '1031110393', '1eb0b8dc987b82e5e310aa9e7d73f3798fdeebea'),
(31, '1031110007', 'b1b3773a05c0ed0176787a4f1574ff0075f7521e'),
(32, '1031110041', 'b1b3773a05c0ed0176787a4f1574ff0075f7521e'),
(33, '1041110040', 'b1b3773a05c0ed0176787a4f1574ff0075f7521e');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
