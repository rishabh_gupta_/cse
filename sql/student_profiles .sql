-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 08, 2015 at 09:33 AM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stagespace`
--

-- --------------------------------------------------------

--
-- Table structure for table `student_profiles`
--

CREATE TABLE IF NOT EXISTS `student_profiles` (
  `SNO` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Serial Number',
  `SID` int(11) NOT NULL COMMENT 'Students College ID',
  `FNAME` varchar(20) NOT NULL COMMENT 'Students first name',
  `LNAME` varchar(20) NOT NULL COMMENT 'Students last name',
  `CEMAIL` varchar(30) NOT NULL COMMENT 'Students college email address',
  `EMAIL` varchar(30) NOT NULL COMMENT 'Students college''s email address',
  `SQUE` varchar(100) NOT NULL COMMENT 'Students Security question',
  `SQANS` varchar(100) NOT NULL COMMENT 'Students Security Answer',
  `STATUS` varchar(10) NOT NULL COMMENT 'Current or alumini',
  `HNAME` varchar(20) NOT NULL COMMENT 'Hostels Name',
  `ROOMNO` int(5) NOT NULL COMMENT 'Students Room Number',
  `YOJ` year(4) NOT NULL COMMENT 'Students Year of Joining',
  `DOB` date NOT NULL COMMENT 'Students Date of Birth',
  `PHONE` int(10) NOT NULL COMMENT 'Students phone number',
  `PRT_EMAIL` varchar(20) NOT NULL COMMENT 'Parents email id.',
  PRIMARY KEY (`SNO`,`SID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
