<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style/main.css" />
<title>CSE Portal-Staff</title>

</head>
<body>
	<header>
		<div id="head_container">
			<div id="head_logo">
				<!--logo.png goes here-->
			</div>
			<div id="head_main">
				<h3>CSE Department Portal</h3>
			</div>
			<div id="head_user">
				<span class="username">User Name</span>
				<br/>
				<span class="designation">Designation</span>
			</div>
			<div id="head_menu">
				<!--Menu goes here-->
			</div>
		</div>
	</header>
	<div id="body_container">
		<div id="body_left">
			<!--User Pofile goes here-->
			<div id="profile_pic"><!--prfile picture goes here--></div>
		</div>
		<div id="body_main">'
			<!--Feed goes here-->
		</div>
		<div id="body_right">
			<!--opposite details goes here-->
		</div>
	</div>
	<footer>
		<div id="foot_container"></div>
	</footer>
</body>
</html> 
