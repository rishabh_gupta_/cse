<?php require_once("includes/connection.php");?>
<?php require_once("includes/functions.php");	?>
<?php require_once("includes/session.php");?>
<?php find_selected_page();?>
<?php
	if(isset($_POST['submit']))
	{
		$id = $_SESSION['user_name'];
		$imageName=mysql_real_escape_string($_FILES["image"]["name"]);
		$image_data= mysql_real_escape_string(file_get_contents($_FILES["image"]["tmp_name"]));
		$image_type=mysql_real_escape_string($_FILES["image"]["type"]);
		if(substr($image_type, 0,5)=="image")
		{
			$query1="INSERT INTO profile_img (
				profile_id,image, img_name
				) VALUES (
				{$user_id},'{$image_data}','{$imageName}'
				)";
			
			$query="UPDATE profile_img ";
			$query.="SET image = '{$image_data}' , img_name = '{$imageName}' WHERE profile_id = {$id}";
			//echo $query;
			if(mysql_query($query))
			{
				//Success
				header("Location: content.php");
				exit;
			}
			else
			{
				//Display Error Message
				echo "<p>Upload failed</p>";
				echo  "<p>".mysql_error()."</p>";
			}

		}
		else
		{
			echo "please insert an image file";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style/main.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="javascripts/basic.js"></script>
<title>Profile Picture</title>

</head>
<body>
	<!--include header-->
	<?php include("includes/header.php");?>
	<!--header ends-->
	<div id="body_container">
		<div id="body_left">
			<!--User Pofile goes here-->
			<div id="profile_pic">
				<?php include("includes/profile_pic.php");?>
			</div>
			<div id="profile_nav">
				<!--Profile Navigation goes here-->
				<?php echo navigation($sel_subject,$sel_page);?>
				<br/>
				<div class="create_menu">
	
				</div>
			</div>
		</div>
		<div id="body_main">
			<!--Feed goes here-->
			<div class="body_header">
				<h2>Upload Profile Picture</h2>
			</div>
			<div class="body_container">
				<form action="upload_img.php" method="post"  enctype="multipart/form-data">
					<input type="hidden" name="MAX_FILE_SIZE" value="99999999" />
					<p>Upload:
						<input name="image" type="file" />
					</p>
					<input type="submit" value="Upload" id="submit_btn" name="submit" />
					<div class="clear"></div>
				</form>
				</br>
				
				<a href="content.php">Cancel</a>
			</div>
				
			
		</div>
		<div id="body_right">
			<!--opposite details goes here-->
		</div>
	</div>
	<!--include footer-->
	<?php require("includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
