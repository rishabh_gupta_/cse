<?php require_once("../../includes/session.php");?>
<?php require_once("../../includes/functions.php");?>
<?php require_once("../../includes/connection_stagespacedb.php");?>
<?php
	if(!isset($_SESSION['user_id']))
	{
		redirect_to("../../index.php");
	} 
	else
	{
		$id=$_SESSION['user_id'];
	}
	//start form processing
	if(isset($_POST['submit']))
	{
		set_stage($connection,$id,8);
		redirect_to("../../home.php");
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../../style/main.css" />
<link rel="stylesheet" type="text/css" href="../../style/signup_form.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="../../javascripts/basic.js"></script>
<title>Sign Up: Student</title>

</head>
<body class="bg-teal">
	<!--include header-->
	<?php include("../../includes/header_signup.php");?>
	<!--header ends-->
	<div id="body_container_signup">
		<div class="clear"></div>
		<div id="signup_container">
			<div id="progress_bar">
				<div class="arrow_active">
					Basic Info
				</div>
				<div class="arrow_active">
					Faculty Info
				</div>
				<div class="arrow_active">
					Profile Picture
				</div>
				<div class="arrow_active">
					Interests
				</div>
				<div class="arrow_active">
					Projects
				</div>
				<div class="arrow_active">
					Varify
				</div>
				<div class="box_arrow active">
					Welcome
				</div>
			</div>
			<div class="signup_right" style="height:455px;">
				<div class="signup_msg">
					Take a tour of StageSpace.
				</div>
			</div>
			<div class="signup_left">
				<form name="signup_basic_form" method="post">
					<p>
						<label>Welcome to Stagespace</label><br>
						<p style="font-size:12px;">
							StageSpace tour video.
						</p>
					</p>
					
					<input type="submit" name="submit" value="Home" id="signup_btn" style="width:50px;float:right;">
				</form>
			</div>
		</div>
	</div>
	
	<!--include footer-->
	<?php require("../../includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
