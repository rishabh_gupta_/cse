<?php require_once("../../includes/session.php");
		ini_set('display_errors', true);
	error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../../includes/functions.php");?>
<?php require_once("../../includes/connection_stagespacedb.php");?>
<?php
	//start form processing
	if(!isset($_SESSION['user_id']))
	{
		redirect_to("../../index.php");
	} 
	if(isset($_POST['submit']))
	{	
		$id_num=$_SESSION['user_id'];
		$query_img = "INSERT INTO profile_img 
						(user_id) VALUES ('{$id_num}')";
		$result=mysqli_query($connection,$query_img);
		if(mysqli_affected_rows($connection)==1)
		{
			//record inserted
		}
		$imageName=mysqli_real_escape_string($connection,$_FILES["image"]["name"]);
		$image_data= mysqli_real_escape_string($connection,file_get_contents($_FILES["image"]["tmp_name"]));
		$image_type=mysqli_real_escape_string($connection,$_FILES["image"]["type"]);
		
		if(substr($image_type, 0,5)=="image")
		{
			
			$query="UPDATE profile_img ";
			$query.="SET image = '{$image_data}' , img_name = '{$imageName}' WHERE user_id = '{$id_num}'";
			//echo $query;
			if(mysqli_query($connection,$query))
			{
				//Success
				set_stage($connection,$id_num,4);
				redirect_to("interest.php");
			}
			else
			{
				//Upload failed
			}

		}
		else
		{
			//select a file error
		}
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../../style/main.css" />
<link rel="stylesheet" type="text/css" href="../../style/signup_form.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="../../javascripts/basic.js"></script>
<title>Sign Up: Student</title>

</head>
<body class="bg-teal">
	<!--include header-->
	<?php include("../../includes/header_signup.php");?>
	<!--header ends-->
	<div id="body_container_signup">
		<div class="clear"></div>
		<div id="signup_container">
			<div id="progress_bar">
				<div class="arrow_active">
					Basic Info
				</div>
				<div class="arrow_active">
					Faculty Info
				</div>
				<div class="arrow_active">
					Profile Picture
				</div>
				<div class="arrow">
					Interests
				</div>
				<div class="arrow">
					Projects
				</div>
				<div class="arrow">
					Varify
				</div>
				<div class="box_arrow">
					Welcome
				</div>
			</div>
			<div class="signup_right" style="height:455px;">
				<div class="signup_msg">
					Welcome to StageSpace. Lets set up your account and get you started. Please fill in your id number.
				</div>
			</div>
			<div class="signup_left">
				<div name="signup_basic_form"  class="signup_basic_form">
					
					<p class="text-center">
						<br><label>Upload Profile Picture</label><br><br>
					</p>
					<div style="height:370px;">

						<div id="profile_pic">
							<img src="../../images/profile_temp.jpg">
						</div>
						<form action="profilepic.php" method="post"  enctype="multipart/form-data">
							<input type="hidden" name="MAX_FILE_SIZE" value="99999999" />
							<p class="text-center">
								<input name="image" type="file" />
							</p>
							<input type="submit" name="submit" value="Upload" id="signup_btn" style="margin-top:20px;margin-left:22px;" />
						</form>
					</div>.
					<div class="clear"></div>
					<!--
					<input type="submit" name="submit" value="Next" id="signup_btn" style="width:50px;float:right;" disabled="true">
					-->
				</div>
			</div>
		</div>
	</div>
	
	<!--include footer-->
	<?php require("../../includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
