<?php
	ini_set('display_errors', true);
	error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../../includes/session.php");?>
<?php require_once("../../includes/functions.php");?>
<?php require_once("../../includes/connection_stagespacedb.php");?>
<?php require_once('../../libs/PHPMailer/PHPMailerAutoload.php');?>
<?php
	if(!isset($_SESSION['user_id']))
	{
		redirect_to("../../index.php");

	}
	else
	{
		$id=$_SESSION['user_id'];
	} 
	if(isset($_POST['submit']))
	{
		//Form has been submitted

		//perform validation on the form data
		$code=$_POST['code'];
		$v_code = get_vcode($connection,$id);
		if($code == $v_code)
		{
			set_stage($connection,$id,7);
			redirect_to("welcome.php");
		}
		else
		{
			$mismatch=1;
		}

		
	}
	else
	{
			//generate a random 5 digit number
		$vcode=(mt_rand(10000,99999));
		//start form processing
		$query="UPDATE users SET 
					vcode='{$vcode}'
					WHERE id_num = {$id}";
		$result=mysqli_query($connection,$query);
		if(mysqli_affected_rows($connection)==1)
		{
			//echo 'updated';
			//now send verification code via email
			$username=$id;
			$email=get_emailid($connection,$id);
			$message="Welcome to StageSpace {$id}<br><br>Please verify this email address by entering the code 5 digit code provided below.<br><br>Verification Code: {$vcode} <br><br>Team StageSpace";

	        $mail = new PHPMailer;

	        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

	        $mail->isSMTP();                                      // Set mailer to use SMTP
	        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	        $mail->SMTPAuth = true;                               // Enable SMTP authentication
	        $mail->Username = 'no-reply@stagespace.in';                 // SMTP username
	        $mail->Password = 'stage@293';                           // SMTP password
	        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	        $mail->Port = 465;                                    // TCP port to connect to
	        $mail->isHTML(true);
	        $mail->From = $email;
	        $mail->SetFrom($email,$username);
	        $mail->FromName = $username;    // Add a recipient
	        $mail->addAddress($email);               // Name is optional
	        $mail->addReplyTo('info@lifewire.com', 'Information');
	                                   // Set email format to HTML

	        $mail->Subject = 'StageSpace: Accout Verfication';
	        $mail->Body    = $message;
	        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	        if(!$mail->send()) 
	        {
	            //echo 'Message could not be sent.';
	            //echo 'Mailer Error: ' . $mail->ErrorInfo;

	        } else {
	            //echo 'Message has been sent';
	        }
		}
		else
		{
			echo 'query failed';
			echo mysqli_error($connection);
		}
	}
	

	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../../style/main.css" />
<link rel="stylesheet" type="text/css" href="../../style/signup_form.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="../../javascripts/basic.js"></script>
<title>Sign Up: Student</title>

</head>
<body class="bg-teal">
	<!--include header-->
	<?php include("../../includes/header_signup.php");?>
	<!--header ends-->
	<div id="body_container_signup">
		<div class="clear"></div>
		<div id="signup_container">
			<div id="progress_bar">
				<div class="arrow_active">
					Basic Info
				</div>
				<div class="arrow_active">
					Faculty Info
				</div>
				<div class="arrow_active">
					Profile Picture
				</div>
				<div class="arrow_active">
					Interests
				</div>
				<div class="arrow_active">
					Projects
				</div>
				<div class="arrow_active">
					Varify
				</div>
				<div class="box_arrow">
					Welcome
				</div>
			</div>
			<div class="signup_right" style="height:455px;">
				<div class="signup_msg">
					Welcome to StageSpace. Lets set up your account and get you started. Please fill in your id number.
				</div>
				<?php
					if(isset($mismatch))
					{
						$message = '<div class="signup_msg error_msg">
										Verification code does not match please login again to resend a new Verfication code 
										to your college email address.
									</div>';
						echo $message;
					}
				?>
			</div>
			<div class="signup_left">
				<form name="signup_basic_form" method="post">
					<p>
						<label>Email Varification</label><br>
						<p style="font-size:12px;">
						A varification code has been sent to your email <?php echo $email;?> Please enter the code below to varify your email address.
						</p>
						<input type="text" name="code" maxlength="5" id="code" placeholder="Code" />
					</p>
					
					<input type="submit" name="submit" value="Next" id="signup_btn" style="width:50px;float:right;">
				</form>
			</div>
		</div>
	</div>
	
	<!--include footer-->
	<?php require("../../includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
