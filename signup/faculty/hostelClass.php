<?php require_once("../../includes/session.php");?>
<?php require_once("../../includes/functions.php");?>
<?php require_once("../../includes/connection_stagespacedb.php");?>
<?php
	//start form processing
	if(isset($_POST['submit']))
	{
		//Form has been submitted

		//perform validation on the form data
		$id_num=trim(mysql_prep($_POST['id']));
		$first_name=trim(mysql_prep($_POST['fname']));
		$middle_name=trim(mysql_prep($_POST['mname']));
		$last_name=trim(mysql_prep($_POST['lname']));
		$cemail =trim(mysql_prep($_POST['email']));
		$password =trim(mysql_prep($_POST['pass']));
		$hashed_password=sha1($password);
		//echo $id_num." ".$first_name." ".$middle_name." ".$last_name." ".$cemail." ".$password." ".$hashed_password;
		$query="INSERT INTO users
				(id_num,hashed_password,signup)
				VALUES
				('{$id_num}','{$hashed_password}','1')";
		$result=mysql_query($query,$connection);
		if(mysql_affected_rows()==1)
		{
			//successful
			//redirect_to("basic.php");
		}
		else
		{
			//failed
			$message=0;
			echo  "<p>".mysql_error()."</p>";
	
		}
		$query2="INSERT INTO student_profiles
				(SID,FNAME,MNAME,LNAME,CEMAIL)
				VALUES
				('{$id_num}','{$first_name}','{$middle_name}','{$last_name}','{$cemail}')";
		$result=mysql_query($query2,$connection);
		if(mysql_affected_rows()==1)
		{
			//successful
			redirect_to("basic.php");
		}
		else
		{
			//failed
			$message=0;
			echo  "<p>".mysql_error()."</p>";
	
		}
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../../style/main.css" />
<link rel="stylesheet" type="text/css" href="../../style/signup_form.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="../../javascripts/hostelClass.js"></script>
<title>Sign Up: Student</title>

</head>
<body style="background-color:#F3F3F3;">
	<!--include header-->
	<?php include("../../includes/header_signup.php");?>
	<!--header ends-->
	<div id="body_container_signup">
		<div class="clear"></div>
		<div id="signup_container">
			<div id="progress_bar">
				<div class="arrow_active">
					Basic Info
				</div>
				<div class="arrow_active">
					Faculty Info
				</div>
				<div class="arrow_active">
					Hostel & Class
				</div>
				<div class="arrow">
					Interests
				</div>
				<div class="arrow">
					Projects
				</div>
				<div class="arrow">
					Varify
				</div>
				<div class="box_arrow">
					Welcome
				</div>
			</div>
			<div class="signup_right" style="height:455px;">
				<div class="signup_msg">
					Please enter the requires student information.
				</div>
			</div>
			<div class="signup_left">
				<form name="signup_basic_form" method="post">
					<div class="clear"></div>
					<div class="signup_left_col" style="width:50%;">
						<p>
							<label>Select Hostel</label><br>
							<select class="hostel">
							  <option value="PAARI">Paari</option>
							  <option value="KAARI">Kaari</option>
							  <option value="OORI">Oori</option>
							  <option value="ADHYAMAN">Adhyaman</option>
							  <option value="ABODE">Abode Valley</option>
							  <option value="ESTANCIA">Estancia</option>
							</select>
						</p>
						<p>
							<label>Block</label><br>
							<select class="block" disabled="true">
							  <option value="none">Select</option>
							  <option value="A">A</option>
							  <option value="B">B</option>
							  <option value="C">C</option>
							  <option value="D">D</option>
							  <option value="E">E</option>
							  <option value="F">F</option>
							  <option value="G">G</option>
							  <option value="H">I</option>
							  <option value="J">K</option>
							  <option value="L">M</option>
							  <option value="N">N</option>
							  <option value="M">M</option>
							  <option value="O">O</option>
							  <option value="P">P</option>
							  <option value="Q">Q</option>
							  <option value="R">R</option>
							  <option value="S">S</option>
							  <option value="T">T</option>
							  <option value="U">U</option>
							  <option value="V">V</option>
							  <option value="X">X</option>
							  <option value="Y">Y</option>
							  <option value="Z">Z</option>
							</select>
						</p>
						<p>
							<label>Room Number</label><br>
							<input type="text" name="roomno" maxlength="50" id="roomno" />
						</p>
						<p>
							<label>Class Section</label><br>
							<input type="text" name="section" maxlength="50" id="section" />
						</p>
					</div>
					<div class="signup_right_col" style="width:50%;float:right;">
						<p>
							<label>Add Teachers</label><br>
							<select multiple class="teachers">
							  <option value="PAARI">Dr. E. Poovammal</option>
							  <option value="KAARI">Dr. S. S. Sridhar</option>
							  <option value="OORI">Prof. C. Malathy</option>
							  <option value="ADHYAMAN">Dr. S. Prabhakaran</option>
							  <option value="ABODE">Dr. D. Malathi</option>
							  <option value="ESTANCIA">Dr. B. Amudha</option>
							  <option value="ESTANCIA">Mrs. A Panaiyappan</option>
							  <option value="ESTANCIA">Mrs. M. Pushpalatha</option>
							  <option value="ESTANCIA">Mrs. T. Manoranjitham</option>
							  <option value="ESTANCIA">Mr. M. Murali</option>
							  <option value="ESTANCIA">Dr. T. Peer Meera Labbai</option>
							</select>
						</p>
					</div>
					<input type="submit" name="submit" value="Next" id="signup_btn" style="width:50px;float:right;" disabled="true">
				</form>
			</div>
		</div>
	</div>
	
	<!--include footer-->
	<?php require("../../includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
