<?php
	ini_set('display_errors', true);
	error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../../includes/session.php");?>
<?php require_once("../../includes/connection_stagespacedb.php");?>
<?php require_once("../../includes/functions.php");?>
<?php require_once("../../includes/signup_functions.php");?>
<?php require_once("../../includes/neo4jfunctions.php");?>
<?php
	//get users data srom session
	if(!isset($_SESSION['user_id']))
	{
		redirect_to("../../index.php");
	} 
	$id_num=$_SESSION['user_id'];
	$user_email=$_SESSION['user_email'];
	$user_name=$_SESSION['user_name'];
	$user_full_name = $_SESSION['user_full_name'];
?>
<?php
	//start form processing
	if(isset($_POST['submit']))
	{
		//Form has been submitted
		//perform validation on the form data
		//prepare all values from form for insertion into databases
		$dob=trim(mysqli_prep($connection,$_POST['dob']));
		$doj=trim(mysqli_prep($connection,$_POST['doj']));
		$parts = explode('-', $doj);
		$batch= $parts[0]+4;
		$year = $parts[0];
		$deg=trim(mysqli_prep($connection,$_POST['degree']));
		$area =trim(mysqli_prep($connection,$_POST['area']));
		$address =trim(mysqli_prep($connection,$_POST['address']));
		$region = trim(mysqli_prep($connection,$_POST['region']));
		$country = trim(mysqli_prep($connection,$_POST['country']));
		$state = trim(mysqli_prep($connection,$_POST['city_state']));
		$city = strtolower(trim(mysqli_prep($connection,$_POST['city'])));
		
		/*
		echo "Selected Teachers<br>";
		if($_POST['teachers']!=null)
		{
			foreach ($_POST['teachers'] as $selectedOption)
			{
	    		echo $selectedOption."\n";
			}
		}
		*/

		//insert into SQL Database
		$query="UPDATE faculty_profiles SET
				DEG ='{$deg}',
				AREA='{$area}',
				YOJ={$year},
				DOB='{$dob}',
				ADDR='{$address}',
				REGION='{$region}',
				COUNTRY='{$country}',
				STATE='{$state}',
				CITY='{$city}'
				WHERE FID = {$id_num}";
		//echo $query;
		$result=mysqli_query($connection,$query);
		if(mysqli_affected_rows($connection)==1)
		{
			//successful
			//perform neo4j quaires here
			//create faculty node
			createFaculty_Node($id_num, $user_full_name);
			//create Faculty city relationship student -[:BELONGS_TO]->city
			createFacultyCity_rel($id_num,$city);
			if($_POST['classes']!=null)
			{	
				//echo 'selected indoor interests:<br>';
				foreach ($_POST['classes'] as $selectedOption)
				{
					$val = explode('|', $selectedOption);
		    		$batch=$val[0];
		    		$course= $val[1];
		    		$section=$val[2];
		    		createClass_Node($batch,$course,$section);
		    		createFacultyClass_rel($id_num,$batch,$course,$section);
		    		createClassBatch_rel($batch,$course,$section);
	
				}
			}
			
			set_stage($connection,$id_num,3);
			redirect_to("profilepic.php");
		}
		else
		{
			echo 'query failed';
			echo mysqli_error($connection);
		}
		
		//perform neo4j queries 
		//create student node

		/*create hostel node
		create room node
		create batch node
		create course node
		create class node 

		rel student -[:STAYS_IN]-> room_no
		rel room_no -[:IS_INSIDE]->hostel
		rel student =[:FROM_BATCH]->batch
		rel student -[:STUDIES]->course
		rel student -[:CLASS_IN]->class

		//createStudent_Node($id_num, $user_full_name,'B.Tech','CSE','2011');
		*/
		
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../../style/main.css" />
<link rel="stylesheet" type="text/css" href="../../style/signup_form.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="../../javascripts/studentinfo.js"></script>
<script type="text/javascript" src="../../javascripts/hostelClass.js"></script>
<script type="text/javascript" src="../../javascripts/city_state.js"></script>
<title>Sign Up: Faculty</title>

</head>
<body class="bg-teal">
	<!--include header-->
	<?php include("../../includes/header_signup.php");?>
	<!--header ends-->
	<div id="body_container_signup">
		<div class="clear"></div>
		<div id="signup_container">
			<div id="progress_bar">
				<div class="arrow_active">
					Basic Info
				</div>
				<div class="arrow_active">
					Faculty Info
				</div>
				<div class="arrow">
					Profile Picture
				</div>
				<div class="arrow">
					Interests
				</div>
				<div class="arrow">
					Projects
				</div>
				<div class="arrow">
					Varify
				</div>
				<div class="box_arrow">
					Welcome
				</div>
			</div>
			<div class="signup_right" style="height:455px;">
				<div class="signup_msg">
					Please enter the requires student information.
				</div>
			</div>
			<div class="signup_left">
				<form name="signup_basic_form" method="post">
					<div class="clear"></div>
					<div class="signup_left_col">
						<p>
							<label>Date of Birth</label><br>
							<input type="date" name="dob" maxlength="50" id="dob" />
						</p>
						<p>
							<label>Date of Joining</label><br>
							<input type="date" name="doj" maxlength="50" id="doj" />
						</p>
						<p>
							<label>Highest Qualification</label><br>
							<select id="degree" name="degree">
							  <option value="BTECH">B.E</option>
							  <option value="MTECH">M.E</option>
							  <option value="PH.D">Ph.D</option>
							</select>
						</p>
						
						<p>
							<label>Area</label><br>
							<input type="text"  name="area" id="area" />
						</p>
					</div>
					<div class="signup_right_col">
						<p>
							<label>Permanent Address</label><br>
							<input type="text" name="address" maxlength="50" id="address" />
						</p>
						<p>
							<label>Region</label><br>
							<select onchange="set_country(this,country,city_state)" size="1" name="region" id="region_sel">
								<option value="0" selected="selected" >Select Region</option>
								<script type="text/javascript">
									setRegions(this);
								</script>
							</select>
						</p>
						<p>
							<label>Country</label><br>
							<select name="country" id="country_sel" size="1" disabled="disabled" onchange="set_city_state(this,city_state)"></select>
						</p>
						<p>
							<label>State</label><br>
							<select name="city_state" id="state_sel" size="1" disabled="disabled" onchange="print_city_state(country,this)"></select>
						</p>
						<p>
							<label>City</label><br>
							<input type="text" name="city" maxlength="50" id="city" />
						</p>
					</div>
					<div class="signup_right_col" style="float:right;min-height:400px;">
						<p>
							<label>Classes</label><br>
							<select multiple class="classes" id="classes" name="classes[]">
							  <?php
							  	$type = 'CSE';
							  	echo class_select_list($type,$connection);
							  ?>
							</select>
						</p>
					</div>
					<div class="clear"></div>
					<input type="submit" name="submit" value="Next" id="signup_btn" style="width:50px;float:right;">
				</form>
			</div>
		</div>
	</div>
	
	<!--include footer-->
	<?php require("../../includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
