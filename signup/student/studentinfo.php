<?php
	ini_set('display_errors', true);
	error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../../includes/session.php");?>
<?php require_once("../../includes/functions.php");?>
<?php require_once("../../includes/connection_stagespacedb.php");?>
<?php require_once("../../includes/neo4jfunctions.php");?>
<?php
	//get users data srom session
	if(!isset($_SESSION['user_id']))
	{
		redirect_to("../../index.php");
	} 
	$id_num=$_SESSION['user_id'];
	$user_email=$_SESSION['user_email'];
	$user_name=$_SESSION['user_name'];
	$user_full_name = $_SESSION['user_full_name'];
?>
<?php
	//start form processing
	if(isset($_POST['submit']))
	{
		//Form has been submitted
		//perform validation on the form data
		//prepare all values from form for insertion into databases
		$dob=trim(mysqli_prep($connection,$_POST['dob']));
		$doj=trim(mysqli_prep($connection,$_POST['doj']));
		$parts = explode('-', $doj);
		$batch= $parts[0]+4;
		$year = $parts[0];
		$deg=trim(mysqli_prep($connection,$_POST['degree']));
		$cgpa =trim(mysqli_prep($connection,$_POST['cgpa']));
		$address =trim(mysqli_prep($connection,$_POST['address']));
		$region = trim(mysqli_prep($connection,$_POST['region']));
		$country = trim(mysqli_prep($connection,$_POST['country']));
		$state = trim(mysqli_prep($connection,$_POST['city_state']));
		$city = strtolower(trim(mysqli_prep($connection,$_POST['city'])));
		$hostel = trim(mysqli_prep($connection,$_POST['hostel']));
		if($hostel == "301" || $hostel == "302")
		{
			$block = trim(mysqli_prep($connection,$_POST['block']));
		}
		$room_no = trim(mysqli_prep($connection,$_POST['roomno']));
		$section = trim(mysqli_prep($connection,$_POST['section']));
		$course= getCourse($connection,$id_num);
		/*
		echo "Selected Teachers<br>";
		if($_POST['teachers']!=null)
		{
			foreach ($_POST['teachers'] as $selectedOption)
			{
	    		echo $selectedOption."\n";
			}
		}
		*/

		//insert into SQL Database
		$query="UPDATE student_profiles SET
				HNAME='{$hostel}',
				ROOMNO= {$room_no},
				CGPA={$cgpa},
				YOJ={$year},
				DOB='{$dob}',
				PADD='{$address}',
				REGION='{$region}',
				COUNTRY='{$country}',
				STATE='{$state}',
				CITY='{$city}',
				BATCH={$batch},
				SECTION='{$section}'
				WHERE SID = {$id_num}";
		$result=mysqli_query($connection,$query);
		if(mysqli_affected_rows($connection)==1)
		{
			//successful
			//perform neo4j quaires here
			//create student node
			createStudent_Node($id_num, $user_full_name);
			//create hostel node 
			createHostel_Node($hostel);
			//create room node
			createRoom_Node($hostel,$room_no);
			//create batch node
			createBatch_Node($batch);
			//create class node
			createClass_Node($batch,$course,$section);
			//create student city relationship student -[:BELONGS_TO]->city
			createStudentCity_rel($id_num,$city);
			//create relationship student -[:STAYS_IN]- room 
			createStudentRoom_rel($id_num,$hostel,$room_no);
			//create relationship student -[ATTENDS] -class
			createStudentClass_rel($id_num,$batch,$course,$section);
			//create relationship room -[IS_IN]->hostel 
			createHostelRoom_rel($room_no,$hostel);
			//create relationship class -[:BATCH_OF]-> batch
			createClassBatch_rel($batch,$course,$section);
			//create roomate and knows relationship
			createRoomate_rel($id_num,$room_no,$hostel);
			//create classmate and knows relationship
			createClassmate_rel($id_num,$year,$course,$section);
		
			set_stage($connection,$id_num,3);
			redirect_to("profilepic.php");
		}
		else
		{
			echo 'query failed';
			echo mysqli_error($connection);
		}
		
		//perform neo4j queries 
		//create student node

		/*create hostel node
		create room node
		create batch node
		create course node
		create class node 

		rel student -[:STAYS_IN]-> room_no
		rel room_no -[:IS_INSIDE]->hostel
		rel student =[:FROM_BATCH]->batch
		rel student -[:STUDIES]->course
		rel student -[:CLASS_IN]->class

		//createStudent_Node($id_num, $user_full_name,'B.Tech','CSE','2011');
		*/
		
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../../style/main.css" />
<link rel="stylesheet" type="text/css" href="../../style/signup_form.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="../../javascripts/studentinfo.js"></script>
<script type="text/javascript" src="../../javascripts/hostelClass.js"></script>
<script type="text/javascript" src="../../javascripts/city_state.js"></script>
<title>Sign Up: Student</title>

</head>
<body class="bg-teal">
	<!--include header-->
	<?php include("../../includes/header_signup.php");?>
	<!--header ends-->
	<div id="body_container_signup">
		<div class="clear"></div>
		<div id="signup_container">
			<div id="progress_bar">
				<div class="arrow_active">
					Basic Info
				</div>
				<div class="arrow_active">
					Student Info
				</div>
				<div class="arrow">
					Profile Picture
				</div>
				<div class="arrow">
					Interests
				</div>
				<div class="arrow">
					Projects
				</div>
				<div class="arrow">
					Varify
				</div>
				<div class="box_arrow">
					Welcome
				</div>
			</div>
			<div class="signup_right" style="height:455px;">
				<div class="signup_msg">
					Please enter the requires student information.
				</div>
			</div>
			<div class="signup_left">
				<form name="signup_basic_form" method="post">
					<div class="clear"></div>
					<div class="signup_left_col">
						<p>
							<label>Date of Birth</label><br>
							<input type="date" name="dob" maxlength="50" id="dob" />
						</p>
						<p>
							<label>Date of Joining</label><br>
							<input type="date" name="doj" maxlength="50" id="doj" />
						</p>
						<p>
							<label>Degree</label><br>
							<select id="degree" name="degree">
							  <option value="BTECH">B.Tech</option>
							  <option value="MTECH">M.Tech</option>
							</select>
						</p>
						
						<p>
							<label>CGPA</label><br>
							<input type="number" step="any" name="cgpa" maxlength="4" id="cgpa" />
						</p>
					</div>
					<div class="signup_right_col">
						<p>
							<label>Permanent Address</label><br>
							<input type="text" name="address" maxlength="50" id="address" />
						</p>
						<p>
							<label>Region</label><br>
							<select onchange="set_country(this,country,city_state)" size="1" name="region" id="region_sel">
								<option value="0" selected="selected" >Select Region</option>
								<script type="text/javascript">
									setRegions(this);
								</script>
							</select>
						</p>
						<p>
							<label>Country</label><br>
							<select name="country" id="country_sel" size="1" disabled="disabled" onchange="set_city_state(this,city_state)"></select>
						</p>
						<p>
							<label>State</label><br>
							<select name="city_state" id="state_sel" size="1" disabled="disabled" onchange="print_city_state(country,this)"></select>
						</p>
						<p>
							<label>City</label><br>
							<input type="text" name="city" maxlength="50" id="city" />
						</p>
					</div>
					<div class="signup_right_col" style="float:right;min-height:400px;">
						<p>
							<label>Select Hostel</label><br>
							<select class="hostel" id="hostel" name="hostel">
								<?php
									getHostels($connection,$id_num);
								?>
							</select>
						</p>
						<p>
							<label>Block</label><br>
							<select class="block" disabled="true" name="block" id="block">
							  <option value="A">A</option>
							  <option value="B">B</option>
							  <option value="C">C</option>
							  <option value="D">D</option>
							  <option value="E">E</option>
							  <option value="F">F</option>
							  <option value="G">G</option>
							  <option value="H">I</option>
							  <option value="J">K</option>
							  <option value="L">M</option>
							  <option value="N">N</option>
							  <option value="M">M</option>
							  <option value="O">O</option>
							  <option value="P">P</option>
							  <option value="Q">Q</option>
							  <option value="R">R</option>
							  <option value="S">S</option>
							  <option value="T">T</option>
							  <option value="U">U</option>
							  <option value="V">V</option>
							  <option value="X">X</option>
							  <option value="Y">Y</option>
							  <option value="Z">Z</option>
							</select>
						</p>
						<p>
							<label>Room Number</label><br>
							<input type="text" name="roomno" maxlength="50" id="roomno" />
						</p>
						<p>
							<label>Class Section</label><br>
							<select class="classSec" name="section" id="section">
							  <option value="A">A</option>
							  <option value="B">B</option>
							  <option value="C">C</option>
							  <option value="D">D</option>
							  <option value="E">E</option>
							  <option value="F">F</option>
							  <option value="G">G</option>
							  <option value="H">I</option>
							  <option value="J">K</option>
							  <option value="L">M</option>
							  <option value="N">N</option>
							  <option value="M">M</option>
							  <option value="O">O</option>
							  <option value="P">P</option>
							  <option value="Q">Q</option>
							  <option value="R">R</option>
							  <option value="S">S</option>
							  <option value="T">T</option>
							  <option value="U">U</option>
							  <option value="V">V</option>
							  <option value="X">X</option>
							  <option value="Y">Y</option>
							  <option value="Z">Z</option>
							</select>
						</p>
						<p>
							<label>Add Teachers</label><br>
							<select multiple class="teachers" id="teachers" name="teachers[]">
							  <option value="EPL">Dr. E. Poovammal</option>
							  <option value="SSS">Dr. S. S. Sridhar</option>
							  <option value="CMT">Prof. C. Malathy</option>
							  <option value="SPK">Dr. S. Prabhakaran</option>
							  <option value="DMI">Dr. D. Malathi</option>
							  <option value="BMD">Dr. B. Amudha</option>
							  <option value="APY">Mrs. A Panaiyappan</option>
							  <option value="MPL">Mrs. M. Pushpalatha</option>
							  <option value="TMJ">Mrs. T. Manoranjitham</option>
							  <option value="MMR">Mr. M. Murali</option>
							  <option value="PML">Dr. T. Peer Meera Labbai</option>
							</select>
						</p>
					</div>
					<div class="clear"></div>
					<input type="submit" name="submit" value="Next" id="signup_btn" style="width:50px;float:right;" disabled="true">
				</form>
			</div>
		</div>
	</div>
	
	<!--include footer-->
	<?php require("../../includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
