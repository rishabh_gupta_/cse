<?php
	ini_set('display_errors', true);
	error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../../includes/session.php");?>
<?php require_once("../../includes/functions.php");?>
<?php require_once("../../includes/connection_stagespacedb.php");?>
<?php require_once("../../includes/neo4jfunctions.php");?>
<?php
	if(!isset($_SESSION['user_id']))
	{
		redirect_to("../../index.php");
	} 
	else
	{
		$id = $_SESSION['user_id'];
	}
	//start form processing
	if(isset($_POST['skip']))
	{
		redirect_to('verify.php');
	}
	if(isset($_POST['submit']))
	{
		//Form has been submitted

		//perform validation on the form data
		$title=trim(mysqli_prep($connection,$_POST['title']));
		$field=trim(mysqli_prep($connection,$_POST['field']));
		$doc=trim(mysqli_prep($connection,$_POST['doc']));
		$partner=trim(mysqli_prep($connection,$_POST['student_id']));
		$desc =trim(mysqli_prep($connection,$_POST['section']));
		$status =trim(mysqli_prep($connection,$_POST['status']));
		//echo $title." ".$field." ".$doc." ".$partner." ".$desc;
		$query="INSERT INTO project
				(NAME,AREA,DES,STATUS,DOC)
				VALUES
				('{$title}','{$field}','{$desc}','{$status}','{$doc}')";
		$result=mysqli_query($connection,$query);
		if(mysqli_affected_rows($connection)==1)
		{
			$query_pid = "SELECT * 
			FROM  `project` 
			WHERE NAME =  '{$title}'
			LIMIT 1";
			$result_set=mysqli_query($connection,$query_pid);
			confirm_query($result_set);
			if($page = mysqli_fetch_array($result_set,MYSQLI_ASSOC))
			{
				$pid= "'".$page['PID']."'";
				createStudentProject_rel($id,$pid,$title);
				if ($partner!=NULL) {
					createStudentProject_rel($partner,$pid,$title);
				}
				
			}
			
				//$pid=getecho $pid;
			//successful
			set_stage($connection,$id_num,6);
			redirect_to("verify.php");
		}
		else
		{
			//failed
			$message=0;
			echo  "<p>".mysqli_connect_error()."</p>";
	
		}
		/*
		$query="INSERT INTO project
				(NAME,AREA,DES,STATUS,DOC)
				VALUES
				('{$title}','{$field}','{$desc}','{$status}','{$doc}'";
		$result=mysql_query($query,$connection);
		if(mysql_affected_rows()==1)
		{
			//successful
			createStudentProject_rel($id,$project_id,$project_name)
			//redirect_to("basic.php");
		}
		else
		{
			//failed
			$message=0;
			echo  "<p>".mysql_error()."</p>";
	
		}
		*/
		
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../../style/main.css" />
<link rel="stylesheet" type="text/css" href="../../style/signup_form.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="../../javascripts/autocomplete.js"></script>
<script type="text/javascript" src="../../javascripts/project.js"></script>
<script type="text/javascript" src="../../javascripts/jquery.min.js"></script>

<title>Sign Up: Student</title>

</head>
<body class="bg-teal">
	<!--include header-->
	<?php include("../../includes/header_signup.php");?>
	<!--header ends-->
	<div id="body_container_signup">
		<div class="clear"></div>
		<div id="signup_container">
			<div id="progress_bar">
				<div class="arrow_active">
					Basic Info
				</div>
				<div class="arrow_active">
					Student Info
				</div>
				<div class="arrow_active">
					Profile Picture
				</div>
				<div class="arrow_active">
					Interests
				</div>
				<div class="arrow_active">
					Projects
				</div>
				<div class="arrow">
					Varify
				</div>
				<div class="box_arrow">
					Welcome
				</div>
			</div>
			<div class="signup_right" style="height:455px;">
				<div class="signup_msg">
					Please enter the requires student information.
				</div>
			</div>
			<div class="signup_left">
				<form name="signup_basic_form" method="post">
					<div class="clear"></div>
					<div class="signup_left_col" style="width:33%;">
						<p>
							<label>Title</label><br>
							<input type="text" name="title" maxlength="50" id="title" />
						</p>
						<p>
							<label>Field</label><br>
							<select name="field" id="field">
							  <option value="Research">Research</option>
							  <option value="Application">Application</option>
							   <option value="Paper">Paper</option>
							</select>
						</p>
						<p>
							<label>Date of Commencement</label><br>
							<input type="date" name="doc" maxlength="50" id="doc" />
						</p>
						<p>
							<label>Status</label><br>
							<select name="status" id="status">
							  <option value="Starting">Starting</option>
							  <option value="On Going">On Going</option>
							  <option value="Finished">Finished</option>
							</select>
						</p>
						<p>
							<!--
							<label style="float:left;width:50%;margin-bottom:0px;">Add partner</label>
							<label style="float:right;margin-bottom:0px;" class="add">+</label>
							-->
							<label>Add Partner</label>
							<input type="text" id="student_id" name="student_id" onkeyup="autocomplet()" style="margin-botton:0px;">
                    		<ul id="student_list_id"></ul>
						</p>
					</div>
					<div class="signup_right_col" style="width:33%;float:left;">
						<p>
							<label>Add Faculty</label><br>
							<select multiple  class="interest" disabled="true">
							  <option value="PAARI">Dr. E. Poovammal</option>
							  <option value="KAARI">Dr. S. S. Sridhar</option>
							  <option value="OORI">Prof. C. Malathy</option>
							  <option value="ADHYAMAN">Dr. S. Prabhakaran</option>
							  <option value="ABODE">Dr. D. Malathi</option>
							  <option value="ESTANCIA">Dr. B. Amudha</option>
							  <option value="ESTANCIA">Mrs. A Panaiyappan</option>
							  <option value="ESTANCIA">Mrs. M. Pushpalatha</option>
							  <option value="ESTANCIA">Mrs. T. Manoranjitham</option>
							  <option value="ESTANCIA">Mr. M. Murali</option>
							  <option value="ESTANCIA">Dr. T. Peer Meera Labbai</option>
							</select>
						</p>
					</div>
					<div class="signup_right_col" style="width:33%;float:right;">
						<p>
							<label>Discription</label><br>
							<textarea row="5" col="50" name="section" maxlength="200" id="section" class="dicp"></textarea> 
						</p>
					</div>
					<input type="submit" name="submit" value="Next" id="signup_btn" style="width:50px;float:right;" disabled="true">
					<input type="submit" name="skip" value="Skip" id="signup_btn" style="width:50px;margin-right:10px;float:right;">
				</form>
			</div>
		</div>
	</div>
	
	<!--include footer-->
	<?php require("../../includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
