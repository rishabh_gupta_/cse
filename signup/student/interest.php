<?php
	ini_set('display_errors', true);
	error_reporting(E_ALL ^ E_NOTICE);
?>
<?php require_once("../../includes/session.php");?>
<?php require_once("../../includes/connection_stagespacedb.php");?>
<?php require_once("../../includes/functions.php");?>
<?php require_once("../../includes/signup_functions.php");?>
<?php require_once("../../includes/neo4jfunctions.php");?>


<?php
	if(!isset($_SESSION['user_id']))
	{
		redirect_to("../../index.php");
	} 
	$id_num=$_SESSION['user_id'];
	//start form processing
	if(isset($_POST['submit']))
	{
		//Form has been submitted
		//perform validation on the form data
		if($_POST['indoor']!=null)
		{	
			echo 'selected indoor interests:<br>';
			foreach ($_POST['indoor'] as $selectedOption)
			{
				$val = explode('|', $selectedOption);
	    		$interest_id=$val[0];
	    		$interest_name= $val[1];
	    		createStudentInterest_rel($id_num,$interest_id,$interest_name);
			}
		}
		if($_POST['outdoor']!=null)
		{
			echo 'selected outdoor interests:<br>';
			foreach ($_POST['outdoor'] as $selectedOption)
			{
	    		$val = explode('|', $selectedOption);
	    		$interest_id=$val[0];
	    		$interest_name= $val[1];
	    		createStudentInterest_rel($id_num,$interest_id,$interest_name);
			}
		}
		if($_POST['skills']!=null)
		{
			echo 'selected skills interests:<br>';
			foreach ($_POST['skills'] as $selectedOption)
			{
	    		$val = explode('|', $selectedOption);
	    		$interest_id=$val[0];
	    		$interest_name= $val[1];
	    		createStudentInterest_rel($id_num,$interest_id,$interest_name);
			}
		}

		set_stage($connection,$id_num,5);
		redirect_to("projects.php");
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../../style/main.css" />
<link rel="stylesheet" type="text/css" href="../../style/signup_form.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="../../javascripts/basic.js"></script>
<script type="text/javascript" src="../../javascripts/city_state.js"></script>
<title>Sign Up: Student</title>

</head>
<body class="bg-teal">
	<!--include header-->
	<?php include("../../includes/header_signup.php");?>
	<!--header ends-->
	<div id="body_container_signup">
		<div class="clear"></div>
		<div id="signup_container">
			<div id="progress_bar">
				<div class="arrow_active">
					Basic Info
				</div>
				<div class="arrow_active">
					Student Info
				</div>
				<div class="arrow_active">
					Profile Picture
				</div>
				<div class="arrow_active">
					Interests
				</div>
				<div class="arrow">
					Projects
				</div>
				<div class="arrow">
					Varify
				</div>
				<div class="box_arrow">
					Welcome
				</div>
			</div>
			<div class="signup_right" style="height:455px;">
				<div class="signup_msg">
					Please enter the requires student information.
				</div>
			</div>
			<div class="signup_left">
				<form name="signup_basic_form" method="post">
					<div class="clear"></div>
					<div class="signup_left_col">
						<p>
							<label>Indoor Activities</label><br>
							<select multiple class="interest" id="indoor" name="indoor[]">
							  <?php
							  	$type = 'INDOOR';
							  	echo interest_select_list($type,$connection);
							  ?>
							</select>
						</p>
					</div>
					<div class="signup_right_col">
						<p>
							<label>Outdoor Activities</label><br>
							<select multiple class="interest" id="outdoor" name="outdoor[]">
							  <?php
							  	$type = 'OUTDOOR';
							  	echo interest_select_list($type,$connection);
							  ?>
							</select>
						</p>
					</div>
					<div class="signup_right_col" style="float:right;min-height:400px;">
						<p>
							<label>Branch Related Skills</label><br>
							<select multiple class="interest" id="skills" name="skills[]">
							  <?php
							  	$type = 'CSE';
							  	echo interest_select_list($type,$connection);
							  ?>
							</select>
						</p>
					</div>
					<div class="clear"></div>
					<input type="submit" name="submit" value="Next" id="signup_btn" style="width:50px;float:right;">
				</form>
			</div>
		</div>
	</div>
	
	<!--include footer-->
	<?php require("../../includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
