<?php 
	ini_set('error_reporting', E_ALL|E_STRICT);
    ini_set('display_errors', 1);

	require_once("../../includes/session.php");
?>
<?php require_once("../../includes/functions.php");?>
<?php require_once("../../includes/connection_stagespacedb.php");?>
<?php
	//cheack if form has been submitted
	if(isset($_POST['submit']))
	{
		//Form has been submitted

		//perform validation on the form data
		$id_num=trim(mysqli_prep($connection,$_POST['id']));
		$gender = trim(mysqli_prep($connection,$_POST['gender']));
		$first_name=trim(mysqli_prep($connection,$_POST['fname']));
		if(isset($_POST['mname']))
		{
			$middle_name=trim(mysqli_prep($connection,$_POST['mname']));
		}
		$last_name=trim(mysqli_prep($connection,$_POST['lname']));
		$cemail =trim(mysqli_prep($connection,$_POST['email']));
		$password =trim(mysqli_prep($connection,$_POST['pass']));
		$course =trim(mysqli_prep($connection,$_POST['course']));
		$hashed_password=sha1($password);
		
		
		$query="INSERT INTO users
				(id_num,hashed_password,signup)
				VALUES
				('{$id_num}','{$hashed_password}','1')";
		$result=mysqli_query($connection,$query);
		if(mysqli_affected_rows($connection)==1)
		{
			$_SESSION['user_id']=$id_num;
			$_SESSION['user_email']=$cemail;
			$_SESSION['user_name']=$first_name;
			if(!isset($middle_name))
			{
				$full_name= $first_name." ".$last_name;
			}
			else
			{
				$full_name=$first_name." ".$middle_name." ".$last_name;
			}
			$_SESSION['user_full_name']=$full_name;
			//successful
			//redirect_to("basic.php");
		}
		else
		{
			//failed
			$message=0;
			echo  "<p>".mysqli_connect_error()."</p>";
	
		}
		$query2="INSERT INTO student_profiles
				(SID,FNAME,MNAME,LNAME,GENDER,CEMAIL,COURSE)
				VALUES
				('{$id_num}','{$first_name}','{$middle_name}','{$last_name}','{$gender}','{$cemail}','{$course}')";
		$result=mysqli_query($connection,$query2);
		if(mysqli_affected_rows($connection)==1)
		{
			//successful
			set_stage($connection,$id_num,2);
			redirect_to("studentinfo.php");

			
		}

		else
		{
			//failed
			$message=0;
			echo  "<p>".mysqli_connect_error()."</p>";
	
		}
		
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../../style/main.css" />
<link rel="stylesheet" type="text/css" href="../../style/signup_form.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="../../javascripts/basic.js"></script>


<title>Sign Up: Student</title>

</head>
<body class="bg-teal">
	<!--include header-->
	<?php include("../../includes/header_signup.php");?>
	<!--header ends-->
	<div id="body_container_signup">
		<div class="clear"></div>
		<div id="signup_container">
			<div id="progress_bar">
				<div class="arrow_active">
					Basic Info
				</div>
				<div class="arrow">
					Student Info
				</div>
				<div class="arrow">
					Hostel & Class
				</div>
				<div class="arrow">
					Interests
				</div>
				<div class="arrow">
					Projects
				</div>
				<div class="arrow">
					Varify
				</div>
				<div class="box_arrow">
					Welcome
				</div>
			</div>
			<div class="signup_right" style="height:455px;">
				<div class="signup_msg">
					Welcome to StageSpace. Lets set up your account and get you started. Please fill in your id number.
				</div>
			</div>
			<div class="signup_left">
				<form name="signup_basic_form" method="post">
					<p>
						<label>ID</label><br>
						<input type="text" name="id" maxlength="50" id="id_num" />
					</p>

					<p style="float:left;">
						<label>First Name</label><br>
						<input type="text" name="fname" maxlength="50" id="first_name" />
					</p>
					<p style="float:left;margin-left:20px;">
						<label>Middle Name</label><br>
						<input type="text" name="mname" maxlength="50" id="middle_name" />
					</p>
					<p style="float:left;margin-left:20px;">
						<label>Last Name</label><br>
						<input type="text" name="lname" maxlength="50" id="last_name" />
					</p>
					<div class="clear"></div>
					<p style="float:left;">
						<label>Course</label><br>
						<select id="course" name="course">
						  <option value="CSE">Computer Science & Engineering</option>
						  <option value="IT">Information Technology</option>
						  <option value="SWE">Software Engineering</option>
						</select>
					</p>
					<p style="float:left;margin-left:28px;">
						<label>Gender</labe><br>
						<select id="gender" name="gender">
						  <option value='M'>Male</option>
						  <option value='F'>Female</option>
						</select>
					</p>
					<div class="clear"></div>
					<p>
						<label>College Email</label><br>
						<input type="email" name="email" maxlength="50" id="email_id" />
					</p>
					<p>
						<label>Retype College Email</label><br>
						<input type="email" maxlength="50" id="r_email_id" />
					</p>
					<p>
						<label>Password</label><br>
						<input type="password" name="pass" maxlength="50" id="password" />
					</p>
					<p>
						<label>Retype Password</label><br>
						<input type="password" maxlength="50" id="r_password" />
					</p>
					
					<input type="submit" name="submit" value="Next" id="signup_btn" style="width:50px;float:right;margin-top:-30px;" disabled="true">
				</form>
			</div>
		</div>
	</div>
	
	<!--include footer-->
	<?php require("../../includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
