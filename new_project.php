<?php require_once("includes/connection.php");?>
<?php require_once("includes/functions.php");?>
<?php find_selected_page();?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style/main.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="javascripts/basic.js"></script>
<title>Create Project</title>

</head>
<body>
	<!--include header-->
	<?php include("includes/header.php");?>
	<!--header ends-->
	<div id="body_container">
		<div id="body_left"
			<!--User Pofile goes here-->
			<div id="profile_pic">
				<?php include("includes/profile_pic.php");?>
			</div>
			<div id="profile_nav">
				<!--Profile Navigation goes here-->
				<?php echo navigation($sel_subject,$sel_page);?>
				<br/>
				<div class="create_menu">
	
				</div>
			</div>
		</div>
		<div id="body_main">
			<!--Feed goes here-->
			<div class="body_header">
				<h2>New Project</h2>
			</div>
			<div class="body_container">
				<form action="create_project.php" method="post">
					<p>Project Name:
						<input type="text" name="menu_name" value="" id="menu_name"/>
					</p>
					<p>Discription:</p>
					<p>
						<textarea rows="5" cols="94" name="content" value="" id="content"></textarea>
					</p>
					<p>Position:
						<select name="position">
							<?php
								$subject_set=get_all_pages(2);
								$subject_count=mysql_num_rows($subject_set);
								for($count=1; $count <= $subject_count+1; $count++ )
								{
									echo "<option value=\"{$count}\">{$count}</option>";
								}
							?>	
							
						</select>
					</p>
					<p>Visible:
						&nbsp;
						<input type="radio" name="visible" value="0"/>&nbsp;No
						&nbsp;
						<input type="radio" name="visible" value="1"/>&nbsp;Yes
					</p>
					<input type="submit" value="Add Project" id="submit_btn"/>
					<div class="clear"></div>
				</form>
				</br>
				
				<a href="content.php">Cancel</a>
			</div>
				
			
		</div>
		<div id="body_right">
			<!--opposite details goes here-->
			<?php
				include('neo.php');
			?>
		</div>
	</div>
	<!--include footer-->
	<?php require("includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
