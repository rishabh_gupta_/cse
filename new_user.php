<?php require_once("includes/connection.php");?>
<?php require_once("includes/functions.php");?>
<?php
	find_selected_page();
?>
<?php
	//start form processing
	if(isset($_POST['submit']))
	{
		//Form has been submitted

		//perform validation on the form data
		$username=trim(mysql_prep($_POST['username']));
		$password=trim(mysql_prep($_POST['password']));
		$hashed_password=sha1($password);

		$query="INSERT INTO faculty_users
				(username,hashed_password)
				VALUES
				('{$username}','{$hashed_password}')";
		$result=mysql_query($query,$connection);
		if(mysql_affected_rows()==1)
		{
			//successful
			//redirect_to("content.php?page={$id}");
		}
		else
		{
			//failed
			$message=0;
			echo  "<p>".mysql_error()."</p>";
	
		}

	}
	else
	{
		$username="";
		$password="";
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style/main.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="javascripts/basic.js"></script>
<title>New User</title>

</head>
<body>
	<!--include header-->
	<?php include("includes/header.php");?>
	<!--header ends-->
	<div id="body_container">
		<div id="body_left">
			<!--User Pofile goes here-->
			<div id="profile_pic">
				<?php include("includes/profile_pic.php");?>
			</div>
			<div id="profile_nav">
				<!--Profile Navigation goes here
				<ul>
					<li><a href="content.php">Profile</a></li>
					<li><a href="projects.php">Projects</a></li>
					<li><a href="new_user.php">Add Staff User</a></li>
					<li><a href="logout.php">Logout</a></li>
				</ul>
				-->
				<?php echo navigation($sel_subject,$sel_page);?>
				<br/>
				<div class="create_menu">
					<a href="new_project.php">+ Add a new Project</a>
					</br>
					<a href="new_user.php">+ Add a new User</a>
				</div>
			</div>
		</div>
		<div id="body_main">
			<!--Feed goes here-->
			<div class="body_header">
				<h2>Create User</h2>
				<?php
					if(isset($message) && $message==1)
					{
					?>
						<div class="saved">
							Created
						</div>
					<?php
					}
					else if(isset($message) && $message==0)
					{
					?>
						<div class="error">
							Error
						</div>
					<?php
					}
				?>
			</div>
			<div class="body_container">
				<form action="new_user.php" method="post">
					<p class="line">User Id:</br>
						<input type="text" name="username"  id="user_id" maxlength="30" class="text" value="<?php echo htmlentities($username)?>" />
					</p>
					<p class="line">Password:</br>
						<input type="password" name="password"  id="password" maxlength="30"  class="text" value="" />
					</p>
					<input type="submit" value="Create" id="submit_btn" name="submit" />
					<div class="clear"></div>
				</form>
				</br>
				
				<a href="admin.php">Cancel</a>
			</div>
		</div>
		<div id="body_right">
			<!--opposite details goes here-->
		</div>
	</div>
	<!--include footer-->
	<?php require("includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
