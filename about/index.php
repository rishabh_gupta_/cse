<?php require_once("../includes/session.php");?>
<?php require_once("../includes/functions.php");?>
<?php require_once("../includes/connection_stagespacedb.php");?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../style/main.css" />
<link rel="stylesheet" type="text/css" href="../style/about.css" />
<link rel="stylesheet" type="text/css" href="../style/signup_form.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="../javascripts/basic.js"></script>
<title>About StageSpace</title>

</head>
<body >
	<!--include header-->
	<?php include("../includes/header_main.php");?>
	<!--header ends-->
	<img src="../images/about.jpg" id="background" style="margin-top:0">
	
	
	<!--include footer-->
	<?php include("../includes/footer.php");?>
	<!--footer ends-->
</body>
</html> 
